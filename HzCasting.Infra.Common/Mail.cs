﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Common
{
    public class Mail
    {
        private string mailFormat;

        public Mail()
        {
            emails = new List<string>();
        }

        private List<string> emails;

        public void Add(string email)
        {
            emails.Add(email);
        }

        public void Send(string titulo, string corpo, TipoEmail tipoEmail = TipoEmail.Html)
        {
            SetMailFormat(tipoEmail);

            var fromAddress = new MailAddress("hzcasting0@gmail.com", "HzCasting");

            string fromPassword = "AlexGls24";

            using (var smtp = new SmtpClient())
            {
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(fromAddress.Address, fromPassword);

                using (var message = new MailMessage()
                {
                    Subject = titulo,
                    Body = corpo,
                    From = fromAddress
                })
                {
                    MailTo(message);
                    message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(corpo, null, mailFormat));
                    smtp.Send(message);
                }
            }
        }

        private void SetMailFormat(TipoEmail tipoEmail)
        {
            if (tipoEmail == TipoEmail.Html)
            {
                mailFormat = MediaTypeNames.Text.Html;
            }
            else
            {
                mailFormat = MediaTypeNames.Text.Plain;
            }
        }

        private void MailFrom(MailMessage mailMsg)
        {
            mailMsg.From = new MailAddress("hzcasting0@gmail.com", "HzCasting");
        }

        private void MailTo(MailMessage mailMsg)
        {
            foreach (var email in emails)
            {
                mailMsg.To.Add(new MailAddress(email));
            }
        }
    }
}
