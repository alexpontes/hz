﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Common
{
  public static  class DateTimeUtil
    {
        private const String FORMAT_DD_MM_YYYY = "dd/MM/yyyy";
        public static DateTime ToDate(this String date, String format = null)
        {
            var dt = DateTime.ParseExact(date, format ?? FORMAT_DD_MM_YYYY, CultureInfo.InvariantCulture);
            return dt;
        }
    }
}
