﻿using System;
using System.IO;
using System.Web;

namespace HzCasting.Infra.Common
{
    public static class ImageUtil
    {

        public static string GetSrcImage(this Byte[] imageBytes)
        {
            
            var base64 = Convert.ToBase64String(imageBytes);
        
            var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
            return imgSrc;
        }

        public static Byte[] GetArrayBytes(this HttpPostedFileBase item)
        {
            Byte[] data;
            using (var inputStream = item.InputStream)
            {
                var memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = memoryStream.ToArray();
                return data;
            }
        }

    }
}

