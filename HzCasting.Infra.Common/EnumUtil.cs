﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HzCasting.Infra.Common
{
    public static class EnumUtil
    {
        public static IDictionary<String, Int32> ObterDicionarioEnum(this Enum enumType)
        {
            var tipo = enumType.GetType();
            var enumValues = Enum.GetValues(tipo);
            var strings = Enum.GetNames(tipo).ToArray();
            var values = enumValues as IList<Int32>;
            var dict = new Dictionary<String, Int32>();
            for (var i = 0; i < values.Count; i++)
            {
                var valoresEnum = values[i];
                dict.Add(strings[i], valoresEnum);

            }

            return dict;

        }
        public static IList<SelectListItem> ToSelectListItem(this Enum enumType, Enum selected = null)
        {
            var selectListItem = enumType.ObterDicionarioEnum()?
               .Select(m => new SelectListItem
               {
                   Value = m.Value.ToString(),
                   Text = m.Key,
                   Selected = m.Key.ToString() == selected?.ToString()
               }).ToList();

            selectListItem.Insert(0, new SelectListItem
            {
                Text = "Selecione...",
                Value = "",
                Selected = selected == null
            });
            return selectListItem;
        }

        public static T ConvertByName<T>(this Enum value)
        {
            return (T)Enum.Parse(typeof(T), Enum.GetName(value.GetType(), value));
        }

        //public static IEnumerable<T> ConvertByValue<T>(this Enum value)
        //{

        //    yield return (T)((dynamic)((int)((object)value)));
        //}
    }


}
