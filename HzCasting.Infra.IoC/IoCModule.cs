﻿using HzCasting.Application;
using HzCasting.Domain.Interfaces;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Domain.Interfaces.Repository;
using HzCasting.Infra.Repositories;
using HzCasting.Infra.Repositories.EF;
using Ninject.Modules;

namespace HzCasting.Infra.IoC
{
    public class IoCModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IClienteRepository>().To<ClienteRepository>();
            Bind<IEventoRepository>().To<EventoRepository>();
            Bind<IEnderecoRepository>().To<EnderecoRepository>();
            Bind<IModeloRepository>().To<ModeloRepository>();
            Bind<IUsuarioRepository>().To<UsuarioRepository>();
            Bind<ICastingAgendaCastingRepository>().To<CastingAgendaCastingRepository>();
            Bind<IAgenciaRepository>().To<AgenciaRepository>();
            Bind<ICastingRepository>().To<CastingRepository>();
            Bind<IPerfilFiltroCastingRepository>().To<PerfilFiltroCastingRepository>();
            Bind<IBackOfficeRepository>().To<BackOfficeRepository>();
            Bind<IAgendaCastingRepository>().To<AgendaCastingRepository>();
            Bind<IInviteRepository>().To<InviteRepository>();
            Bind<IUsuarioClienteRepository>().To<UsuarioClienteRepository>();
            Bind<IBookingRepository>().To<BookingRepository>();
            Bind<IIdiomaRepository>().To<IdiomaRepository>();

            Bind<IModeloApplication>().To<ModeloApplication>();
            Bind<IClienteApplication>().To<ClienteApplication>();
            Bind<IAgenciaApplication>().To<AgenciaApplication>();
            Bind<IEventoApplication>().To<EventoApplication>();
            Bind<IUsuarioApplication>().To<UsuarioApplication>();
            Bind<ILoginApplication>().To<LoginApplication>();
            Bind<ICastingApplication>().To<CastingApplication>();
            Bind<IPerfilFiltroApplication>().To<PerfilFiltroApplication>(); 
            Bind<ICastingAgendaCastingApplication>().To<CastingAgendaCastingApplication>();
            Bind<IBackOfficeApplication>().To<BackOfficeApplication>();
            Bind<IUsuarioClienteApplication>().To<UsuarioClienteApplication>();
            Bind<IInviteApplication>().To<InviteApplication>();
            Bind<IAgendaCastingApplication>().To<AgendaCastingApplication>();
            Bind<IBookingApplication>().To<BookingApplication>();
            Bind<IIdiomaApplication>().To<IdiomaApplication>();

            Bind<IEnderecoApplication>().To<EnderecoApplication>();

            Bind<ContextManager>().ToSelf();
        }
    }
}
