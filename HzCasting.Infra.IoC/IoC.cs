﻿using CommonServiceLocator.NinjectAdapter.Unofficial;
using Microsoft.Practices.ServiceLocation;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.IoC
{
    public class IoC
    {
        private static StandardKernel _kernel;
        
        public static StandardKernel Kernel
        {
            get;
            private set;

        }

            

        public static void Init()
        {
            Kernel = new StandardKernel(new IoCModule());
            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(Kernel));

        }
        
    }
}
