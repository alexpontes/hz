﻿/* Scrits to site */

$(document).ready(function () {
    //menu
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled", 600);

    });

    //tabs logins
    $('#loginTabs li a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')

        if ($(".modelo").hasClass("active")) {
            $("#modeloShow").removeClass("hidden");
        } else {
            $("#modeloShow").addClass("hidden");
        }
    });

    $('.dataInicial').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });

    $('.dataFinal').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });

    $('.dataNascimento').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });

    //datepicker
    //$('#DataInicio').datepicker({
    //    format: "dd/mm/yyyy",
    //    language: "pt-BR"
    //});

    //$('#DataFim').datepicker({
    //    format: "dd/mm/yyyy",
    //    language: "pt-BR"
    //});


    //$('#DataNascimentoDadosPessoais').datepicker({
    //    format: "dd/mm/yyyy",
    //    language: "pt-BR"
    //});
        
    //$('#DataNascimento').datepicker({
    //    format: "dd/mm/yyyy",
    //    language: "pt-BR"
    //});

    //collapse
    $('#formModelo').collapse();

    //icon toggle form modelo

});

