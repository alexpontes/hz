﻿$(function () {
    var _util = new Util();
    $('.btnVisualizarPerfil').click(function () {
        var id = $(this).attr('data-item'); 
        carregarDetalhesModelo(id);
    });
    $('#modelosCasting').click(function () {
        debugger;
        var id = $(this).attr('data-item');
        carregarModelosSelecionadas(id);
    });

    function carregarDetalhesModelo(idModelo) {
        var div = $('#detalhesModeloComposite');
        var url = '/Modelo/ModeloComposite/' + idModelo;
        $('#detalhesModeloComposite div').remove();
        _util.request(url).then(function (result) {
            $(result).appendTo(div);
            $('.modal-composite').modal('show');
             
        });
    }

    function carregarModelosSelecionadas(idCasting) {
        var div = $('#modelosSelecionadas');
        var obj = { 'idCasting': idCasting };
        $('#modelosSelecionadas div').remove();
        var url = '/Modelo/ModeloSelecionadasCasting';
        _util.request(url, 'GET', obj).then(function (result) {
            $(result).appendTo(div);
            $('.modal-modelos').modal('show');

        });
    }


});

$(document).ready(function () {
    $('#thumbCarousel').carousel({
        interval: 3000
    })
});

/* affix the Carousel Buttons to Carousel Item on scroll */
$('.nav-carousel').bind({
    offset:
	{
	    top: $('#thumbCarousel').height() - $('.nav-carousel').height()
	}
});

$(document).ready(function () {
    var carouselContainer = $('.carousel');
    var slideInterval = 2500;

    $('#carousel').carousel({
        interval: false
    });

    var clickEvent = false;
    $('#thumbCarousel').on('click', '.nav-carousel a', function () {
        clickEvent = true;
        $('.nav-carousel li').removeClass('active');
        $(this).parent().addClass('active');
    }).on('slid.bs.carousel', function (e) {
        if (!clickEvent) {
            var count = $('.nav-carousel').children().length - 1;
            var current = $('.nav-carousel li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if (count == id) {
                $('.nav-carousel li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
});