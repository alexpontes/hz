﻿

var Util = function () { };

Util.prototype.createImageFromUrl = function (imageUrl) {
    var defer = Promise.defer();

    var xhr = new XMLHttpRequest();
    xhr.open("GET", imageUrl, true);

    xhr.responseType = "arraybuffer";

    xhr.onload = function (e) {

        var arrayBufferView = new Uint8Array(this.response);
        var blob = new Blob([arrayBufferView], { type: "image/jpeg" });
        var obj = { blob: blob, fileName: imageUrl };
        defer.resolve(obj);
    };

    xhr.send();
    return defer.promise;

};

Util.prototype.request = function (url, type, data, contentType, dataType, processData, async) {
    var queryString = "";
    type = type || "GET";
    if (type.toUpperCase() === "GET" && (typeof (data) !== "undefined" && typeof (data) !== null))
        url += objToQueryString(data);

    var defer = Promise.defer();
    var options = getAjaxOptions(url, type, data, contentType, dataType, processData, async, defer);

    $.ajax(options);
    return defer.promise;

};

Util.prototype.getObjectsForm = function (form) {
    var obj = {};
    form.each(function (index, element) {
        var idElement = $(element).attr('id');
        var valueElement = $(element).val();
        obj[idElement] = valueElement;
    });

    return obj;
};

Util.prototype.getImageData = function (inputFile, name, formName) {
    var imageData = new FormData(); 
    var files = inputFile.get(0).files; 
    imageData.append(formName, files[0], name);

    return imageData;
     
};

//helpers
function getAjaxOptions(url, type, data, contentType, dataType, processData, async, defer) {
    var options = {
        url: url,
        type: type,

        success: function (result) {
            defer.resolve(result);
        },
        error: function (result) {
            defer.reject(result);
        }
    };
    if (typeof (contentType) !== "undefined") options.contentType = contentType;
    if (typeof (dataType) !== "undefined") options.dataType = dataType;
    if (typeof (data) !== "undefined") options.data = data;
    if (typeof (processData) !== "undefined") options.processData = processData;
    if (typeof (async) !== "undefined") options.async = async;

    return options;
}

function objToQueryString(obj) {
    var queryString = "";
    for (var key in obj) {
        var value = obj[key];
        queryString += key + "=" + value + "&";
    }

    queryString = "?" + queryString.substr(0, queryString.length - 1);
    return queryString;
}

