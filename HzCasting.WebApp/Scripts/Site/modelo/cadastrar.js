﻿var _util = new Util();
$(function () {
    $("#btn-usuario").click(function (e) {
        e.preventDefault();
        cadastrarUsuario();
    });

    $("#btn-dadosCadastrais").click(function (e) {
        e.preventDefault();

        dadosPessoais();
    });

    $("#btn-imagem").click(function (e) {
        var file = $('#arquivo-file');
        var userId = $("#userId").val();
        if (!userId) {
            alert('cadastre um usuario para inserir uma foto');
            return;
        }
        var name = new Date().toISOString() + '-foto-modelo-perfil:userId' + userId;
        var imgData = _util.getImageData(file, name, 'imagem');
        //var imgData = new FormData();
        //var files = file.get(0).files;
        //imgData.append('imagem', files[0], name);
        cadastrarFotoPerfilModelo(imgData);
    });

    $("#btn-endereco").click(function (e) {
        e.preventDefault();
        endereco();
    });

    $("#btn-contato").click(function (e) {
        e.preventDefault();
        contato();
    });

    $("#btn-documento").click(function (e) {
        e.preventDefault();
        documentos();
    });

    $("#btn-dadosbancarios").click(function (e) {
        e.preventDefault();
        dadosBancarios();
    });

    $("#btn-escolaridade").click(function (e) {
        e.preventDefault();
        escolaridade();
    });

    $("#btn-idioma").click(function (e) {
        e.preventDefault();
        idiomas();
    });

    $("#btn-caracteristicas").click(function (e) {
        e.preventDefault();
        caracteristicas();
    });

    $("#btn-dadoscomplementares").click(function (e) {
        e.preventDefault();
        dadosComplementares();
    });

    function cadastrarUsuario() {
        var form = $("#form-usuario").serializeObject();

        iterative.post('/Modelo/CadastrarUsuario', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            console.log(data);
            $("#userId").val(data);
            $("#form-usuario :input").attr("disabled", true);
            alert('Usuário criado com sucesso!');
        });
    }

    function dadosPessoais() {
        debugger;
        var form = $("#form-dadoscadastrais").serializeObject();
        //UsuarioId
        form.UsuarioId = $("#userId").val();
        form.ModeloId = $('#modeloId').val();
        iterative.post('/Modelo/InserirDadosCadastrais', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            console.log(data);
            $("#modeloId").val(data);
            if (!paginaEdicao())
                $("#form-dadoscadastrais :input:not(#arquivo-file):not(#btn-imagem)").attr("disabled", true);

            alert('Salvo com sucesso!');
        });
    }

    function cadastrarFotoPerfilModelo(imageData) {

        _util.request('/Modelo/InserirFoto', 'POST', imageData, false, undefined, false)
            .then(function (result) {
                if (result.error) {
                    alert(result.error);
                    return;
                }
                alert('Foto Inserida com sucesso !');
            });
    }

    function endereco() {
        var modeloId = $("#modeloId");

        var form = _util.getObjectsForm($("#form-endereco input"));

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#modeloId").val();

        iterative.post('/Modelo/InserirEndereco', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }

            if (!paginaEdicao())
                $("#form-endereco :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });

    }

    function contato() {
        var modeloId = $("#modeloId");

        var form =  $("#form-contato").serializeObject();

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#modeloId").val();

        iterative.post('/Modelo/InserirContato', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            if (!paginaEdicao())
                $("#form-contato :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });
    }

    function documentos() {
        var modeloId = $("#modeloId");

        var form = $("#form-documentos").serializeObject();

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#modeloId").val();

        iterative.post('/Modelo/InserirDocumentos', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }

            if (!paginaEdicao())
                $("#form-documentos :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });
    }

    function dadosBancarios() {
        var modeloId = $("#modeloId");

        var form = $("#form-dadosbancarios").serializeObject();

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#modeloId").val();

        iterative.post('/Modelo/InserirDadosBancarios', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }

            if (!paginaEdicao())
                $("#form-dadosbancarios :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });
    }

    function escolaridade() {
        var modeloId = $("#modeloId");

        var form = $("#form-escolaridade").serializeObject();

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#modeloId").val();

        iterative.post('/Modelo/InserirEscolaridade', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            if (!paginaEdicao())
                $("#form-escolaridade :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });
    }

    function idiomas() {
        var modeloId = $("#modeloId");

        var form = $("#form-idioma").serializeObject();

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#modeloId").val();

        iterative.post('/Modelo/InserirIdioma', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            if (!paginaEdicao())
                $("#form-idioma :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });
    }

    function caracteristicas() {
        var modeloId = $("#modeloId");

        var form = $("#form-caracteristica").serializeObject();

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#modeloId").val();

        iterative.post('/Modelo/InserirCaracteristicas', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }

            if (!paginaEdicao())
                $("#form-caracteristica :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });
    }

    function dadosComplementares() {
        var modeloId = $("#modeloId");

        var form = $("#form-dadoscomplementares").serializeObject();

        form.UsuarioId = $("#userId").val();
        form.ModeloId = $("#ModeloId").val();

        iterative.post('/Modelo/InserirDadosComplementares', form, function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }

            if (!paginaEdicao())
                $("#form-dadoscomplementares :input").attr("disabled", true);
            alert('Salvo com sucesso!');
        });
    }
    function paginaEdicao() {
        return iterative.getUrlContext().search(/editar/g) >= 0;
    }
});