﻿var _util = new Util();

$(function () {

    var btn_idioma = $("#btn-idioma");

    btn_idioma.click(function (e) {

        e.preventDefault();

        var idiomas = $("#idiomas input:checked");

        var obj = new Object();
        
        obj.ModeloId = $("#ModeloId").val();
        obj.Idiomas = new Array();

        idiomas.each(function () {
            var item = $(this);
          
            var idiomaObjeto = new Object();
            idiomaObjeto.IdiomaId = item.attr('name');
          
            obj.Idiomas.push(idiomaObjeto);
                     
        });

        iterative.post('/Modelo/CadastrarIdioma', obj, function (data) { alert('Sucesso'); });
    });

    $("#btn-imagem").click(function (e) {
        var file = $('#arquivo-file');
        var userId = $("#ModeloId").val();
        if (!userId) {
            alert('cadastre um usuario para inserir uma foto');
            return;
        }
        var name = new Date().toISOString() + '-foto-modelo-perfil:userId' + userId;
        var imgData = _util.getImageData(file, name, 'imagem');
        //var imgData = new FormData();
        //var files = file.get(0).files;
        //imgData.append('imagem', files[0], name);
        cadastrarFotoPerfilModelo(imgData);
    });

    function cadastrarFotoPerfilModelo(imageData) {

        _util.request('/Modelo/InserirFoto', 'POST', imageData, false, undefined, false)
            .then(function (result) {
                if (result.error) {
                    alert(result.error);
                    return;
                }
                alert('Foto Inserida com sucesso !');
            });
    }
});