﻿

var section = {
    init: function () {

        var cep = $("#CepEndereco");

        $(cep).blur(section.carregar_cep);

        $("#Email").blur(function () {

            var _email = $('#Email').val();

            if (_email != "") {
                if (!section.checkEmail($('#Email'))) {
                    vCheck = false;

                    $('.alertEmail').modal('show')
                }
            }
        });

        $('.cpfModelo').blur(function () {

            var _cpfModelo = $('.cpfModelo').val();

            var _valid_cpf = section.valida_cpf($('.cpfModelo').val());

            if (_cpfModelo != "") {

                if (_valid_cpf) {
                    $('.salvarModelo').removeAttr('disabled');
                } else {
                    $('.alertCPF').modal('show');
                    $('.salvarModelo').attr('disabled', 'disabled');
                }
            }
        });

        $('#DataNascimentoDadosPessoais').blur(function () {

            var _valid_dataNascimento = section.validaDat($('#DataNascimentoDadosPessoais').val());

            if (_valid_dataNascimento) {
                $('.salvarDadosPessoais').removeAttr('disabled');
            } else {
                $('.salvarDadosPessoais').attr('disabled', 'disabled');
            }

        });

        $('#Cpf').blur(function () {

            var _cpf = $('#Cpf').val();
            var _valid_cpf = section.valida_cpf($('#Cpf').val());

            if (_cpf != "") {

                if (_valid_cpf) {
                    $('.salvarUsuario').removeAttr('disabled');
                } else {
                    $('.salvarUsuario').attr('disabled', 'disabled');
                    $('.alertCPF').modal('show');
                }
            }

        });

    },
    valida_cpf: function (cpf) {
        var numeros, digitos, soma, i, resultado, digitos_iguais;

        digitos_iguais = 1;

        cpf = cpf.replace(/\./g, '');
        cpf = cpf.replace(/\-/g, '');

        if (cpf.length < 11)
            return false;
        for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais) {
            numeros = cpf.substring(0, 9);
            digitos = cpf.substring(9);

            soma = 0;
            for (i = 10; i > 1; i--)
                soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            numeros = cpf.substring(0, 10);
            soma = 0;
            for (i = 11; i > 1; i--)
                soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;
            return true;
        }
        else
            return false;
    },
    checkEmail: function (obj) {

        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(obj).val())) return true;
        else return false;

    },
    checkBlank: function (obj) {
        if ($(obj).val() != '') return true;
        else return false;
    },
    carregar_cep: function () {

        var numeroCep = $("#CepEndereco").val().replace('-', '');

        iterative.endereco(numeroCep, function (data) {

            var cidade = $("#CidadeEndereco").val(data.localidade);
            var estado = $("#EstadoEndereco").val(data.uf);
            var logradouro = $("#LogradouroEndereco").val(data.logradouro);
            var complemento = $("#ComplementoEndereco").val(data.complemento);
            var bairro = $("#BairroEndereco").val(data.bairro);

            $("#NumeroEndereco").focus();

            console.log(data)
        });

    },
    validaDat: function () {
        var campo = $('#DataNascimentoDadosPessoais').val();
        var valor = campo;

        var date = valor;
        var ardt = new Array;
        var ExpReg = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
        ardt = date.split("/");

        erro = false;
        if (date.search(ExpReg) == -1) {
            erro = true;
        }
        else if (((ardt[1] == 4) || (ardt[1] == 6) || (ardt[1] == 9) || (ardt[1] == 11)) && (ardt[0] > 30))
            erro = true;
        else if (ardt[1] == 2) {
            if ((ardt[0] > 28) && ((ardt[2] % 4) != 0))
                erro = true;
            if ((ardt[0] > 29) && ((ardt[2] % 4) == 0))
                erro = true;
        }
        if (erro) {

            $('.alertDataNascimento').modal('show');

            $('#DataNascimentoDadosPessoais').val('');

            return false;
        }
        return true;
    }
};
$(document).ready(section.init);








//$(function () {

//    var cep = $("#CepEndereco");

//    var cidade = $("#CidadeEndereco");
//    var estado = $("#EstadoEndereco");
//    var logradouro = $("#LogradouroEndereco");
//    var complemento = $("#ComplementoEndereco");
//    var bairro = $("#BairroEndereco");

//    cep.blur(function () {

//        var numeroCep = cep.val().replace('-', '');

//        iterative.endereco(numeroCep, function (data) {

//            cidade.val(data.localidade);
//            estado.val(data.uf);
//            logradouro.val(data.logradouro);
//            complemento.val(data.complemento);
//            bairro.val(data.bairro);

//            console.log(data)
//        });
//    });
//});