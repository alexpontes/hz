﻿//$(function () {
var _imagensSelecionadas = [];
var _imagemUpload = null;
var _util = new Util();
$("img[name=img-selection]").on("click", function () {
    var imagem = $(this).attr('src');
    //_imagensSelecionadas.push(imagem);
    adicionarImagens(imagem);
    $(this).css("border", "10px solid yellow");
});

$('#arquivo-file').change(function () {
    var fileInput = $('#arquivo-file')[0];
    var file = fileInput.files[0];
    _imagemUpload = file;
});

$("#btnSalvar").click(function () {
    console.log('_imagensSelecionadas', _imagensSelecionadas);
    console.log('_imagemUpload', _imagemUpload);
    enviarImagens(_imagensSelecionadas, _imagemUpload);

});
function adicionarImagens(imagem) {
    var defer = Promise.defer();
    _util.createImageFromUrl(imagem)
        .then(function onSuccess(result) {
            _imagensSelecionadas.push(result);
            defer.resolve();
        });

    return defer.promise;
}



function enviarImagens(imagensSelecionadas, imagemUpload) {
    var formData = new FormData();
    debugger;
    imagensSelecionadas.forEach(function (value) {
        $("img[src='" + value.fileName + "']").css("border", "10px solid red");
        formData.append("imagens", value.blob, value.fileName);
    });
    if (imagemUpload !== null) formData.append('imagens', imagemUpload);
    salvar(formData);

}
function salvar(formData) {
    $.ajax({
        type: "POST",
        url: '/Modelo/InserirImagens',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            console.log('success!', response);
            _imagensSelecionadas = [];
            esconderMensagens();
            if (response.error) {
                $("#saida-gravar-imagem").text("Erro ao gravar fotos !").css('color', 'red');
                return;
            }

            $("#saida-gravar-imagem").text("Fotos gravadas com sucesso !").css('color', 'red');
        },
        error: function (error) {
            console.error('erro!', error);
        }
    });
}

function esconderMensagens() {
    setTimeout(function () {
        $("#saida-gravar-imagem").text("").css('color', 'red');

    }, 5000);
}


//});
