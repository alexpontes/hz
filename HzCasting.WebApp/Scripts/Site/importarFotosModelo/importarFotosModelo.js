﻿$(function () {
    var _util = new Util();
    var _skip = 0;
    var PADRAO_ITEMS = 1;
    var _imagensSelecionadas = [];
    var _imagemUpload = null;
    var _idUsuario = $("#usuarioId").val();


    $('#page-selection').bootpag({
        total: 10
    }).on("page", function (event, num) {
        hideContent();
        carregarFotos(num);
    });
    (function init() {
        hideContent();
        carregarFotos(0); 
    })();

    function inicializarEventos() {
        $("img[name=img-selection]").on("click", function () {
            var imagem = $(this).attr('src'); 
            adicionarImagens(imagem);
            $(this).css("border", "10px solid yellow");
        });

        $('#arquivo-file').change(function () {
            var fileInput = $('#arquivo-file')[0];
            var file = fileInput.files[0];
            _imagemUpload = file;
        });

        $("#btnSalvar").click(function () {
            console.log('_imagensSelecionadas', _imagensSelecionadas);
            console.log('_imagemUpload', _imagemUpload);
            enviarImagens(_imagensSelecionadas, _imagemUpload);

        });

    }

    function getHtmlTemplateImage() {
        return ' <img src="IMG-SRC" class="img-thumbnail" data-id="IMG-ID" name="img-selection"  />';
    }

    function mapTemplateItens(result, text) {
        return result.map(function (item) {
            return text.replace(/IMG-SRC/g, item.ImageURL)
                        .replace(/IMG-ID/g, item.Id);
        });
    }

    function carregarFotos(pageNumber) {
        if (pageNumber === 1)
            _skip = pageNumber;
        else
            _skip = pageNumber * 10;

        var url = '/Modelo/CarregarFotosFacebookModelo';
        var data = {
            idUsuario: _idUsuario,
            take: 10,
            skip: _skip
        };
        _util.request(url, 'POST', data).then(function (result) {
            $('#loading').hide();
            if (result.error) {
                alert('erro ao carregar fotos');
                return;
            }
            if (result.length === 0)
                return;

            $("#page-selection").show();

            var text = getHtmlTemplateImage();
            var txtHtml = mapTemplateItens(result, text);
            $("#content").html(txtHtml);
            
            showContent();
            inicializarEventos();
        });
    }

    function showContent() {
        $('#loading').hide();
        $("#content").show();
    }

    function hideContent() {
        $('#loading').show();
        $("#content").hide();
    }

   
    function adicionarImagens(imagem) {
        var defer = Promise.defer();
        _util.createImageFromUrl(imagem)
            .then(function onSuccess(result) {
                _imagensSelecionadas.push(result);
                defer.resolve();
            });

        return defer.promise;
    }
     
    function enviarImagens(imagensSelecionadas, imagemUpload) {
        var formData = new FormData();
        debugger;
        imagensSelecionadas.forEach(function (value) {
            $("img[src='" + value.fileName + "']").css("border", "10px solid red");
            formData.append("imagens", value.blob, value.fileName);
        });
        if (imagemUpload !== null) formData.append('imagens', imagemUpload);
        salvar(formData);

    }

    function salvar(formData) {
        $.ajax({
            type: "POST",
            url: '/Modelo/InserirImagens',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                console.log('success!', response);
                _imagensSelecionadas = [];
                esconderMensagens();
                if (response.error) {
                    $("#saida-gravar-imagem").text("Erro ao gravar fotos !").css('color', 'red');
                    return;
                }

                $("#saida-gravar-imagem").text("Fotos gravadas com sucesso !").css('color', 'red');
            },
            error: function (error) {
                console.error('erro!', error);
            }
        });
    }

    function esconderMensagens() {
        setTimeout(function () {
            $("#saida-gravar-imagem").text("").css('color', 'red');

        }, 5000);
    }


    //function carregarFotos() {
    //    _util.
    //}

});