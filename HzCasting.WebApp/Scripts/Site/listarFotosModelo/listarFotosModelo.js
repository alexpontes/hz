﻿$(function () {
    var _util = new Util();
    var _skip = 0;
    var PADRAO_ITEMS = 1;
    var _usuarioId = $('#usuarioId').val();
    var _isBackOffice = $('#modelo').val(); 
    $('#page-selection').bootpag({
        total: 10
    }).on("page", function (event, num) {
        hideContent();
        toggleLoading();
        carregarFotos(num);
    });


    (function init() {
        hideContent();
        carregarFotos(0);
        iniciarEventos();
    })();

    function iniciarEventos() {
        $('input[name=btn-remover-imagem]').click(function () {

            var id = $(this).parent().find('img').attr('data-id');
            removerImagemModelo(id);
        });
        $('input[name=btn-selecionar-imagem]').click(function () {
            var id = $(this).parent().find('img').attr('data-id');
            alterarImagemModelo(id);
        });

        $('input[name=btn-aprovar-imagem]').click(function () {
            aprovarImagem(this);
        });
    }

    function removerImagemModelo(idFoto) {
        $('#loading').show();
        var url = '/Modelo/RemoverFoto';
        var data = { usuarioId: _usuarioId, idFoto: idFoto };
        _util.request(url, 'POST', data).then(function (result) {
            $('#loading').hide();
            if (result.error) {
                alert('não foi possivel remover a imagem');
                return;
            }
            $('img[data-id=' + idFoto + ']').parent().remove();
        });
    }

    function alterarImagemModelo(idFoto) {
        $('#loading').show();
        var url = '/Modelo/AlterarFoto';
        var data = { usuarioId: _usuarioId, idFoto: idFoto };

        _util.request(url, 'POST', data).then(function (result) {
            $('#loading').hide();
            if (result.error) {
                alert('não foi possivel selecionar a imagem');
                return;
            }
            toggleImagemSelecionada(idFoto);
        });
    }

    function aprovarImagem(elemento) {
        var idFoto = $(elemento).parent().find('img').attr('data-id');

        var url = '/Modelo/AprovarFoto';
        var data = { usuarioId: _usuarioId, idFoto: idFoto };
        _util.request(url, 'POST', data).then(function (result) {
            if (result.error) {
                alert('Não foi possivel aprovar a foto selecionada');
                return;
            }

            $(elemento).attr('disabled', 'disabled');
            showContent();
        });
    }

    function toggleImagemSelecionada(idFoto) {
        $('input[name=btn-selecionar-imagem]').removeAttr('disabled');
        $('img[data-id=' + idFoto + ']')
                    .parent()
                    .find('input[name=btn-selecionar-imagem]')
                    .attr('disabled', 'disabled');
    }
    function toggleLoading() {

        $('#loading').toggleClass('hide');
    }
    function showContent() {
        toggleLoading();
        $("#content").show();
        $('#quantidadeItens').show();

    }

    function hideContent() {
        toggleLoading();
        $("#content").hide();
    }

    function getHtmlTemplateImage() {

        return '<div>\
                        <input type="button" class="btn btn-primary" value="Remover" name="btn-remover-imagem" />\
                        INPUT-ADICIONAL\
                        <img src="IMG-SRC" class="img-thumbnail" data-id="IMG-ID" name="img-selection"  />\
                    </div>';
    }

    function mapTemplateItens(result, text) {
        var inputModelo = '<input type="button" class="btn btn-primary"  ATTR-DISABLED value="Para Perfil"  name="btn-selecionar-imagem"/>';
        var inputBackOffice = '<input type="button" class="btn btn-primary" value="Aprovar foto"  name="btn-aprovar-imagem" ATTR-DISABLED />'

        return result.map(function (item) {
            var disabled = false;
            console.log('item.Status ' + item.Status, 'item.Principal' + item.Principal);
            if (typeof (item.Status) !== "undefined") {
                disabled = item.Status.toLowerCase() === "pendente";
            }
            else if (typeof (item.Principal) !== "undefined") {
                disabled = item.Principal;
            }

            return text.replace(/IMG-SRC/g, item.ImageURL)
                        .replace(/INPUT-ADICIONAL/g, _isBackOffice ? inputBackOffice : inputModelo)
                        .replace(/IMG-ID/g, item.Id)
                        .replace(/ATTR-DISABLED/g, item.Principal ? "disabled" : "");
        });
    }

    function carregarFotos(pageNumber) {
        if (pageNumber === 1)
            _skip = pageNumber;
        else
            _skip = pageNumber * 10;

        var url = '/Modelo/CarregarFotosModelo';
        var data = {
            idUsuario: _usuarioId,
            take: 10,
            skip: _skip
        };
        _util.request(url, 'POST', data).then(function (result) {
            $('#loading').hide();
            if (result.error) {
                alert('erro ao carregar fotos');
                return;
            }
            var text = getHtmlTemplateImage();
            var txtHtml = mapTemplateItens(result.data, text);
            $("#content").html(txtHtml);
            $('#quantidadePendentes').val(result.quantidadePendentes);

            showContent();
            iniciarEventos();
        });
    }

});