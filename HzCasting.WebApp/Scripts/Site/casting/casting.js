﻿/*TELA REFEITA, VERIFICAR e REMOVER METODOS INUTILIZADOS*/
$(function () {
    var _util = new Util();
    var URL_BASE_CASTING = '/Casting';
    var KEYS = { 'clientes': 'clientes', 'eventos': 'eventos', 'agencias': 'agencias', 'agendas': 'agendas' };

    var spanCampoObrigatorio = '<span id="error" class="text-danger">Campo Obrigatório!</span>';

    $("#imagem-icon").click(function () {
        $("#ImagemEvento").click().show();
    });



    $("#btnSalvar").click(function (e) {
        debugger;
        var form = $('#form-casting');
        var isValid = validarFormulario(form);
        if (!isValid) return false;

        isValid = validarAutoCompletes();
        if (!isValid) return false;
        popularCasting(form);
        //resetForms(form);
        //removerAgendasLocais();
        ocultarInputImagem();
        $("#form-casting").submit();

    });

    $("#btnSalvarPorCliente").click(function (e) {

        var form = $('#form-casting');
        var isValid = validarFormulario(form);
        if (!isValid) return false;

        popularCasting(form);
        //resetForms(form);
        //removerAgendasLocais();
        ocultarInputImagem();
        $("#form-casting").submit();

    });


    (function init() {
        //popularAgendaExistente();
        carregarItensCliente();
        carregarItensEvento();
        carregarItensAgencia();
    })();

    function ocultarInputImagem() {
        $("#ImagemEvento").hide();

    }

    function validarAutoCompletes() {
        var itens = obterValoresAutoCompletes();
        var evento = obterEvento(itens.evento);
        if (Object.keys(evento).length === 0) return false;

        var cliente = obterCliente(itens.cliente);
        if (Object.keys(cliente).length === 0) return false;

        var agencia = obterAgencia(itens.agencia);
        if (Object.keys(agencia).length === 0) return false;

        return true;

    }

    function obterEvento(nomeEvento) {
        var valor = obterEventosLocais().filter(function (item) {
            return item.nome == nomeEvento;
        });
        if (valor.length === 0)
            return {};

        return valor[0];
    }

    function obterCliente(nomeCliente) {
        var valor = obterClientesLocais().filter(function (item) {
            return item.nome == nomeCliente;
        });
        if (valor.length === 0)
            return [];

        return valor[0];
    }

    function obterAgencia(nomeAgencia) {

        var valor = obterAgenciasLocais().filter(function (item) {
            return item.nome == nomeAgencia;
        });
        if (valor.length === 0)
            return [];

        return valor[0];
    }

    function obterValoresAutoCompletes() {
        var evento = $('#NomeEvento').val();
        var agencia = $('#NomeAgencia').val();
        var cliente = $('#NomeCliente').val();
        return { evento: evento, agencia: agencia, cliente: cliente };
    }

    function carregarItensAgencia() {
        var url = URL_BASE_CASTING + "/CarregarAutoCompleteAgencia";

        _util.request(url).then(function (result) {
            if (result.error) {
                alert('Erro ao carregar agendas');
                return;
            }
            mapAgencias(result);
        });
    }

    function carregarItensCliente() {
        var url = URL_BASE_CASTING + "/CarregarAutoCompleteCliente";

        _util.request(url).then(function (result) {
            if (result.error) {
                alert('Erro ao carregar eventos');
                return;
            }
            mapClientes(result);
        });
    }

    function carregarItensEvento() {
        var url = URL_BASE_CASTING + "/CarregarAutoCompleteEvento";
        _util.request(url).then(function (result) {
            if (result.error) {
                alert('Erro ao carregar eventos');
                return;
            }
            mapEventos(result);

        });

    }

    function mapAgencias(result) {
        guardarAgenciasLocais(result);
        debugger;
        var agencias = result.map(function (item) {
            if (item.nome != null) return item.nome

            return "";
        });
        nomeAgenciaAutoComplete(agencias);
    }

    function mapEventos(result) {
        debugger;
        guardarEventosLocais(result);
        var eventos = result.map(function (item) {
            return item.nome
        });
        nomeEventoAutoComplete(eventos);
    }

    function mapClientes(result) {
        guardarClientesLocais(result);
        var clientes = result.map(function (item) {
            return item.nome
        });
        nomeClienteAutoComplete(clientes);
    }


    function nomeAgenciaAutoComplete(items) {
        var agencia = $('#nomeAgenciaDefault').val();
        $('#NomeAgencia').typeahead({
            name: 'agencias',
            local: items,
            limit: 10
        })

        .on('typeahead:selected', function (event, selection) {

            console.log('agencia', selection.value);
        });
        if (agencia) {
            $('#NomeAgencia').val(agencia);
        }
        debugger;

    }

    function nomeClienteAutoComplete(items) {
        var cliente = $('#nomeClienteDefault').val();
        $('#NomeCliente').typeahead({
            name: 'clientes',
            local: items,
            limit: 10
        }).on('typeahead:selected', function (event, selection) {
            console.log('cliente', selection.value);
        });
        if (cliente) {
            $('#NomeCliente').val(cliente);
        }
    }

    function nomeEventoAutoComplete(eventos) {

        var evento = $('#nomeTituloDefault').val();

        $('#NomeEvento').typeahead({
            name: 'eventos',
            local: eventos,
            limit: 10
        }).on('typeahead:selected', function (event, selection) {
            console.log('eveto', selection.value);
        });
        if (evento) {
            $('#NomeEvento').val(evento);
        }
    }

    function autoCompletes() {
        nomeAgenciaAutoComplete();
        nomeClienteAutoComplete();
        nomeEventoAutoComplete();

    }

    function popularCasting(form) {
        var itens = obterValoresAutoCompletes();
        var evento = obterEvento(itens.evento);
        var cliente = obterCliente(itens.cliente);
        var agencia = obterAgencia(itens.agencia);
        var imagem = $('#ImagemEvento');

        var form = form.find('input:not([type=submit]):not([type=button]), select');
        var obj = _util.getObjectsForm(form);

        obj.idEvento = evento.id;
        obj.idCliente = cliente.id;
        obj.idAgencia = agencia.id;

        $("input[name=IdEvento]").val(evento.id);
        $("input[name=IdCliente]").val(cliente.id);
        $("input[name=IdAgencia]").val(agencia.id);


    }


    function guardarAgenciasLocais(obj) {
        setLocalData(KEYS.agencias, obj)
    }

    function obterAgenciasLocais() {
        return getLocalData(KEYS.agencias);
    }


    function removerAgenciasLocais() {
        deleteLocalData(KEYS.clientes);
    }

    function guardarClientesLocais(obj) {
        setLocalData(KEYS.clientes, obj)
    }

    function obterClientesLocais() {
        return getLocalData(KEYS.clientes);
    }


    function removerClientesLocais() {
        deleteLocalData(KEYS.clientes);
    }

    function guardarEventosLocais(obj) {

        setLocalData(KEYS.eventos, obj);
    }

    function guardarAgenciasLocais(obj) {

        setLocalData(KEYS.agencias, obj);
    }

    function obterEventosLocais() {
        return getLocalData(KEYS.eventos);
    }

    function removerEventosLocais() {
        deleteLocalData(KEYS.eventos);
    }


    function setLocalData(key, data) {
        localStorage[key] = JSON.stringify(data);
    }

    function getLocalData(key) {
        return JSON.parse(localStorage[key] || '[]');
    }

    function deleteLocalData(key) {
        localStorage[key] = '[]';

    }


    function resetForms(form) {
        var inputs = form;
        inputs.each(function (e) {
            this.reset();
        });
    }

    function validarFormulario(form) {
        var itensPendentes = 0;
        var inputs = form.find("input[required='required'], select[required='required']");
        inputs.each(function (value) {
            $(this).parent().find('#error').remove();
            var val = $(this).val();
            if (!(val === "" || val === "0"))
                return;


            itensPendentes += 1;
            $(this).css('border', 'red solid 1px');
            $(this).after(spanCampoObrigatorio);

        });
        var formularioValido = itensPendentes === 0;
        return formularioValido;
    }


});