﻿

var section = {
    init: function () {
        var cep = $("#Cep");

        $(cep).blur(section.carregar_cep);
    },
    carregar_cep: function () {

        var numeroCep = $("#Cep").val().replace('-', '');

        iterative.endereco(numeroCep, function (data) {

            var cidade = $("#Cidade").val(data.localidade);
            var estado = $("#Estado").val(data.uf);
            var logradouro = $("#Logradouro").val(data.logradouro);
            var complemento = $("#Complemento").val(data.complemento);
            var bairro = $("#Bairro").val(data.bairro);

            $("#Numero").focus();

            console.log(data)
        });

    },
};
$(document).ready(section.init);