﻿

var section = {
    init: function () {
        $('#Cpf').blur(function () {

            var _valid_cpf = section.valida_cpf($('#Cpf').val());
            var _cpf = $('#Cpf').val();


            if (_cpf != "") {

                if (_valid_cpf) {
                    $('.salvarUsuario').removeAttr('disabled')
                    $('.editarUsuario').removeAttr('disabled')
                } else {
                    $('.alertCPF').modal('show');

                    $('.salvarUsuario').attr('disabled', 'disabled')
                    $('.editarUsuario').attr('disabled', 'disabled')
                }
            }


        });

        $("#Email").blur(function () {

            var _email = $('#Email').val();

            if (_email != "") {
                if (!section.checkEmail($('#Email'))) {
                    vCheck = false;

                    $('.alertEmail').modal('show');

                }
            }
        });

        $('#Telefone').blur(function () {

            var _telefone = $('#Telefone').val();

            if (_telefone != "") {
                if (!section.checkTelefoneCelular($('#Telefone'))) {

                    $('.alertNumeroTelefone').modal('show');
                    $('.salvarUsuario').attr('disabled', 'disabled');
                }
                else
                    $('.salvarUsuario').removeAttr('disabled', 'disabled');
            }
        });

        $('#Celular').blur(function () {

            var _celular = $('#Celular').val();

            if (_celular != "") {
                if (!section.checkTelefoneCelular($('#Celular'))) {

                    $('.alertNumeroTelefone').modal('show');
                    $('.salvarUsuario').attr('disabled', 'disabled');
                } else
                    $('.salvarUsuario').removeAttr('disabled', 'disabled');

            }
        });
    },
    valida_cpf: function (cpf) {
        var numeros, digitos, soma, i, resultado, digitos_iguais;

        digitos_iguais = 1;

        cpf = cpf.replace(/\./g, '');
        cpf = cpf.replace(/\-/g, '');

        if (cpf.length < 11)
            return false;
        for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais) {
            numeros = cpf.substring(0, 9);
            digitos = cpf.substring(9);

            soma = 0;
            for (i = 10; i > 1; i--)
                soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            numeros = cpf.substring(0, 10);
            soma = 0;
            for (i = 11; i > 1; i--)
                soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;
            return true;
        }
        else
            return false;
    },
    checkEmail: function (obj) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(obj).val())) return true;
        else return false;
    },
    checkTelefoneCelular: function (obj) {

        if (/^\([1-9]{2}\) [9]{0,1}[6-9]{1}[0-9]{3}\-[0-9]{4}/.test($(obj).val())) return true;
        else return false;

    }
};
$(document).ready(section.init)