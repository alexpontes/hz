﻿var iterative = {

    popularCombo: function (url, destino, valor, propVal, propDisplay) {

        if (!propVal) {
            propVal = 'value';
        }

        if (!propDisplay) {
            propDisplay = 'display';
        }

        $.ajax({
            url: url,
            dataType: "json",
            success: function (data) {

                $("#" + destino).empty();

                $("#" + destino).append($("<option value=''>Selecione...</option>"));

                $.each(data, function () {
                    $("#" + destino).append($("<option></option>").val(this[propVal]).html(this[propDisplay]));
                });

                $("#" + destino).val(valor).change();
            }
        });
    },

    post: function (url, obj, callback) {
        $.ajax({
            type: "POST",
            url: url,
            contentType: 'application/json',
            data: JSON.stringify(obj),
            datatype: "json",
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            }
        });
    },

    get: function (url, obj, callback, error) {
        $.ajax({
            url: url,
            data: obj,
            type: "GET",
            dataType: "json",
            success: function (data) {

                if (callback) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                console.log(xhr);

                if (error) {
                    error();
                }
            },
            complete: function (xhr, status) {
            }
        });
    },

    getHtml: function (url, obj, callback) {
        $.ajax({
            url: url,
            data: obj,
            type: "GET",
            dataType: "html",
            success: function (data) {

                if (callback) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                console.log(xhr);
            },
            complete: function (xhr, status) {
            }
        });
    },

    getHtmlOnPost: function (url, obj, callback) {
        $.ajax({
            url: url,
            contentType: 'application/json',
            data: JSON.stringify(obj),
            type: "POST",
            dataType: "html",
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            },
            error: function (xhr, status) {
                console.log(xhr);
            },
            complete: function (xhr, status) {
            }
        });
    },

    endereco: function(cep, callback){
        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        iterative.get(url, null, callback);
    },

    getUrlContext: function () {
        return location.href.toLowerCase();
    }
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};