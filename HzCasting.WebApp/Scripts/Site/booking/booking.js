﻿var _util = new Util();
$(function document() {

    init();
    function init() {
        initDataTable();
    };

    function initDataTable() {
        $('#tblModelos').DataTable({
            columnDefs: [{
                orderable: true,
                className: 'select-checkbox',
                targets: 0,
                className: 'dt-body-center',
                render: function (data, type, full, meta) {

                    var html_checkbox = "";

                    if ($('<div/>').text(full[5]).html().toLowerCase() != "") {
                        html_checkbox = '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '" disabled>';
                    }
                    else {
                        html_checkbox = '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">'
                    };

                    debugger;
                    
                                        
                    return html_checkbox;
                }
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
            order: [[1, 'asc']]
        });
    }
});

$('#btnEnviarInvites').click(function () {    
    enviarInvites();
});

$('.btnEnviarInvite').click(function () {
    var id = $(this).attr('data-item');
    debugger;
    var idCasting = $(this).parent().find('#IdCasting').val();
    var idModelo = id;
    var idBooking = $(this).parent().find('#idBooking').val(); 
    var idPerfil = $(this).parent().find('#IdPerfil').val();
    var modelos = [{ 'IdModelo': idModelo, 'idBooking': idBooking, 'idPerfil': idPerfil, 'IdCasting': idCasting }];
    enviar(modelos);
});

$('#selecionarTodos').change(function () {
    $('#tblModelos  input[type=checkbox]:not(#selecionarTodos)').each(function (index, element) {
        debugger;
        if (!this.disabled) {
            this.checked = !this.checked;
        }

    });
});


function enviarInvites() {
    var items = [];    
    $('.btnVisualizarPerfil').each(function (index, element) {
        debugger;

        var isChecked = $(element).parent().parent().find('input[type=checkbox]').is(':checked');
        if (!isChecked) return;
        
        var idCasting = $(this).parent().find('#IdCasting').val();
        var idBooking = $(this).parent().find('#idBooking').val();
        var idPerfil = $(this).parent().find('#IdPerfil').val();
        var idModelo = $(element).attr('data-item');
        var data = { IdBooking: idBooking, IdPerfil: idPerfil, IdModelo: idModelo, IdCasting: idCasting };
        debugger;
        items.push(data);
    });

    enviar(items);



}
function enviar(dados) {
     
    var url = '/Invite/DispararInvites'; 
    _util.request(url, 'POST', { dados: dados }).then(function (result) {
        if (result.success)
            location.reload();
        else
            alert('Erro ao enviar invites');

        console.log('result', result);
    });
}