﻿var section = {
    init: function () {



        $('input[type=file]').on("change", function () {
            section.carregar_foto();
        });

        $('.hide').on("click", function () {
            $(document.body).append($('<input />', { type: "file" }).change(verificaMostraBotao));
            $(document.body).append($('<img />'));
            $('.hide').hide();
        });

        section.mascaras();
        var cep = $("#CepNumeroEndereco");
        $(cep).blur(section.carregar_cep);

        $('#DataInicio').on('keyup change', section.validarDataInicioEvento);

        $('#DataFim').on('keyup change', section.validarDataFinalEvento);

        $('#HoraInicio').blur(function () {

            var _horarioInicio = $("#HoraInicio").val();

            if (_horarioInicio.length != 5 && _horarioInicio != "") {

                $('.alertHorarioMenor').modal('show');
                $('#HoraFim').attr('disabled', 'disabled');
                $('.inserir').attr('disabled', 'disabled');

            } else {
                $('.inserir').removeAttr('disabled');
                $('#HoraFim').removeAttr('disabled');
            }
        });

        $('#HoraFim').blur(section.validarHorarioEvento);

    },
    carregar_cep: function () {

        var numeroCep = $("#CepNumeroEndereco").val().replace('-', '');

        iterative.endereco(numeroCep, function (data) {

            var cidade = $("#CidadeEndereco").val(data.localidade);
            var estado = $("#EstadoEndereco").val(data.uf);
            var logradouro = $("#LogradouroEndereco").val(data.logradouro);
            var complemento = $("#ComplementoEndereco").val(data.complemento);
            var bairro = $("#BairroEndereco").val(data.bairro);

            $("#NumeroEndereco").focus();

            console.log(data)
        });
    },
    validarDataInicioEvento: function () {
        var _dataInicio = $("#DataInicio").val();
        var _dataFim = $("#DataFim").val();
        var _date = new Date();

        var curr_date = _date.getDate();
        var curr_month = _date.getMonth() + 1;
        var curr_year = _date.getFullYear();

        var _dataAtual = curr_date + "-" + curr_month + "-" + curr_year;

        setTimeout(function () {
            if (_dataAtual > _dataInicio && _dataInicio != "") {

                $('.alertDataAtual').modal('show');
                $('#DataFim').attr('disabled', 'disabled');
                $('.inserir').attr('disabled', 'disabled');
            }
            else {

                $('.inserir').removeAttr('disabled');
                $('#DataFim').removeAttr('disabled');
            }
        }, 300);

    },
    validarDataFinalEvento: function () {
        var _dataInicio = $("#DataInicio").val();
        var _dataFim = $("#DataFim").val();

        setTimeout(function () {

            if (_dataInicio > _dataFim && _dataFim != "") {
                $('.alertDataFim').modal('show');
                $('.inserir').attr('disabled', 'disabled');

            } else if (_dataInicio == "") {
                $('.alertDataInicio').modal('show');

            } else
                $('.inserir').removeAttr('disabled');

        }, 300);


    },
    validarHorarioEvento: function () {

        var _dataInicio = $("#DataInicio").val();
        var _dataFim = $("#DataFim").val();

        var _horarioInicial = $('#HoraInicio').val();
        var _horarioFinal = $('#HoraFim').val();

        setTimeout(function () {

            if (_dataInicio == _dataFim) {

                if (_horarioInicial > _horarioFinal && _horarioFinal != "") {
                    $('.alertHorario').modal('show');
                    $('.inserir').attr('disabled', 'disabled');

                } else
                    $('.inserir').removeAttr('disabled');
            }
            else if (_dataInicio != _dataFim) {

                if (_horarioFinal.length != 5) {

                    $('.alertHorarioMenor').modal('show');
                    $('.inserir').attr('disabled', 'disabled');

                } else {
                    $('.inserir').removeAttr('disabled');
                }
            }

        }, 300);
    },
    mascaras: function () {
        $('.dataEvento').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true,
            //onSelect: function (date) {
            //    alert(date)
            //},
        });

        $('#dataInicial, #dataFinal').datetimepicker({
            format: 'H:m'
        });

    },
    carregar_foto: function () {
        $('input[type=file]').each(function (index) {
            if ($('input[type=file]').eq(index).val() != "") {
                section.readURL(this);
                $('.hide').show();
            }
        });

    },
    readURL: function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(input).next()
                .attr('src', e.target.result)
            };
            reader.readAsDataURL(input.files[0]);
        }
        else {
            var img = input.value;
            $(input).next().attr('src', img);
        }
    }

};
$(document).ready(section.init);











//$(function () {

//    var cep = $("#CepNumeroEndereco");

//    var cidade = $("#CidadeEndereco");
//    var estado = $("#EstadoEndereco");
//    var logradouro = $("#LogradouroEndereco");
//    var complemento = $("#ComplementoEndereco");
//    var bairro = $("#BairroEndereco");

//    cep.blur(function () {

//        var numeroCep = cep.val().replace('-', '');

//        iterative.endereco(numeroCep, function (data) {

//            cidade.val(data.localidade);
//            estado.val(data.uf);
//            logradouro.val(data.logradouro);
//            complemento.val(data.complemento);
//            bairro.val(data.bairro);

//            console.log(data)
//        });
//    });
//});


//$('.inserir').bind("click", function () {
//    var imgVal = $('#fotoEvento').val();
//    if (imgVal == '') {
//        $('.alertUploadFoto').modal('show');
//        return false;
//    }
//});