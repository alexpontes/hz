﻿$(function () {
    $('.modelo').click(function () {
        $("#modeloShow").show();
        $("#cadastrar").show();
        $(".modelo").addClass("active");
        $(".cliente").removeClass("active");
    });
    $('.cliente').click(function () {
        $("#modeloShow").hide();
        $("#cadastrar").hide();
        $(".cliente").addClass("active");
        $(".modelo").removeClass("active");
    });
});
