﻿
var section = {
    init: function () {

        $("#CPNJ").blur(function () {

            var _cnpj = $("#CPNJ").val();
            var _valid_cnpj = section.validarCNPJ($('#CPNJ').val());

            if (_cnpj != "") {

                if (_valid_cnpj) {
                    $('.cadastrarCliente').removeAttr('disabled')
                    $('.editarCliente').removeAttr('disabled')
                }
                else {
                    $('.alertCNPJ').modal('show')
                    $('.cadastrarCliente').attr('disabled', 'disabled')
                    $('.editarCliente').attr('disabled', 'disabled')
                }
            }
        });

        var cep = $("#Endereco_CepNumero");

        $(cep).blur(section.carregar_cep);


    },
    validarCNPJ: function (cnpj) {

        cnpj = cnpj.replace(/\./g, '');
        cnpj = cnpj.replace(/\-/g, '');
        cnpj = cnpj.replace(/\//g, '');



        if (cnpj == '') return false;

        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999")

            return false;

        // Valida DVs
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;
    },
    carregar_cep: function () {

        var numeroCep = $("#Endereco_CepNumero").val().replace('-', '');

        iterative.endereco(numeroCep, function (data) {

            var cidade = $("#Endereco_Cidade").val(data.localidade);
            var estado = $("#Endereco_Estado").val(data.uf);
            var logradouro = $("#Endereco_Logradouro").val(data.logradouro);
            var complemento = $("#Endereco_Complemento").val(data.complemento);
            var bairro = $("#Endereco_Bairro").val(data.bairro);

            $("#Endereco_Numero").focus();

            console.log(data)
        });

    },
    
};
$(document).ready(section.init);































//$(function () {

//    var cep = $("#Endereco_CepNumero");

//    var cidade = $("#Endereco_Cidade");
//    var estado = $("#Endereco_Estado");
//    var logradouro = $("#Endereco_Logradouro");
//    var numero = $("#Endereco_Numero");
//    var complemento = $("#Endereco_Complemento");
//    var bairro = $("#Endereco_Bairro");

//    cep.blur(function () {

//        var numeroCep = cep.val().replace('-', '');

//        iterative.endereco(numeroCep, function (data) {

//            cidade.val(data.localidade);
//            estado.val(data.uf);
//            logradouro.val(data.logradouro);
//            complemento.val(data.complemento);
//            bairro.val(data.bairro);

//            console.log(data)
//        });
//    });
//});