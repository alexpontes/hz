﻿var CASTING_ID = $('#CastingId').val();
var TIPO_SELECAO = { PRESENCIAL: "1", FOTO: "2" };
var _util = new Util();

var section = {
    init: function () {
        

        $("a[name=adicionaPerfil]").click(function () {
            debugger;
            var inputs = $(this).parent().find('input');
            var perfilId = inputs[0].value;
            var castingId = inputs[1].value;
            adicionarPerfilCasting(perfilId, castingId);
        });
        $("select[name=AgendaId]").change(function () {
           
            var link = $(this).parent().find('a');
            var href = link.attr('href');
            var value = $(this).val();
            link.attr('href', href + '&agendaId=' + value);
        });
        $("#TipoSelecao").change(function () {

            console.log('valor', $(this).val());
            if ($(this).val() === TIPO_SELECAO.FOTO) {
                $("#divAgenda").hide();
                return;
            }

            $("#divAgenda").show();
            return;

        });

        section.inserir_range();
        section.carregarAgendas();
    },
    adicionarPerfilCasting: function (perfilId, castingId) {
        var url = "/Casting/AdicionarPerfil";
        var obj = { perfilId: perfilId, castingId: castingId };
        _util.request(url, 'POST', obj).then(function (result) {
            alert(result.mensagem);
        });
    },
    inserir_range: function () {

        $("#Idade").slider({
            range: true,
            min: 18,
            max: 40,
            values: [18, 40],
            slide: function (event, ui) {
                $("#IdadeMin").val(ui.values[0]);
                $("#IdadeMax").val(ui.values[1]);
            }
           
        });

        $("#Peso").slider({
            range: true,
            min: 30,
            max: 85,
            values: [30, 85],
            slide: function (event, ui) {
                $("#PesoMin").val(ui.values[0]);
                $("#PesoMax").val(ui.values[1]);
            }
        });

        $("#Altura").slider({
            range: true,
            step: 0.01,
            min: 1.70,
            max: 2.00,
            values: [1.70, 2.00],
            slide: function (event, ui) {
                $("#AlturaMin").val(ui.values[0].toString().replace(".",","));
                $("#AlturaMax").val(ui.values[1].toString().replace(".",","));
            }
        });

        $("#Manequin").slider({
            range: true,
            min: 34,
            max: 46,
            values: [34, 46],
            slide: function (event, ui) {
                $("#ManequinMin").val(ui.values[0]);
                $("#ManequinMax").val(ui.values[1]);
            }
        });

        $("#Cintura").slider({
            range: true,
            min: 64,
            max: 104,
            values: [64, 104],
            slide: function (event, ui) {
                $("#CinturaMin").val(ui.values[0]);
                $("#CinturaMax").val(ui.values[1]);
            }
        });

        $("#Quadril").slider({
            range: true,
            min: 84,
            max: 128,
            values: [84, 128],
            slide: function (event, ui) {
                $("#QuadrilMin").val(ui.values[0]);
                $("#QuadrilMax").val(ui.values[1]);
            }
        });

        $("#Busto").slider({
            range: true,
            min: 78,
            max: 122,
            values: [78, 122],
            slide: function (event, ui) {
                $("#BustoMin").val(ui.values[0]);
                $("#BustoMax").val(ui.values[1]);
            }
        });

        $("#Calcado").slider({
            range: true,
            min: 32,
            max: 41,
            values: [32, 41],
            slide: function (event, ui) {
                $("#CalcadoMin").val(ui.values[0]);
                $("#CalcadoMax").val(ui.values[1]);
            }
        });
    },
    carregarAgendas: function () {
        var url = '/Agenda/ListarPorCasting';
        var data = { 'castingId': CASTING_ID };
        _util.request(url, 'GET', data).then(function (result) {
            debugger;
            if (result === null) return;
            $('#agenda select').remove();
            var selecione = '<option selected> Selecione... </option>';
            var options = result.map(function (item) {
                return '<option value="' + item.id + '">' + item.dataInicio + ' - ' + item.dataFim + '</option>';
            });
            debugger;
            $('#agenda').append(selecione + options.toString());
        });
    }

};
$(document).ready(section.init)























//$(function () {
//    var util = new Util();
//    $("a[name=adicionaPerfil]").click(function () {
//        debugger;
//        var inputs = $(this).parent().find('input');
//        var perfilId = inputs[0].value;
//        var castingId = inputs[1].value;
//        adicionarPerfilCasting(perfilId, castingId);
//    });
//    function adicionarPerfilCasting(perfilId, castingId) {
//        var url = "/Casting/AdicionarPerfil";
//        var obj = { perfilId: perfilId, castingId: castingId };
//        util.request(url, 'POST', obj).then(function (result) {
//            alert(result.mensagem);
//        });
//    }

//});