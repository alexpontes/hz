﻿$(function () {
    $("#Idade").slider({
        range: true,
        min: 0,
        max: 100,
        values: [0, 18],
        slide: function (event, ui) {
            $("#IdadeMin").val(ui.values[0]);
            $("#IdadeMax").val(ui.values[1]);
        }
    });

    $("#Peso").slider({
        range: true,
        min: 0,
        max: 100,
        values: [0, 18],
        slide: function (event, ui) {
            $("#PesoMin").val(ui.values[0]);
            $("#PesoMax").val(ui.values[1]);
        }
    });

    $("#Altura").slider({
        range: true,
        //step: 0.01,
        min: 150,
        max: 260,
        values: [150, 160],
        slide: function (event, ui) {
            $("#AlturaMin").val(ui.values[0]);
            $("#AlturaMax").val(ui.values[1]);
        }
    });

    $("#Manequin").slider({
        range: true,
        min: 0,
        max: 100,
        values: [0, 18],
        slide: function (event, ui) {
            $("#ManequinMin").val(ui.values[0]);
            $("#ManequinMax").val(ui.values[1]);
        }
    });

    $("#Cintura").slider({
        range: true,
        min: 0,
        max: 100,
        values: [0, 18],
        slide: function (event, ui) {
            $("#CinturaMin").val(ui.values[0]);
            $("#CinturaMax").val(ui.values[1]);
        }
    });

    $("#Quadril").slider({
        range: true,
        min: 0,
        max: 100,
        values: [0, 18],
        slide: function (event, ui) {
            $("#QuadrilMin").val(ui.values[0]);
            $("#QuadrilMax").val(ui.values[1]);
        }
    });

    $("#Busto").slider({
        range: true,
        min: 0,
        max: 100,
        values: [0, 18],
        slide: function (event, ui) {
            $("#BustoMin").val(ui.values[0]);
            $("#BustoMax").val(ui.values[1]);
        }
    });

    $("#Calcado").slider({
        range: true,
        min: 0,
        max: 100,
        values: [0, 18],
        slide: function (event, ui) {
            $("#CalcadoMin").val(ui.values[0]);
            $("#CalcadoMax").val(ui.values[1]);
        }
    });
});