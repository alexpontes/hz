﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Helpers
{
    public class ResgateCookie
    {
        //private static int retornoId;
        private static string retornoString;

        public static string Nome
        {
            get
            {
                if (HttpContext.Current.Request.Cookies["InfoUsuario"] != null)
                {
                    retornoString = HttpContext.Current.Request.Cookies["InfoUsuario"]["Nome"];
                                      
                }
                else
                {
                    retornoString = null;
                }
                return retornoString;
            }
        }

        public static string Email
        {
            get
            {
                if (HttpContext.Current.Request.Cookies["InfoUsuario"] != null)
                {
                    retornoString = HttpContext.Current.Request.Cookies["InfoUsuario"]["Email"];
                }
                else
                {
                    retornoString = null;
                }
                return retornoString;
            }
        }

        public static string Sexo
        {
            get
            {
                if (HttpContext.Current.Request.Cookies["InfoUsuario"] != null)
                {
                    retornoString = HttpContext.Current.Request.Cookies["InfoUsuario"]["Sexo"];
                }
                else
                {
                    retornoString = null;
                }
                return retornoString;
            }
        }

        public static void LimparPropriedadesDoCookies()
        {
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("InfoUsuario"))
            {
                HttpCookie myCookie = new HttpCookie("InfoUsuario");
                myCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(myCookie);
            }
        }

    }
}