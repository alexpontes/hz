﻿using AutoMapper; 
using HzCasting.Domain.Entities;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Models.Modelos;
using System;
using System.Linq;

namespace HzCasting.WebApp.Mappers
{
    public sealed class DomainToViewModel : Profile
    {
        protected override void Configure()
        {
            CreateMap<CastingAgendaCasting, CastingViewModel>();
        }
    }
    //public class DomainToViewModel
    //{

    //    public static void Init()
    //    {
    //       var a = TypeAdapterConfig<Modelo, ModeloBaseViewModel>
    //            .NewConfig()
    //            .MapFrom(dest => dest.PrimeiroNomeDadosPessoais, src => src.DadosPessoais.PrimeiroNome)
    //            .MapFrom(dest => dest.UltimoNomeDadosPessoais, src => src.DadosPessoais.UltimoNome)
    //            .MapFrom(dest => dest.DataNascimentoDadosPessoais, src => src.DadosPessoais.DataNascimento)
    //            .MapFrom(dest => dest.EstadoCivilIdDadosPessoais, src => (Int32)src.DadosPessoais.EstadoCivil)
    //            .MapFrom(dest => dest.SexoIdDadosPessoais, src => (Int32)src.DadosPessoais.Sexo)
    //            .MapFrom(dest => dest.TelefoneDadosPessoais, src => src.DadosPessoais.Telefone)
    //            .MapFrom(dest => dest.CelularDadosPessoais, src => src.DadosPessoais.Celular)

    //            .MapFrom(dest => dest.RgDocumento, src => src.Documento.Rg)
    //            .MapFrom(dest => dest.PisDocumento, src => src.Documento.Pis)
    //            .MapFrom(dest => dest.CateiraProfissionalDocumento, src => src.Documento.CarteiraProfissional)
    //            .MapFrom(dest => dest.Passaporte, src => src.Documento.Passaporte)
    //            .MapFrom(dest => dest.Cpf, src => src.Documento.Cpf.Numero)


    //            .MapFrom(dest => dest.IdBanco, src => src.Banco.Id)
    //            .MapFrom(dest => dest.NomeBanco, src => src.Banco.Nome)
    //            .MapFrom(dest => dest.AgenciaBanco, src => src.Banco.Agencia)
    //            .MapFrom(dest => dest.ContaBanco, src => src.Banco.Conta)
    //            .MapFrom(dest => dest.TipoContaBanco, src => (Int32)src.Banco.TipoConta)


    //            .MapFrom(dest => dest.FormacaoEscolaridade, src => src.Escolaridade.Formacao)
    //            .MapFrom(dest => dest.TecnicoEscolaridade, src => src.Escolaridade.Tecnico)
    //            .MapFrom(dest => dest.SuperiorEscolaridade, src => src.Escolaridade.Superior)

    //            .MapFrom(dest => dest.DescricaoIdioma, src => src.Idioma.Descricao)
    //            .MapFrom(dest => dest.IdIdioma, src => src.Idioma.Id)

    //            .MapFrom(dest => dest.IdPerfilModelo, src => src.PerfilModelo.Id)
    //            .MapFrom(dest => dest.CorPelePerfiModelo, src => (Int32)src.PerfilModelo.CorPele)
    //            .MapFrom(dest => dest.CorOlhosPerfiModelo, src => (Int32)src.PerfilModelo.CorOlhos)
    //            .MapFrom(dest => dest.CorCabeloPerfilModelo, src => src.PerfilModelo.CorCabelo.Descricao)
    //            .MapFrom(dest => dest.IdCorCabeloPerfilModelo, src => src.PerfilModelo.CorCabelo.Id)

    //            .MapFrom(dest => dest.PesoPerfiModelo, src => src.PerfilModelo.Peso)
    //            .MapFrom(dest => dest.AlturaPerfiModelo, src => src.PerfilModelo.Altura)
    //            .MapFrom(dest => dest.ManequimPerfiModelo, src => src.PerfilModelo.Manequim)
    //            .MapFrom(dest => dest.CinturaPerfiModelo, src => src.PerfilModelo.Cintura)
    //            .MapFrom(dest => dest.QuadrilPerfiModelo, src => src.PerfilModelo.Quadril)
    //            .MapFrom(dest => dest.BustoPerfiModelo, src => src.PerfilModelo.Busto)
    //            .MapFrom(dest => dest.CalcadoPerfiModelo, src => src.PerfilModelo.Calcado)
    //            .MapFrom(dest => dest.OutrosPerfiModelo, src => src.PerfilModelo.Outros)

    //            .MapFrom(dest => dest.IdTatuagem, src => src.Tatuagem.Id)
    //            .MapFrom(dest => dest.DescricaoTatuagem, src => src.Tatuagem.Descricao)
    //            .MapFrom(dest => dest.ParteCorpoTatuagem, src => src.Tatuagem.ParteCorpo)

    //            .MapFrom(dest => dest.DisponibilidadeHorario, src => src.DisponibilidadeHorario)
    //            .MapFrom(dest => dest.DisponibilidadeViagem, src => src.DisponibilidadeViagem)
    //            .MapFrom(dest => dest.PossuiVeiculo, src => src.PossuiVeiculo)

    //            .MapFrom(dest => dest.TipoModelo, src => (Int32)src.TipoModelo)
    //            .MapFrom(dest => dest.CategoriaModelo, src => (Int32)src.CategoriaModelo)

    //            .MapFrom(dest => dest.IdEndereco, src => src.Endereco.Id)
    //            .MapFrom(dest => dest.NumeroEndereco, src => src.Endereco.Numero)
    //            .MapFrom(dest => dest.ComplementoEndereco, src => src.Endereco.Numero)
    //            .MapFrom(dest => dest.CepEndereco, src => src.Endereco.Cep.CepNumero)
    //            .MapFrom(dest => dest.LogradouroEndereco, src => src.Endereco.Logradouro)
    //            .MapFrom(dest => dest.GDEEndereco, src => src.Endereco.GDE)
    //            .MapFrom(dest => dest.CidadeEndereco, src => src.Endereco.Cidade)
    //            .MapFrom(dest => dest.EstadoEndereco, src => src.Endereco.Estado)

    //            .MapFrom(dest => dest.DataAprovacao, src => src.DataAprovacao)
    //            .MapFrom(dest => dest.StatusModelo, src => (Int32)src.StatusModelo);

    //    }

    //}
    //public class DomainToViewModel : Profile
    //{


    //}
}