﻿using System;
using AutoMapper;
using FastMapper;
using HzCasting.Domain.Entities;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Models.Modelos;
using HzCasting.WebApp.ModelsView;

namespace HzCasting.WebApp.Mappers
{
    public class ViewModelToDomain : Profile
    {
        protected override void Configure()
        {
            CreateMap<CastingViewModel, CastingAgendaCasting>(); 


        }
    }
    //public class ViewModelToDomain
    //{
    //    public static void Init()
    //    {
    //        TypeAdapterConfig<ModeloBaseViewModel, Modelo>
    //            .NewConfig()
    //            .MapFrom(dest => dest.DadosPessoais.PrimeiroNome, src => src.PrimeiroNomeDadosPessoais)
    //            .MapFrom(dest => dest.DadosPessoais.UltimoNome, src => src.UltimoNomeDadosPessoais)
    //            .MapFrom(dest => dest.DadosPessoais.DataNascimento, src => src.DataNascimentoDadosPessoais)
    //            .MapFrom(dest => dest.DadosPessoais.EstadoCivil, src => (EstadoCivilEnum)src.EstadoCivilIdDadosPessoais)
    //            .MapFrom(dest => dest.DadosPessoais.Sexo, src => (SexoEnum)src.SexoIdDadosPessoais)
    //            .MapFrom(dest => dest.DadosPessoais.Telefone, src => src.TelefoneDadosPessoais)
    //            .MapFrom(dest => dest.DadosPessoais.Celular, src => src.CelularDadosPessoais)

    //            .MapFrom(dest => dest.Documento.Rg, src => src.RgDocumento)
    //            .MapFrom(dest => dest.Documento.Cpf.Numero, src => src.Cpf)
    //            .MapFrom(dest => dest.Documento.Pis, src => src.PisDocumento)
    //            .MapFrom(dest => dest.Documento.CarteiraProfissional, src => src.CateiraProfissionalDocumento)
    //            .MapFrom(dest => dest.Documento.Passaporte, src => src.Passaporte)


    //            .MapFrom(dest => dest.Banco.Id, src => src.IdBanco)
    //            .MapFrom(dest => dest.Banco.Nome, src => src.NomeBanco)
    //            .MapFrom(dest => dest.Banco.Agencia, src => src.AgenciaBanco)
    //            .MapFrom(dest => dest.Banco.Conta, src => src.ContaBanco)
    //            .MapFrom(dest => dest.Banco.TipoConta, src => (TipoContaEnum)src.TipoContaBanco)


    //            .MapFrom(dest => dest.Escolaridade.Formacao, src => src.FormacaoEscolaridade)
    //            .MapFrom(dest => dest.Escolaridade.Tecnico, src => src.TecnicoEscolaridade)
    //            .MapFrom(dest => dest.Escolaridade.Superior, src => src.SuperiorEscolaridade)

    //            .MapFrom(dest => dest.Idioma.Descricao, src => src.DescricaoIdioma)
    //            .MapFrom(dest => dest.Idioma.Id, src => src.IdIdioma)

    //            .MapFrom(dest => dest.PerfilModelo.Id, src => src.IdPerfilModelo)
    //            .MapFrom(dest => dest.PerfilModelo.CorPele, src => (CorPeleEnum)src.CorPelePerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.CorOlhos, src => (CorOlhosEnum)src.CorOlhosPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.CorCabelo.Descricao, src => src.CorCabeloPerfilModelo)
    //            .MapFrom(dest => dest.PerfilModelo.CorCabelo.Id, src => src.IdCorCabeloPerfilModelo)

    //            .MapFrom(dest => dest.PerfilModelo.Peso, src => src.PesoPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.Altura, src => src.AlturaPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.Manequim, src => src.ManequimPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.Cintura, src => src.CinturaPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.Quadril, src => src.QuadrilPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.Busto, src => src.BustoPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.Calcado, src => src.CalcadoPerfiModelo)
    //            .MapFrom(dest => dest.PerfilModelo.Outros, src => src.OutrosPerfiModelo)

    //            .MapFrom(dest => dest.Tatuagem.Id, src => src.IdTatuagem)
    //            .MapFrom(dest => dest.Tatuagem.Descricao, src => src.DescricaoTatuagem)
    //            .MapFrom(dest => dest.Tatuagem.ParteCorpo, src => src.ParteCorpoTatuagem)

    //            .MapFrom(dest => dest.DisponibilidadeHorario, src => src.DisponibilidadeHorario)
    //            .MapFrom(dest => dest.DisponibilidadeViagem, src => src.DisponibilidadeViagem)
    //            .MapFrom(dest => dest.PossuiVeiculo, src => src.PossuiVeiculo)

    //            .MapFrom(dest => dest.TipoModelo, src => (TipoModeloEnum)src.TipoModelo)
    //            .MapFrom(dest => dest.CategoriaModelo, src => (CategoriaModeloEnum)src.CategoriaModelo)

    //            .MapFrom(dest => dest.Endereco.Id, src => src.IdEndereco)
    //            .MapFrom(dest => dest.Endereco.Numero, src => src.NumeroEndereco)
    //            .MapFrom(dest => dest.Endereco.Cep.CepNumero, src => src.CepEndereco)
    //            .MapFrom(dest => dest.Endereco.Cep.Logradouro, src => src.LogradouroEndereco)
    //            .MapFrom(dest => dest.Endereco.Cep.GDE, src => src.GDEEndereco)
    //            .MapFrom(dest => dest.Endereco.Complemento, src => src.ComplementoEndereco)
    //            .MapFrom(dest => dest.Endereco.Cep.Cidade, src => src.CidadeEndereco)
    //            .MapFrom(dest => dest.Endereco.Cep.Estado, src => src.EstadoEndereco)

    //            .MapFrom(dest => dest.DataAprovacao, src => src.DataAprovacao)
    //            .MapFrom(dest => dest.StatusModelo, src => (StatusModeloEnum)src.StatusModelo);



    //    }

    //}
}