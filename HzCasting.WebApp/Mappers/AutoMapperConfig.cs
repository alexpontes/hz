﻿using AutoMapper;
using HzCasting.Domain.Entities;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Models.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Mappers
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize((e) =>
            {
                e.AddProfile<DomainToViewModel>();
                e.AddProfile<ViewModelToDomain>();

            });
        }
    }
}