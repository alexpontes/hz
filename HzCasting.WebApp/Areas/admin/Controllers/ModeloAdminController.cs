﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.WebApp.Areas.admin.Controllers
{
    public class ModeloAdminController : Controller
    {
        // GET: admin/Modelo
        public ActionResult Index()
        {
            return View();
        }

        // GET: admin/Modelo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: admin/Modelo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin/Modelo/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: admin/Modelo/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: admin/Modelo/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: admin/Modelo/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: admin/Modelo/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
