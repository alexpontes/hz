﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using HzCasting.WebApp.Models;
using HzCasting.Domain.Entities;
using HzCasting.WebApp.Controllers.Base;
using System.Diagnostics;
using HzCasting.Domain.Interfaces.Application;
using System.Security.Claims;
using System.Threading;
using System.Collections.Generic;
using HzCasting.Infra.Common;
using HzCasting.Application;
using HzCasting.WebApp.Models.Account;

namespace HzCasting.WebApp.Controllers
{
    [Authorize]
    public sealed class AccountController : BaseController
    {
        private readonly IUsuarioClienteApplication _usuarioClienteApplication;
        private readonly IBackOfficeApplication _backofficeApplication;
        private readonly IUsuarioApplication _usuarioApplication;
        private readonly IModeloApplication _modeloApplication;
        private ILoginApplication _loginApplication;

        public AccountController(IUsuarioApplication usuarioApplication, IModeloApplication modeloApplication, IBackOfficeApplication backofficeApplication, IUsuarioClienteApplication usuarioClienteApplication)
        {
            _usuarioApplication = usuarioApplication;
            _modeloApplication = modeloApplication;
            _backofficeApplication = backofficeApplication;
            _usuarioClienteApplication = usuarioClienteApplication;

        }


        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string perfil, bool isBackOffice = false)
        {
            Helpers.ResgateCookie.LimparPropriedadesDoCookies();

            LoginViewModel model = new LoginViewModel();

            model.IsBackOffice = isBackOffice;

            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Perfil = perfil;

            return View(model);
        }


        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {

            HttpCookie myCookie = Request.Cookies["InfoUsuario"];

            if (myCookie == null)
            {
                myCookie = new HttpCookie("InfoUsuario");
                myCookie.Values.Add("Email", model.Email);
                var user = _usuarioApplication.Obter(x => x.Email.ToLower() == model.Email.ToLower())?.FirstOrDefault();
                if(user == null)
                {
                    ModelState.AddModelError("", "E-mail ou Senha incorretos.");
                    return View(model);
                }
                var modelo = _modeloApplication.Obter(x => x.IdUsuario == user.Id).FirstOrDefault();
                if (modelo != null)
                {
                    myCookie.Values.Add("Nome", modelo.DadosPessoais.PrimeiroNome);
                }
                else
                {
                    var backoffice = _backofficeApplication.Obter(x => x.IdUsuario == user.Id).FirstOrDefault();
                    if (backoffice != null)
                    {
                        myCookie.Values.Add("Nome", backoffice.PrimeiroNome);
                    }
                    else
                    {
                        var cliente = _usuarioClienteApplication.Obter(x => x.IdUsuario == user.Id).FirstOrDefault();
                        if (cliente != null)
                        {
                            myCookie.Values.Add("Nome", cliente.PrimeiroNome);
                        }

                    }

                }


                myCookie.Expires = DateTime.Now.AddDays(1);
                Response.SetCookie(myCookie);
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "E-mail ou Senha incorreto.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            //todo: jogar para a camada de aplicacao
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                var login = new LoginApplication(UserManager, SignInManager);

                //verifica se  usuario existe (pode ser que o usuario tenha se logado com o facebook e nao tinha uma conta atrelada)
                var usuario = new Usuario { CPF = model.Cpf, Perfild = 4 };
                var usuarioExistente = _usuarioApplication.VerificarUsuarioExistente(usuario);


                //caso seja um usuario existente, associa a conta existe
                if (usuarioExistente != null && usuarioExistente?.PasswordHash == null)
                {
                    usuario = usuarioExistente;
                    usuario.Email = model.Email;
                    usuario.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                    AssociarConta(usuario);


                    await login.LogarUserAsync(usuarioExistente);

                    var percentual = _modeloApplication.CalcularPercentualCadastro(usuario.Id);
                    return percentual > default(int) ?
                        RedirectToAction("Editar", "Modelo", new { usuario.Id }) :
                        RedirectToAction("Index", "Home");
                }

                if (usuarioExistente?.PasswordHash != null)
                    AddErrors(new IdentityResult("Usuario ja cadastrado!"));

                else
                {

                    var user = new Usuario { UserName = model.Email, Email = model.Email, CPF = model.Cpf, Senha = model.Password, Perfild = 3 };

                    var result = await login.CadastrarUsuarioAsync(user);
                    if (result.Succeeded) return RedirectToAction("Index", "Home");

                    AddErrors(result);
                }
                return View(model);



            }

            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return View(model);
            }
        }


        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            try
            {
                //todo: enviar para camada de aplicacao
                if (User.Identity.IsAuthenticated)
                    return RedirectToAction("Index", "Manage");

                if (ModelState.IsValid)
                {
                    var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                    if (info == null) return View("ExternalLoginFailure");


                    var usuario = new Usuario { CPF = model.Cpf };
                    var user = _usuarioApplication.VerificarUsuarioExistente(usuario);
                    var result = new IdentityResult();

                    #region usuario inexistente
                    if (user == null)
                    {
                        usuario.UserName = info.DefaultUserName;
                        usuario.Email = info.Email;
                        usuario.FacebookEmail = info.Email;
                        usuario.FacebookToken = ObterFacebookTokenUsuario(info);

                        var modelo = new Modelo
                        {
                            IdUsuario = usuario.Id,
                            Documento = new Documento
                            {
                                Cpf = new Cpf
                                {
                                    Numero = model.Cpf,
                                    IsValido = true
                                }
                            }
                        };
                        await _modeloApplication.Registrar(modelo, usuario);
                        result = await UserManager.AddLoginAsync(usuario.Id, info.Login);

                        if (result.Succeeded)
                        {
                            var loginResult = await SignInManager.ExternalSignInAsync(info, isPersistent: false);

                            var percentual = _modeloApplication.CalcularPercentualCadastro(usuario.Id);
                            if (percentual > default(int))
                            {
                                return RedirectToAction("Editar", "Modelo", new { id = usuario.Id });
                            }

                            return RedirectToLocal(returnUrl);
                        }


                    }
                    #endregion
                    //caso seja um usuario existente, associa a conta existe
                    #region usuario ativo sem fb
                    else if (user?.PasswordHash != null)
                    {
                        usuario = user;
                        usuario.FacebookEmail = info.Email;
                        usuario.FacebookToken = ObterFacebookTokenUsuario(info);

                        AssociarConta(usuario);

                        result = await UserManager.AddLoginAsync(usuario.Id, info.Login);

                        if (result.Succeeded)
                        {
                            var loginResult = await SignInManager.ExternalSignInAsync(info, isPersistent: false);


                            var percentual = _modeloApplication.CalcularPercentualCadastro(user.Id);
                            if (percentual > default(int))
                                return RedirectToAction("Editar", "Modelo", new { id = user.Id });


                            return RedirectToLocal(returnUrl);
                        }

                    }
                    #endregion

                    #region usuario com login ja ativo
                    else if (user?.FacebookEmail != null)
                    {
                        user.FacebookToken = ObterFacebookTokenUsuario(info);

                        await _loginApplication.LogarUserAsync(user);
                        var percentual = _modeloApplication.CalcularPercentualCadastro(user.Id);
                        if (percentual > default(int))
                        {
                            return RedirectToAction("Editar", "Modelo", new { Id = user.Id });
                        }
                    }
                    #endregion


                    AddErrors(result);
                }

                ViewBag.ReturnUrl = returnUrl;
                return View(model);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> VerificarCadastro(string facebookMail, string facebookToken)
        {
            VerificarCadastroViewModel model = new VerificarCadastroViewModel();

            model.FacebookEmail = facebookMail;
            model.FacebookToken = facebookToken;

            var usuario = _usuarioApplication.GetWithRoles()
                                            .Where(x => x.FacebookEmail == model.FacebookEmail)
                                            .FirstOrDefault();

            if (usuario == null)
            {
                return View(model);
            }

            else
            {
                usuario.FacebookEmail = model.FacebookEmail;
                usuario.FacebookToken = model.FacebookToken;

                //   usuario.Roles.Add(role);

                AssociarConta(usuario);

                var user = await UserManager.FindByEmailAsync(usuario.FacebookEmail);

                if (user != null)
                {
                    await SignInManager.SignInAsync(user, true, true);
                }


                return RedirectToAction("Index", "Home");
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> VerificarCadastro(VerificarCadastroViewModel model)
        {
            var usuario = new Usuario { CPF = model.Cpf };
            var user = _usuarioApplication.VerificarUsuarioExistente(usuario);

            //Se usuario não existe redireciona para o Registrar
            if (user == null)
            {
                var modelo = new ModeloSocialLogin
                {
                    Cpf = model.Cpf,
                    FacebookEmail = model.FacebookEmail,
                    FacebookToken = model.FacebookToken
                };

                TempData["MODELO_SOCIAL_LOGIN"] = modelo;

                return RedirectToAction("Registrar", "Modelo");
            }

            else
            {
                user.FacebookEmail = model.FacebookEmail;
                user.FacebookToken = model.FacebookToken;

                AssociarConta(user);

                var userManager = await UserManager.FindByEmailAsync(user.Email);

                if (userManager != null)
                {
                    await SignInManager.SignInAsync(userManager, true, true);
                }

                return RedirectToAction("Index", "Home");
            }
        }
        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_loginApplication == null)
                {
                    _loginApplication = new LoginApplication(UserManager, SignInManager);
                }

                await _loginApplication.ResetPassword(model.Email);

                //var user = await UserManager.FindByNameAsync(model.Email);
                //if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                //{
                //    // Don't reveal that the user does not exist or is not confirmed
                //    return View("ForgotPasswordConfirmation");
                //}

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");


            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
                return RedirectToAction("Login");

            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:

                    var usuario = _usuarioApplication.Obter(m => m.FacebookEmail == loginInfo.Email).FirstOrDefault();
                    if (usuario == null)
                    {
                        //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                        var model = new
                        {
                            facebookMail = loginInfo.Email,
                            facebookToken = loginInfo.ExternalIdentity.GetUserId()
                        };

                        return RedirectToAction("VerificarCadastro", "Account", model);
                    }
                    AtualizarFacebookTokenUsuario(loginInfo, usuario);
                    var percentual = _modeloApplication.CalcularPercentualCadastro(usuario.Id);
                    if (percentual > default(int))
                        return RedirectToAction("Editar", "Modelo", new { id = usuario.Id });

                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    var model2 = new
                    {
                        facebookMail = loginInfo.Email,
                        //facebookToken = loginInfo.ExternalIdentity.GetUserId()
                        facebookToken = ObterFacebookTokenUsuario(loginInfo)
                    };

                    return RedirectToAction("VerificarCadastro", "Account", model2);
            }
        }


        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Helpers.ResgateCookie.LimparPropriedadesDoCookies();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            Helpers.ResgateCookie.LimparPropriedadesDoCookies();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }
        #region private methods

        private void AssociarConta(Usuario user)
        {
            _usuarioApplication.AssociarFacebook(user);

        }

        private void AtualizarFacebookTokenUsuario(ExternalLoginInfo loginInfo, Usuario usuario)
        {

            usuario.FacebookToken = ObterFacebookTokenUsuario(loginInfo);
            _usuarioApplication.Atualizar(usuario);

        }

        private string ObterFacebookTokenUsuario(ExternalLoginInfo loginInfo)
        {
            var token = loginInfo.ExternalIdentity.Claims.Where(m => m.Type == "ExternalAccessToken").FirstOrDefault()?.Value;
            return token;
        }
        #endregion

        #region Helpers
        // Used for XSRF protection when adding external logins


        private const string XsrfKey = "XsrfId";


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }



        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}