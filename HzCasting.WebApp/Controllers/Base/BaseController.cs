﻿using HzCasting.Infra.Common;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HzCasting.WebApp.Helpers;
using System.Threading;
using System;
using System.Collections;
using System.Security.Claims;
using System.Collections.Generic;

namespace HzCasting.WebApp.Controllers.Base
{
    public abstract class BaseController : Controller
    {
        #region fields
        protected ApplicationUserManager _userManager;
        protected ApplicationSignInManager _signInManager;



        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            protected set
            {
                _userManager = value;
            }
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            protected set
            {
                _signInManager = value;
            }
        }
        protected IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #endregion
        protected void SetValidationErrors(ModelStateDictionary modelState)
        {
            var erros = ModelState
                               .SelectMany(m => m.Value.Errors.Select(n => n.ErrorMessage))
                               .Aggregate((i, j) => string.Concat(i, "\n", j));

            ModelState.AddModelError("error", erros);

        }
        protected String GetValidationErrors(ModelStateDictionary modelState)
        {
            var erros = ModelState
                               .SelectMany(m => m.Value.Errors.Select(n => n.ErrorMessage))
                               .Aggregate((i, j) => string.Concat(i, "\n", j));

            return erros;
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ?
                        Request.UserLanguages[0] :  // obtain it from HTTP header AcceptLanguages
                        null;
            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }
        
        [AllowAnonymous]
        public ActionResult SetCulture(string culture, string returnUrl)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }

        public IEnumerable<string> ObterRoles()
        {
            return ((ClaimsIdentity)User.Identity).Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value.ToLower());
        }

    }
}