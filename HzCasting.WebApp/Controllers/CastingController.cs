﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Common;
using HzCasting.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.WebApp.Controllers
{
    public class CastingController : Base.BaseController
    {
        private readonly IClienteApplication _clienteApplication;
        private readonly IEventoApplication _eventoApplication;
        private readonly ICastingAgendaCastingApplication _agendaCastingApplication;
        private readonly ICastingApplication _castingApplication;
        private readonly IAgenciaApplication _agenciaApplication;
        private readonly IPerfilFiltroApplication _perfilApplication;
        private readonly IInviteApplication _inviteApplication;
        private readonly IUsuarioClienteApplication _usuarioClienteApplication;

        public CastingController(IClienteApplication clienteApplication,
                                 IEventoApplication eventoApplication,
                                 ICastingAgendaCastingApplication agendaCastingApplication,
                                 ICastingApplication castingApplication,
                                 IAgenciaApplication agenciaApplication,
                                 IPerfilFiltroApplication perfilApplication,
                                 IInviteApplication inviteApplication,
                                 IUsuarioClienteApplication usuarioClienteApplication)
        {
            _inviteApplication = inviteApplication;
            _clienteApplication = clienteApplication;
            _eventoApplication = eventoApplication;
            _agendaCastingApplication = agendaCastingApplication;
            _castingApplication = castingApplication;
            _agenciaApplication = agenciaApplication;
            _perfilApplication = perfilApplication;
            _usuarioClienteApplication = usuarioClienteApplication;
        }

        [HttpGet]
        public ViewResult Listar(String titulo, String cliente, String agencia, DateTime? dataFim, DateTime? dataInicio, Int32? statusId, bool? criadoCliente)
        {
            try
            {
                CarregarSelects();

                var casting = _castingApplication.Listar(titulo, cliente, agencia, dataFim, dataInicio, statusId, criadoCliente)

                            .Select(c => new CastingViewModel
                            {
                                Id = c.Id,
                                TituloEvento = c.Evento?.Titulo,
                                NomeCliente = c.Cliente?.RazaoSocial,
                                NomeAgencia = c.Agencia?.Nome,
                                ImagemSrc = c.Foto?.ImagemBinario.GetSrcImage(),
                            }).ToList();

                return View(casting);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var casting = new List<CastingViewModel>();
                return View(casting);
            }
        }

        [HttpGet]
        public ViewResult ListarPorCliente(String titulo, DateTime? dataFim, DateTime? dataInicio)
        {
            try
            {
                var usuarioId = User.Identity.GetUserId();

                var usuarioCliente = _usuarioClienteApplication.Obter(x => x.IdUsuario == usuarioId).FirstOrDefault();


                var casting = _castingApplication.Obter(x => x.Cliente.Id == usuarioCliente.ClienteId);
                var itens = casting.Select(c => new CastingViewModel
                            {
                                Id = c.Id,
                                TituloEvento = c.Evento?.Titulo,
                                ImagemSrc = c.Foto?.ImagemBinario.GetSrcImage()
                            }).ToList();

                return View(itens);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var casting = new List<CastingViewModel>();
                return View(casting);
            }
        }

        [HttpGet]
        public ActionResult Cadastrar()
        {
            CarregarSelects();
            return View();
        }

        [HttpGet]
        public ActionResult CastingModelo()
        {
            var userId = User.Identity.GetUserId();

            var roles = ObterRoles();

            var castings =
            ToCastingViewModel(
                _inviteApplication.Obter(
                                            i => (i.Booking.Modelo.IdUsuario == userId || roles.Contains("admin")) &&
                                            i.StatusTrackInvite == StatusTrackInviteEnum.Aceito &&
                                            i.Booking.StatusModeloCastingEnum == StatusModeloCastingEnum.Aprovada)
                                        .Select(
                                                    i => i.Booking.PerfilFiltro.Casting

                                        ).ToList()
            );

            return View(castings);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var casting = _castingApplication.Obter(id);

            return View();
        }

        [HttpGet]
        public ActionResult CadastrarPorCliente()
        {
            CarregarSelects();
            return View();
        }

        [HttpGet]
        public ActionResult AgendaPorCasting(int castingId)
        {
            List<Casting> castingVm = new List<Casting>();

            try
            {

                var casting = _castingApplication.Obter(castingId);

                castingVm.Add(casting);


                return View(ToCastingViewModel(castingVm));

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Errro" + ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult CadastrarPorCliente(CastingViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                var usuarioId = User.Identity.GetUserId();

                var usuarioCliente = _usuarioClienteApplication.Obter(x => x.IdUsuario == usuarioId).FirstOrDefault();

                if (usuarioCliente != null)
                {
                    _castingApplication.Registrar(usuarioCliente.ClienteId, model.IdEvento, model.Imagem);

                    return RedirectToAction("ListarPorCliente");
                }

                return Json(new { erro = "Erro ao cadastrar Casting !" });
            }
            catch (Exception ex)
            {
                Debug.Write($"Erro: {ex}");
                return Json(new { erro = "Erro ao cadastrar Casting !" });

            }
        }



        [HttpPost]
        public ActionResult Cadastrar(CastingViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                _castingApplication.Registrar(model.IdCliente, model.IdEvento, model.Imagem, model.IdAgencia);

                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                Debug.Write($"Erro: {ex}");
                return Json(new { erro = "Erro ao cadastrar Casting !" });

            }
        }

        public ActionResult Editar(Int32 id, Boolean cliente = false)
        {
            var casting = _castingApplication.Obter(id);
            var vm = ToCastingViewModel(casting);
            vm.Cliente = cliente;
            return View(vm);
        }
        [HttpPost]
        public ActionResult Editar(CastingViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                if (model.Cliente)
                {
                    var usuarioId = User.Identity.GetUserId();
                    var usuarioCliente = _usuarioClienteApplication.Obter(x => x.IdUsuario == usuarioId).FirstOrDefault();
                    if (usuarioCliente == null) throw new ApplicationException("Usuario não encontrado");

                    _castingApplication.Registrar(usuarioCliente.ClienteId, model.IdEvento, model.Imagem);
                    return RedirectToAction("ListarPorCliente");
                }

                _castingApplication.Atualizar(model.IdCliente, model.IdEvento, model.Imagem, model.IdAgencia, model.Id);
                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                Debug.Write($"Erro: {ex}");
                return Json(new { erro = "Erro ao cadastrar Casting !" });

            }
        }

        private void CarregarSelects(CastingAgendaCasting casting = null)
        {
            var uf = default(UfEnum).ToSelectListItem();
            ViewBag.Uf = uf;

            var statusCasting = default(StatusCastingEnum).ToSelectListItem();
            ViewBag.StatusCasting = statusCasting;
        }

        [HttpGet]
        public async Task<JsonResult> CarregarAutoCompleteAgencia()
        {
            try
            {
                var agencias = await Task.FromResult(_agenciaApplication.Listar());
                var jsonAgencias = agencias.Select(m => new
                {
                    nome = m.Nome,
                    id = m.Id
                });
                return Json(jsonAgencias, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return Json(new { error = "Erro, contate o suporte!" });
            }
        }

        [HttpGet]
        public async Task<JsonResult> CarregarAutoCompleteCliente()
        {
            try
            {
                var clientes = await Task.FromResult(_clienteApplication.Listar());
                var jsonClientes = clientes.Select(m => new
                {
                    nome = m.NomeFantasia,
                    id = m.Id
                });
                return Json(jsonClientes, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return Json(new { error = "Erro, contate o suporte!" });
            }
        }

        [HttpGet]
        public async Task<JsonResult> CarregarAutoCompleteEvento()
        {
            try
            {
                var eventos = await Task.FromResult(_eventoApplication.Listar());
                var jsonEventos = eventos.Select(m => new
                {
                    nome = m.Titulo,
                    id = m.Id
                });
                return Json(jsonEventos, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return Json(new { error = "Erro, contate o suporte!" });
            }
        }

        [HttpPost]
        public JsonResult AdicionarPerfil(Int32 perfilId, Int32 castingId)
        {
            try
            {
                var model = _castingApplication.Obter(castingId);
                var perfil = _perfilApplication.Obter(perfilId);
                if (perfil != null)
                {
                    model.PerfisFiltros = model.PerfisFiltros ?? new List<PerfilFiltro>();
                    model.PerfisFiltros.Add(perfil);
                }


                if (model != null)
                    _castingApplication.Atualizar(model);

                return Json(new { mensagem = "perfil associado com sucesso" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return Json(new { error = "Erro, contate o suporte!" });
            }
        }

        #region helpers



        private List<CastingViewModel> ToCastingViewModel(List<Casting> castings)
        {
            var listCastingViewModel = new List<CastingViewModel>();

            foreach (var casting in castings)
            {
                var castingVm = new CastingViewModel
                {
                    Id = casting.Id,
                    IdAgencia = casting.Agencia.Id,
                    IdCliente = casting.Cliente.Id,
                    IdEvento = casting.Evento.Id,
                    NomeAgencia = casting.Agencia.Nome,
                    NomeCliente = casting.Cliente.NomeFantasia,
                    TituloEvento = casting.Evento.Titulo,
                    Agendas = ToAgendaViewModel(casting.Agendas)
                };

                listCastingViewModel.Add(castingVm);
            }

            return listCastingViewModel;
        }
        private CastingViewModel ToCastingViewModel(Casting casting)
        {

            var castingVm = new CastingViewModel
            {
                Id = casting.Id,
                IdCliente = casting.Cliente.Id,
                IdEvento = casting.Evento.Id,
                IdAgencia = (casting.Agencia?.Id ?? 0),
                NomeAgencia = casting.Agencia?.Nome,
                NomeCliente = casting.Cliente.NomeFantasia,
                TituloEvento = casting.Evento.Titulo,
                Agendas = ToAgendaViewModel(casting.Agendas),
                ImagemSrc = casting.Foto?.ImagemBinario?.GetSrcImage()
            };


            return castingVm;
        }

        private static IList<AgendaViewModel> ToAgendaViewModel(IList<AgendaCasting> dadosView)
        {
            var modelVws = new List<AgendaViewModel>();

            foreach (var item in dadosView)
            {

                var modelVw = new AgendaViewModel
                {
                    DataInicio = item.DataHoraInicio,
                    HoraInicio = item.DataHoraInicio,
                    HoraFim = item.DataHoraFim,
                    AgendaCastingId = item.Id,
                    QuantidadeCandidatos = item.QuantidadeCandidatos

                };

                modelVws.Add(modelVw);
            }

            return modelVws;
        }

        #endregion
    }
}