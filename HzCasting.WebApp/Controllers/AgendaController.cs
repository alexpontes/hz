﻿
using System;
using System.Web.Mvc;
using HzCasting.WebApp.Controllers.Base;
using HzCasting.WebApp.Models;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using System.Collections.Generic;
using HzCasting.Infra.Common;
using System.Diagnostics;
using System.Linq;

namespace HzCasting.WebApp.Controllers
{

    public class AgendaController : BaseController
    {
        #region fields

        private readonly IAgendaCastingApplication _agendaApplication;
        private readonly ICastingApplication _castingApplication;
        private readonly IPerfilFiltroApplication _perfilFiltroApplication;

        #endregion
        public AgendaController(IAgendaCastingApplication agendaApplication, ICastingApplication castingApplication, IPerfilFiltroApplication perfilFiltroApplication)
        {
            _perfilFiltroApplication = perfilFiltroApplication;
            _agendaApplication = agendaApplication;
            _castingApplication = castingApplication;
        }

        public ActionResult Cadastrar(Int32 idCasting)
        {
            try
            {
                //CarregarSelects();
                var vm = new AgendaViewModel();
                vm.CastingId = idCasting;
                return View(vm);

            }
            catch (Exception)
            {

                return View();

            }
        }

        [HttpPost]
        public ActionResult Cadastrar(AgendaViewModel dadosTela)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(dadosTela);

                var model = ToModel(dadosTela);
                _agendaApplication.Registrar(model);
                var casting = _castingApplication.Obter(dadosTela.CastingId);
                casting.Agendas = casting.Agendas ?? new List<AgendaCasting>();
                casting.Agendas.Add(model);

                _castingApplication.Atualizar(casting);

                return RedirectToActionPermanent("AgendaPorCasting", "Casting", new { castingId = dadosTela.CastingId });
            }
            catch (Exception ex)
            {
                var vm = dadosTela;
                vm.CastingId = dadosTela.CastingId;
                ModelState.AddModelError("", "Erro interno do servidor");
                return View(vm);
            }
        }


        public JsonResult ListarPorCasting(Int32 castingId)
        {
            try
            {
                var agendas = _castingApplication.Obter(castingId)?.Agendas?
                    .Select(m => new
                    {
                        id = m.Id,
                        dataInicio = m.DataHoraInicio.ToString(),
                        dataFim = m.DataHoraFim.ToString()
                    });

                return Json(agendas, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Errro" + ex);
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private AgendaCasting ToModel(AgendaViewModel dadosTela)
        {

            var dataFim = new DateTime(dadosTela.DataInicio.Value.Year, dadosTela.DataInicio.Value.Month, dadosTela.DataInicio.Value.Day,dadosTela.HoraFim.Value.Hour,dadosTela.HoraFim.Value.Minute,0);
            //dataFim.AddHours(dadosTela.HoraFim.Value.Hour);
            //dataFim.AddSeconds(dadosTela.HoraFim.Value.Second);

            var dataInicio = new DateTime(dadosTela.DataInicio.Value.Year, dadosTela.DataInicio.Value.Month, dadosTela.DataInicio.Value.Day, dadosTela.HoraInicio.Value.Hour, dadosTela.HoraInicio.Value.Minute, 0);
            //dataInicio.AddHours(dadosTela.HoraInicio.Value.Hour);
            //dataInicio.AddSeconds(dadosTela.HoraInicio.Value.Second);


	  return new AgendaCasting {
		DataHoraFim = dataFim,
		DataHoraInicio = dataInicio,
		Endereco = new Endereco {
		  Cep = new Cep {
			Bairro = dadosTela.Bairro,
			CepNumero = dadosTela.Cep,
			Cidade = dadosTela.Cidade,
			Estado = dadosTela.Estado,
			GDE = dadosTela.GDE,
			Logradouro = dadosTela.Logradouro,
			UF = (UfEnum)dadosTela.Uf,
			Zona = dadosTela.Zona

		  },
		  Complemento = dadosTela.Complemento,
		  Numero = dadosTela.Numero
		},
		QuantidadeCandidatos = dadosTela.QuantidadeCandidatos.Value

                //ListaPerfilCasting = new List<PerfilFiltro>
                //{
                //    new PerfilFiltro
                //    {
                //        Castings = new List<Casting >
                //        {
                //            casting
                //        }
                //    }
                //}


            };
        }

        private static IList<AgendaViewModel> ToAgendaViewModel(IList<AgendaCasting> dadosView)
        {
            var modelVws = new List<AgendaViewModel>();

            foreach (var item in dadosView)
            {

                var modelVw = new AgendaViewModel
                {
                    DataInicio = item.DataHoraInicio.Date,
                    HoraInicio = item.DataHoraInicio.Date,
                    HoraFim = item.DataHoraFim.Date,
                    AgendaCastingId = item.Id

                };

                modelVws.Add(modelVw);
            }

            return modelVws;
        }


        // POST: Modelo/Create
        private void CarregarSelects(AgendaCasting agenda = null)
        {

            var uf = default(UfEnum).ToSelectListItem(agenda?.Endereco?.Cep?.UF);


            ViewBag.Uf = uf;

        }
    }
}
