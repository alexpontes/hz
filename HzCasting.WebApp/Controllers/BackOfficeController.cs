﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.WebApp.Controllers.Base;
using HzCasting.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.WebApp.Controllers
{
    public class BackOfficeController : BaseController
    {
        private readonly IUsuarioApplication _usuarioApplication;

        public BackOfficeController(IUsuarioApplication usuarioApplication, IBackOfficeApplication backOfficeApplication)
        {
            _usuarioApplication = usuarioApplication;
        }

        // GET: BackOffice
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult ListarUsuarios(String cpf, String email)
        {
            try
            {
                cpf = cpf != null ? cpf.Replace(".", "").Replace("-", "") : string.Empty;
                email = email ?? String.Empty;

                var model = new List<UsuarioBackOfficeViewModel>();

                var usuariosDomain = _usuarioApplication.Listar(cpf, email);

                foreach (var item in usuariosDomain)
                {
                    model.Add(new UsuarioBackOfficeViewModel()
                    {
                        Cpf = item.CPF,
                        Email = item.Email,
                        UsuarioId = item.Id,
                    });
                }

                return View(model);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var list = new List<UsuarioBackOfficeViewModel>();
                return View(list);
            }
        }

        public UsuarioBackOfficeViewModel ToViewModel(BackOffice usuario)
        {
            var dadosView = new UsuarioBackOfficeViewModel
            {
                 PrimeiroNome = usuario.PrimeiroNome,
                 UltimoNome = usuario.UltimoNome,
                 Cpf = usuario.CpfNumero,
                 Email = usuario.DadosPessoais_Email               
            };
            return dadosView;
        }
    }
}