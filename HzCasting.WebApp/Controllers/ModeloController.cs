﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using HzCasting.WebApp.Controllers.Base;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Models.Modelos;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.WebApp.ModelsView;
using System.Collections.Generic;
using HzCasting.Infra.Common;
using Microsoft.AspNet.Identity.Owin;

namespace HzCasting.WebApp.Controllers
{

    public class ModeloController : BaseController
    {
        #region fields
        private readonly ModeloCadastrarViewModel _viewModel;
        private readonly IModeloApplication _modeloApplication;
        private readonly IUsuarioApplication _usuario;
        private readonly IEnderecoApplication _enderecoApplication;
        private readonly ICastingApplication _castingApplication;
        private readonly IIdiomaApplication _idiomaApplication;

        #endregion

        public ModeloController(IModeloApplication modeloApplication, IUsuarioApplication usuario,
            IEnderecoApplication enderecoApplication, ICastingApplication castingApplication, IIdiomaApplication idiomaApplication)
        {
            _modeloApplication = modeloApplication;
            _usuario = usuario;
            _castingApplication = castingApplication;
            _enderecoApplication = enderecoApplication;
            _idiomaApplication = idiomaApplication;
            _viewModel = new ModeloCadastrarViewModel();


        }

        public ActionResult ImportarFotosModelo(Int32 idModelo = 0, Int32 take = 10, Int32 skip = 0)
        {
            var id = string.Empty;
            Modelo modelo;
            var vm = new ImportacaoFotosViewModel();
            var isBackOffice = idModelo != 0;
            vm.BackOffice = isBackOffice;
            try
            {
                if (isBackOffice)
                {
                    modelo = _modeloApplication.Obter(idModelo);
                    id = modelo?.IdUsuario;
                }
                else
                {
                    id = User.Identity.GetUserId();
                    modelo = _modeloApplication.Obter(m => m.IdUsuario == id)?.FirstOrDefault();
                }

                vm.UsuarioId = id;
                return View(vm);
            }
            catch (ApplicationException ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View(vm);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro:" + ex);
                ModelState.AddModelError("", ex.Message);
                return View(vm);
            }

        }

        public ActionResult ListarFotos(Int32 idModelo = 0)
        {
            var idUsuario = string.Empty;
            var isBackOffice = idModelo != 0;
            ViewBag.IsBackOffice = isBackOffice;
            try
            {
                if (isBackOffice)
                {
                    var modelo = _modeloApplication.Obter(idModelo);
                    idUsuario = modelo?.IdUsuario;
                }
                else
                {
                    idUsuario = User.Identity.GetUserId();
                }
                ViewBag.UsuarioId = idUsuario;
                return View();
            }
            catch (ApplicationException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro:" + ex);
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost]
        public async Task<JsonResult> CarregarFotosFacebookModelo(String idUsuario, Int32 take = 10, Int32 skip = 0)
        {
            try
            {
                var user = await UserManager.FindByIdAsync(idUsuario);
                if (user == null) throw new ApplicationException("Usuário não encontrado!");

                var fotos = new List<UsuarioFacebook>();
                IEnumerable<FacebookViewModel> vm;
                try
                {
                    fotos = await _usuario.ObterFotosUsuarioAsync(user.FacebookToken, take, skip);
                }
                catch (ApplicationException ex)
                {
                    //caso nao tenham fotos associadas, nao exibe.
                    Debug.WriteLine("Modelo sem facebook", ex.Message);
                }

                vm = fotos.Select(m => new FacebookViewModel()
                {
                    Id = m.Id,
                    ImageURL = m.ImageURL

                });

                return Json(vm, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CarregarFotosModelo(String idUsuario, Int32 take = 10, Int32 skip = 0)
        {

            var fotos = new List<FotoModelo>();
            try
            {
                if (string.IsNullOrEmpty(idUsuario))
                    return Json(new { error = "Id do usuario nao informado ou incorreto!" });

                fotos = _modeloApplication
                                            .Obter(m => m.IdUsuario == idUsuario)?
                                            .FirstOrDefault()?
                                            .Fotos?
                                            .Take(take)
                                            .Skip(skip)
                                            .ToList();

                var vm = fotos?.Select(MapearFotosModelo)?.ToList();
                var itens = new
                {
                    data = vm ?? new List<FacebookViewModel>(),
                    quantidadePendentes = vm?.Select(m => m.Status == StatusFotoModeloEnum.Pendente.ToString())?.Count() ?? 0
                };

                var json = Json(itens, JsonRequestBehavior.AllowGet);
                json.MaxJsonLength = int.MaxValue;
                return json;
            }
            catch (ApplicationException ex)
            {
                ModelState.AddModelError("", ex.Message);
                var vm = new List<FacebookViewModel>();
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro:" + ex);

                return Json(new { error = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult RemoverFoto(String usuarioId, Int64 idFoto)
        {
            try
            {
                _modeloApplication.RemoverFoto(usuarioId, idFoto);
                return Json(new { });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro:" + ex);

                return Json(new { error = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AlterarFoto(String usuarioId, Int64 idFoto)
        {
            try
            {
                _modeloApplication.AlterarFotoPrincipal(usuarioId, idFoto);
                return Json(new { });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro:" + ex);

                return Json(new { error = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AprovarFoto(String usuarioId, Int32 idFoto)
        {
            try
            {
                _modeloApplication.AprovarFoto(usuarioId, idFoto);
                return Json(new { });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro:" + ex);

                return Json(new { error = ex.Message });
            }
        }
        public ActionResult CriarPerfilModelo(int modeloId)
        {
            var modelo = _modeloApplication.Obter(modeloId);

            var mainModel = ToMainModel(modelo);

            return View(mainModel);
        }

        [HttpPost]
        public ActionResult CriarPerfilModelo(ModeloPerfilMainViewModel model)
        {
            var domain = ToMainDomain(model);

            _modeloApplication.Atualizar(domain);

            var mainModel = ToMainModel(domain);

            return View(mainModel);
        }

        [HttpPost]
        public JsonResult CadastrarIdioma(ModeloIdiomaViewModel model)
        {
            var modelo = _modeloApplication.Obter(model.ModeloId);

            modelo.Idiomas.Clear();

            foreach (var item in model.Idiomas)
            {
                modelo.Idiomas.Add(new ModeloIdioma()
                {
                    ModeloId = model.ModeloId,
                    IdiomaId = item.IdiomaId
                });
            }

            _modeloApplication.Atualizar(modelo);

            return Json(true);
        }

        private ModeloPerfilMainViewModel ToMainModel(Modelo modelo)
        {
            var vm = new ModeloPerfilMainViewModel();

            if (modelo.PerfilModelo != null)
            {
                vm.Caracteristicas.Altura = modelo.PerfilModelo.Altura;
                vm.Caracteristicas.Busto = modelo.PerfilModelo.Busto;
                vm.Caracteristicas.Calcado = modelo.PerfilModelo.Calcado;
                vm.Caracteristicas.Cintura = modelo.PerfilModelo.Cintura;
                vm.Caracteristicas.Manequim = modelo.PerfilModelo.Manequim;
                vm.Caracteristicas.Outros = modelo.PerfilModelo.Outros;
                vm.Caracteristicas.Peso = modelo.PerfilModelo.Peso;
                vm.Caracteristicas.CorCabeloId = (CorCabeloEnum)modelo.PerfilModelo.CorCabelo;
                vm.Caracteristicas.CorOlhosId = modelo.PerfilModelo.CorOlhos;
                vm.Caracteristicas.CorPeleId = modelo.PerfilModelo.CorPele;
                vm.Caracteristicas.TipoModeloId = modelo.TipoModelo;
                vm.Caracteristicas.CategoriaModeloId = modelo.CategoriaModelo;

                var tipoModeloSelecionada = vm.Caracteristicas
                                          .TipoModelo
                                          .FirstOrDefault(x => x.Value == ((int)vm.Caracteristicas.TipoModeloId).ToString());

                if (tipoModeloSelecionada != null)
                    tipoModeloSelecionada.Selected = true;

                var categoriaModeloSelecionada = vm.Caracteristicas
                                        .CategoriaModelo
                                        .FirstOrDefault(x => x.Value == ((int)vm.Caracteristicas.CategoriaModeloId).ToString());

                if (categoriaModeloSelecionada != null)
                    categoriaModeloSelecionada.Selected = true;

                var corCabeloSelecionada = vm.Caracteristicas
                                            .CorCabelo
                                            .FirstOrDefault(x => x.Value == ((int)vm.Caracteristicas.CorCabeloId).ToString());

                if (corCabeloSelecionada != null)
                    corCabeloSelecionada.Selected = true;


                var corOlhoSelecionada = vm.Caracteristicas
                                            .CorOlhos
                                            .FirstOrDefault(x => x.Value == ((int)vm.Caracteristicas.CorOlhosId).ToString());

                if (corOlhoSelecionada != null)
                    corOlhoSelecionada.Selected = true;


                var corPeleSelecionada = vm.Caracteristicas
                                          .CorPele
                                          .FirstOrDefault(x => x.Value == ((int)vm.Caracteristicas.CorPeleId).ToString());


                if (corPeleSelecionada != null)
                    corPeleSelecionada.Selected = true;

            }

            if (modelo.Tatuagem != null)
            {
                vm.Caracteristicas.Tatuagem.Descricao = modelo.Tatuagem.Descricao;
                vm.Caracteristicas.Tatuagem.LocalDoCorpo = modelo.Tatuagem.ParteCorpo;
            }


            if (modelo.DadosPessoais != null)
            {
                vm.Contato.Celular = modelo.DadosPessoais.Celular;
                vm.Contato.Telefone = modelo.DadosPessoais.Telefone;
                vm.DadosPessoais.Celular = modelo.DadosPessoais.Celular;
                vm.DadosPessoais.DataNascimento = modelo.DadosPessoais.DataNascimento;
                vm.DadosPessoais.EstadoCivilId = modelo.DadosPessoais.EstadoCivil;
                vm.DadosPessoais.PrimeiroNome = modelo.DadosPessoais.PrimeiroNome;
                vm.DadosPessoais.UltimoNome = modelo.DadosPessoais.UltimoNome;

                var estadoCivil = vm.DadosPessoais
                                 .EstadoCivil
                                 .FirstOrDefault(x => x.Value == ((int)vm.DadosPessoais.EstadoCivilId).ToString());

                if (estadoCivil != null)
                    estadoCivil.Selected = true;

                var sexo = vm.DadosPessoais
                             .Sexo
                             .FirstOrDefault(x => x.Value == ((int)vm.DadosPessoais.SexoId).ToString());

                if (sexo != null)
                    sexo.Selected = true;

            }

            if (modelo.Banco != null)
            {
                vm.DadosBancarios.Agencia = modelo.Banco.Agencia;
                vm.DadosBancarios.Banco = modelo.Banco.Nome;
                vm.DadosBancarios.ContaCorrente = modelo.Banco.Conta;
            }

            if (modelo.Documento != null)
            {
                vm.Documentos.Cpf = modelo.Documento.Cpf?.Numero;
                vm.Documentos.Ctps = modelo.Documento.CarteiraProfissional;
                vm.Documentos.Passaporte = modelo.Documento.Passaporte;
                vm.Documentos.Pis = modelo.Documento.Pis;
                vm.Documentos.Rg = modelo.Documento.Rg;
            }

            vm.Endereco.Bairro = modelo.Endereco?.Cep?.Bairro;
            vm.Endereco.Cep = modelo.Endereco?.Cep?.CepNumero;
            vm.Endereco.Cidade = modelo.Endereco?.Cep?.Cidade;
            vm.Endereco.Estado = modelo.Endereco?.Cep?.Estado;
            vm.Endereco.Complemento = modelo.Endereco?.Complemento;
            vm.Endereco.Numero = modelo.Endereco?.Numero;
            vm.Endereco.Logradouro = modelo.Endereco?.Cep?.Logradouro;

            vm.EscolaridadeId = modelo.Escolaridade;

            var idiomas = _idiomaApplication.Obter(x => x.Ativo.HasValue && x.Ativo.Value);

            foreach (var item in idiomas)
            {
                foreach (var idiomaModelo in modelo.Idiomas)
                {
                    if (item.Id == idiomaModelo.IdiomaId)
                    {
                        vm.Idiomas.AddIdioma((int)item.Id, item.Descricao, true);
                        continue;
                    }
                }

                vm.Idiomas.AddIdioma((int)item.Id, item.Descricao, false);
            }

            return vm;
        }

        private Modelo ToMainDomain(ModeloPerfilMainViewModel vm)
        {
            var modelo = _modeloApplication.Obter(vm.ModeloId);

            if (vm.Caracteristicas != null)
            {
                modelo.PerfilModelo.Altura = vm.Caracteristicas.Altura;
                modelo.PerfilModelo.Busto = vm.Caracteristicas.Busto;
                modelo.PerfilModelo.Calcado = (int)vm.Caracteristicas.Calcado;
                modelo.PerfilModelo.Cintura = vm.Caracteristicas.Cintura;
                modelo.PerfilModelo.Manequim = vm.Caracteristicas.Manequim;
                modelo.PerfilModelo.Outros = vm.Caracteristicas.Outros;
                modelo.PerfilModelo.Peso = vm.Caracteristicas.Peso;
                modelo.PerfilModelo.CorCabelo = vm.Caracteristicas.CorCabeloId;
                modelo.PerfilModelo.CorOlhos = vm.Caracteristicas.CorOlhosId;
                modelo.PerfilModelo.CorPele = vm.Caracteristicas.CorPeleId;
            }

            if (vm.Caracteristicas != null && vm.Caracteristicas.Tatuagem != null)
            {
                modelo.Tatuagem.Descricao = vm.Caracteristicas.Tatuagem.Descricao;
                modelo.Tatuagem.ParteCorpo = vm.Caracteristicas.Tatuagem.LocalDoCorpo;
            }


            if (vm.DadosPessoais != null)
            {
                modelo.DadosPessoais.Celular = vm.Contato.Celular;
                modelo.DadosPessoais.Telefone = vm.Contato.Telefone;
                modelo.DadosPessoais.Celular = vm.DadosPessoais.Celular;
                modelo.DadosPessoais.DataNascimento = vm.DadosPessoais.DataNascimento;
                modelo.DadosPessoais.EstadoCivil = vm.DadosPessoais.EstadoCivilId;
                modelo.DadosPessoais.PrimeiroNome = vm.DadosPessoais.PrimeiroNome;
                modelo.DadosPessoais.UltimoNome = vm.DadosPessoais.UltimoNome;
            }

            if (modelo.Banco != null)
            {
                modelo.Banco.Agencia = vm.DadosBancarios.Agencia;
                modelo.Banco.Nome = vm.DadosBancarios.Banco;
                modelo.Banco.Conta = vm.DadosBancarios.ContaCorrente;
            }

            if (vm.Documentos != null)
            {
                if (modelo.Documento.Cpf != null)
                {
                    modelo.Documento.Cpf.Numero = vm.Documentos.Cpf;
                }
                modelo.Documento.CarteiraProfissional = vm.Documentos.Ctps;
                modelo.Documento.Passaporte = vm.Documentos.Passaporte;
                modelo.Documento.Pis = vm.Documentos.Pis;
                modelo.Documento.Rg = vm.Documentos.Rg;
            }

            if (modelo.Documento.Cpf != null)
            {
                modelo.Endereco.Cep.Bairro = vm.Endereco.Bairro;
                modelo.Endereco.Cep.CepNumero = vm.Endereco.Cep;
                modelo.Endereco.Cep.Cidade = vm.Endereco.Cidade;
                modelo.Endereco.Cep.Estado = vm.Endereco.Estado;
                modelo.Endereco.Complemento = vm.Endereco.Complemento;
                modelo.Endereco.Numero = vm.Endereco.Numero;
                modelo.Endereco.Cep.Logradouro = vm.Endereco.Logradouro;
            }

            modelo.Escolaridade = vm.EscolaridadeId;


            modelo.DisponibilidadeHorario = vm.DisponibilidadeHorario;
            modelo.DisponibilidadeViagem = vm.DisponibilidadeViagem;
            modelo.PossuiVeiculo = vm.PossuiVeiculo;


            return modelo;
        }

        public ActionResult HomeModelo()
        {
            return View();
        }

        [HttpPost]
        public JsonResult InserirImagens(IList<HttpPostedFileBase> imagens)
        {
            try
            {
                if (imagens == null)
                    return Json(new { });
                var usuarioId = User.Identity.GetUserId();
                _modeloApplication.InserirImagens(usuarioId, imagens);
                return Json(new { message = "success" });
            }
            catch (Exception ex)
            {
                return Json(new { error = $"error: {ex}" });
            }
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            ModeloRegistrarViewModel modelo = new ModeloRegistrarViewModel();
            modelo.Sexo = default(SexoEnum).ToSelectListItem(modelo?.SexoId);
            modelo.EstadoCivil = default(EstadoCivilEnum).ToSelectListItem(modelo.EstadoCivilId);

            ModeloSocialLogin modeloSocialLogin = (ModeloSocialLogin)TempData["MODELO_SOCIAL_LOGIN"] ?? null;

            modelo.Cpf = modeloSocialLogin?.Cpf;
            modelo.Email = modeloSocialLogin?.FacebookEmail;
            modelo.FacebookEmail = modeloSocialLogin?.FacebookEmail;
            modelo.FacebookToken = modeloSocialLogin?.FacebookToken;

            return View(modelo);
        }

        [HttpPost]
        public async Task<ActionResult> Registrar(ModeloRegistrarViewModel modelo)
        {
            if (ModelState.IsValid)
            {
                var modeloDomain = new Modelo();

                modeloDomain.DadosPessoais = new DadosPessoais
                {
                    Celular = string.Empty,
                    DataNascimento = modelo?.DataNascimento,
                    Telefone = string.Empty,
                    Email = modelo?.Email,

                    EstadoCivil = (EstadoCivilEnum)(modelo?.EstadoCivilId ?? 0),
                    PrimeiroNome = modelo?.Nome,
                    Sexo = (SexoEnum)(modelo?.SexoId ?? 0),
                    UltimoNome = modelo?.Sobrenome
                };
                modeloDomain.Documento = new Documento
                {
                    Cpf = new Cpf
                    {
                        Numero = modelo.Cpf,
                        IsValido = true
                    }
                };

                var usuario = new Usuario
                {
                    UserName = modelo.Email,
                    Email = modelo.Email,
                    CPF = modelo.Cpf,
                    PasswordHash = UserManager.PasswordHasher.HashPassword(modelo.Senha),
                    Perfild = (int)PerfilAcessoEnum.Modelo,//Modelo,
                    FacebookEmail = modelo.FacebookEmail,
                    FacebookToken = modelo.FacebookToken
                };

                await _modeloApplication.Registrar(modeloDomain, usuario);

                //modelo.Sexo = default(SexoEnum).ToSelectListItem(modelo?.SexoId);
                //modelo.EstadoCivil = default(EstadoCivilEnum).ToSelectListItem(modelo.EstadoCivilId);


                var result = await SignInManager.PasswordSignInAsync(modelo.Email, modelo.Senha, true, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToAction("Editar", "Modelo", new { usuario.Id });
                    //return RedirectToAction("Index", "Home");
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = "", RememberMe = true });
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(modelo);
                }
            }
            else
            {
                return View(modelo);
            }


        }

        // GET: Modelo/Cadastrar
        public ActionResult Cadastrar()
        {
            CarregarSelects();
            var vm = new ModeloBaseViewModel();

            vm.Idiomas = _idiomaApplication.Listar().Select(ToIdiomaViewModel).ToList();

            return View(vm);
        }


        public ActionResult Perfil()
        {
            var userid = User.Identity.GetUserId();
            var modelo = _modeloApplication.Obter(x => x.IdUsuario == userid).FirstOrDefault();
            return RedirectToAction("Editar/" + modelo.Id.ToString());
        }


        [HttpPost]
        public ActionResult SalvarDadosPessoais(ModeloPerfilMainViewModel main)
        {
            var model = main.DadosPessoais;

            var modelMain = new ModeloPerfilMainViewModel();
            modelMain.DadosPessoais = model;

            return View("CriarPerfilModelo", modelMain);
        }
        // POST: Modelo/Create
        [HttpPost]
        public async Task<ActionResult> Cadastrar(ModeloBaseViewModel dadosView)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    SetValidationErrors(ModelState);

                    CarregarSelects();
                    return View(dadosView);
                }

                var modelo = ToModel(dadosView);

                var usuario = new Usuario
                {
                    UserName = dadosView.Email,
                    Email = dadosView.Email,
                    CPF = dadosView.Cpf,
                    PasswordHash = UserManager.PasswordHasher.HashPassword(dadosView.Password),
                    Perfild = 2 //Modelo

                };

                await Task.Run(() => _modeloApplication.Registrar(modelo, usuario));

                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                CarregarSelects();

                ModelState.AddModelError("error", ex.Message);
                return View(dadosView);
            }
        }

        public ActionResult Editar(string id)
        {
            try
            {
                Func<ActionResult> retornarViewCadastrar = () =>
                {
                    var viewModel = new ModeloBaseViewModel();
                    viewModel.Idiomas = _idiomaApplication.Listar().Select(ToIdiomaViewModel).ToList();
                    CarregarSelects();
                    return View("Cadastrar", viewModel);
                };

                if (string.IsNullOrEmpty(id)) return retornarViewCadastrar();

                var idModelo = 0;
                var isInt = int.TryParse(id, out idModelo);

                var modeloApp = _modeloApplication.Obter(m => m.IdUsuario == id).FirstOrDefault();
                if (modeloApp == null && !isInt) return retornarViewCadastrar();


                modeloApp = modeloApp ?? _modeloApplication.Obter(m => m.Id == idModelo).FirstOrDefault();
                if (modeloApp == null) return retornarViewCadastrar();


                CarregarSelects(modeloApp);
                var vm = ToViewModel(modeloApp);

                return View(vm);
            }
            catch (Exception ex)
            {
                CarregarSelects();
                var viewModel = new ModeloBaseViewModel();
                viewModel.Idiomas = _idiomaApplication.Listar().Select(ToIdiomaViewModel).ToList();
                Debug.WriteLine($"Erro: {ex}");
                return View(viewModel);
            }
        }

        [HttpPost]
        public ActionResult Editar(ModeloEditarViewModel dadosView)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    SetValidationErrors(ModelState);

                    CarregarSelects();
                    return View(dadosView);
                }

                var modelo = ToModel(dadosView as ModeloBaseViewModel);
                modelo.IdUsuario = dadosView.UsuarioId;


                _modeloApplication.Atualizar(modelo);

                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {

                CarregarSelects();
                Debug.WriteLine($"Erro: {ex}");
                ModelState.AddModelError("error", ex);

                return View(dadosView);
            }
        }

        public ActionResult EditarDadosPereciveis(Int32 id)
        {
            try
            {
                var modelo = _modeloApplication.Obter(id);
                var perfilModelo = modelo?.PerfilModelo;

                CarregarSelectsPereciveis(perfilModelo);

                var vm = ToEditarPereciveisModel(perfilModelo, id);
                return View(vm);
            }
            catch (Exception ex)
            {
                CarregarSelectsPereciveis();
                ModelState.AddModelError("error", "Não foi possivel carregar os dados da modelo");
                return View();
            }
        }

        [HttpPost]
        public ActionResult EditarDadosPereciveis(int modeloId, ModeloEditarPereciveisViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid) return View(modelo);


                var perfil = ToEditarPereciveisVm(modelo);
                _modeloApplication.AtualizarDadosPereciveis(modeloId, perfil);

                return RedirectToAction("Listar");

            }
            catch (Exception ex)
            {
                return View();

            }
        }

        public ViewResult Listar(String nome, String rg, String cpf, String DataNascimento)
        {
            try
            {
                nome = nome ?? string.Empty;
                rg = rg ?? string.Empty;
                cpf = cpf != null ? cpf.Replace(".", "").Replace("-", "") : string.Empty;
                DateTime? dataNascimentoFiltro = null;
                if (DataNascimento != null && DataNascimento != string.Empty) dataNascimentoFiltro = DataNascimento.ToDate();

                var modelos = _modeloApplication.Listar(nome, rg, cpf, dataNascimentoFiltro);
                var dados = modelos.Select(ToViewModel).ToList();
                return View(dados);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var list = new List<ModeloCadastrarViewModel>();
                return View(list);
            }
        }

        //COMPOSITE
        public ActionResult Composite()
        {
            return View();
        }

        public PartialViewResult ModeloComposite(Int32 id)
        {
            try
            {
                var modelo = _modeloApplication
                                        .Obter(id);

                var vm = ToModeloCompositeViewModel(modelo);

                return PartialView("_ModeloComposite", vm);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var vm = new ModeloBaseViewModel();
                return PartialView("_ModeloComposite", vm);
            }
        }

        public PartialViewResult ModeloSelecionadasCasting(Int32 idCasting)
        {
            try
            {
                var casting = _castingApplication.Obter(idCasting);
                var modelos = casting?.Modelos?.Select(ToModelosSelecionadasViewModel);


                return PartialView("_ModelosSelecionadasCasting", modelos);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var vm = new ModelosSelecionadasViewModel();
                return PartialView("_ModelosSelecionadasCasting", vm);
            }
        }

        [HttpPost]
        public async Task<JsonResult> CadastrarUsuario(ModeloUsuarioViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { error = GetValidationErrors(ModelState) }, JsonRequestBehavior.AllowGet);

            try
            {
                var modeloDomain = new Modelo();

                var usuario = new Usuario
                {
                    UserName = model.Email,
                    Email = model.Email,
                    CPF = model.Cpf,
                    PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password),
                    Perfild = 2 //Modelo

                };

                await Task.Run(() => _usuario.Registrar(usuario));

                var userId = await Task.Run(() => _usuario.VerificarUsuarioExistente(usuario).Id);

                return Json(userId);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> InserirDadosCadastrais(ModeloDadosCadastraisViewModel model)
        {

            if (!ModelState.IsValid)
                return Json(new { error = GetValidationErrors(ModelState) }, JsonRequestBehavior.AllowGet);
            try
            {
                var modelo = new Modelo()
                {
                    IdUsuario = model.UsuarioId,
                    Id = model?.ModeloId ?? 0,
                    DadosPessoais = new DadosPessoais
                    {
                        Celular = string.Empty,
                        DataNascimento = model?.DataNascimentoDadosPessoais ?? DateTime.Now,
                        Telefone = string.Empty,
                        Email = string.Empty,
                        EstadoCivil = (EstadoCivilEnum)(model?.EstadoCivilId ?? 0),
                        PrimeiroNome = model?.PrimeiroNomeDadosPessoais,
                        Sexo = (SexoEnum)(model?.SexoId ?? 0),
                        UltimoNome = model?.UltimoNomeDadosPessoais
                    }

                };

                if (modelo.Id == 0)
                {
                    await _modeloApplication.RegistrarAsync(modelo);
                    var modeloBanco = _modeloApplication.Obter(m => m.DadosPessoais.PrimeiroNome == model.PrimeiroNomeDadosPessoais && m.DadosPessoais.UltimoNome == model.UltimoNomeDadosPessoais).FirstOrDefault();
                    modelo.Id = modeloBanco.Id;
                    return Json(modelo.Id);

                }

                //var modeloDomain = _modeloApplication.Obter((int)modelo.Id);

                _modeloApplication.Atualizar(modelo);
                return Json(modelo.Id);

            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult InserirFoto(HttpPostedFileBase imagem)
        {
            try
            {
                if (imagem == null)
                    return Json(new { error = "nenhuma imagem foi selecionada" }, JsonRequestBehavior.AllowGet);

                var idUsuario = imagem.FileName.Split(new[] { "userId" }, StringSplitOptions.None).ElementAtOrDefault(1);
                if (idUsuario == null)
                    return Json(new { error = "a modelo nao foi cadastrada previamente" }, JsonRequestBehavior.AllowGet);

                var modelo = _modeloApplication.Obter(m => m.IdUsuario == idUsuario).FirstOrDefault();
                modelo.Fotos = modelo.Fotos ?? new List<FotoModelo>();

                var foto = new FotoModelo
                {
                    Descricao = imagem.FileName,
                    ImagemBinario = imagem.GetArrayBytes(),
                    Tipo = imagem.ContentType,
                    Principal = true
                };
                var contains = modelo.Fotos.Any(m => m.ImagemBinario == foto.ImagemBinario);
                if (contains)
                    return Json(new { error = "A foto escolhida ja foi inserida!" }, JsonRequestBehavior.AllowGet);
                modelo.Fotos.Insert(0, foto);
                _modeloApplication.Atualizar(modelo);

                return Json(true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult InserirEndereco(ModeloEnderecoViewModel model)
        {
            var modelo = _modeloApplication.Obter(model.ModeloId);

            modelo.Endereco = new Endereco
            {
                Cep = new Cep
                {
                    Cidade = model?.CidadeEndereco,
                    Estado = model?.EstadoEndereco,
                    Logradouro = model?.LogradouroEndereco,
                    Bairro = model?.BairroEndereco,
                    CepNumero = model?.CepEndereco,
                    GDE = model?.GDEEndereco,
                    UF = (UfEnum)(model?.UfEndereco ?? 0),
                    Zona = model?.ZonaEndereco
                },
                Complemento = model?.ComplementoEndereco,
                Numero = model?.NumeroEndereco
            };

            _modeloApplication.Atualizar(modelo);

            return Json(true);
        }

        public JsonResult InserirContato(ModeloContatoViewModel model)
        {
            var modelo = new Modelo();

            modelo.Id = model.ModeloId;
            modelo.DadosPessoais.Celular = model.CelularDadosPessoais;
            modelo.DadosPessoais.Telefone = model.TelefoneDadosPessoais;

            _modeloApplication.AtualizarContato(modelo);

            return Json(true);
        }

        public JsonResult InserirDocumentos(ModeloDocumentoViewModel model)
        {
            var modelo = new Modelo(); //_modeloApplication.Obter(model.ModeloId);

            modelo.Id = model.ModeloId;

            modelo.Documento = new Documento
            {
                Cpf = new Cpf { Numero = model?.Cpf, IsValido = true },
                CarteiraProfissional = model?.CateiraProfissionalDocumento,
                Pis = model?.PisDocumento,
                Rg = model?.RgDocumento

            };

            _modeloApplication.AtualizarDocumento(modelo);

            return Json(true);
        }

        public JsonResult InserirDadosBancarios(ModeloDadosBancariosViewModel model)
        {
            var modelo = new Modelo()
            {
                Id = model.ModeloId,
                Banco = new Banco
                {
                    Agencia = model?.AgenciaBanco,
                    Conta = model?.ContaBanco,
                    Nome = model?.NomeBanco,
                    TipoConta = (TipoContaEnum)(model?.TipoContaBanco ?? 0)
                }
            };

            _modeloApplication.AtualizarDadosBancarios(modelo);

            return Json(true);
        }

        public JsonResult InserirEscolaridade(ModeloEscolaridadeViewModel model)
        {
            var modelo = new Modelo()
            {
                Id = model.ModeloId,
                Escolaridade = (EscolaridadeEnum)(model.EscolaridadeId ?? 0)
            };

            _modeloApplication.AtualizarEscolaridade(modelo);

            return Json(true);
        }

        public JsonResult InserirIdioma(ModeloIdiomaViewModel model)
        {
            var modelo = new Modelo()
            {
                Id = model.ModeloId,
                Idioma = new Idioma
                {
                    Descricao = model?.DescricaoIdioma
                }
            };

            _modeloApplication.AtualizarIdioma(modelo);

            return Json(true);
        }

        public JsonResult InserirCaracteristicas(ModeloCaracteristicasViewModel model)
        {
            var modelo = new Modelo()
            {
                Id = model.ModeloId,
                PerfilModelo = new PerfilModelo
                {
                    Altura = model?.AlturaPerfiModelo ?? 0,
                    Busto = model?.BustoPerfiModelo ?? 0,
                    Calcado = model?.CalcadoPerfiModelo ?? 0,
                    Cintura = model?.CinturaPerfiModelo ?? 0,
                    CorCabelo = (CorCabeloEnum)(model?.CorCabeloPerfilModelo ?? 0),
                    CorOlhos = (CorOlhosEnum)(model?.CorOlhosPerfiModelo ?? 0),
                    CorPele = (CorPeleEnum)(model?.CorPelePerfiModelo ?? 0),
                    Manequim = model.ManequimPerfiModelo,
                    Outros = model?.OutrosPerfiModelo,
                    Peso = model?.PesoPerfiModelo ?? 0,
                    Quadril = model?.QuadrilPerfiModelo ?? 0
                },

                CategoriaModelo = (CategoriaModeloEnum)(model?.CategoriaModelo ?? 0),
                TipoModelo = (TipoModeloEnum)model?.TipoModelo,
                DataAprovacao = model?.DataAprovacao == DateTime.MinValue ? DateTime.Now : model?.DataAprovacao
            };

            _modeloApplication.AtualizarCaracteristicas(modelo);

            return Json(true);
        }

        public JsonResult InserirDadosComplementares(ModeloDadosComplementaresViewModel model)
        {
            var modelo = new Modelo()
            {
                Id = model.ModeloId,
                DisponibilidadeHorario = (model?.DisponibilidadeHorario ?? false),
                DisponibilidadeViagem = (model?.DisponibilidadeViagem ?? false),
                PossuiVeiculo = (model?.PossuiVeiculo ?? false),
            };

            _modeloApplication.AtualizarDadosComplementares(modelo);

            return Json(true);
        }


        #region helpers
        private ModelosSelecionadasViewModel ToModelosSelecionadasViewModel(Modelo modelo)
        {
            return new ModelosSelecionadasViewModel
            {
                NomeModelo = $"{modelo.DadosPessoais?.PrimeiroNome } {modelo.DadosPessoais?.UltimoNome }",
                ImagemSrc = modelo.Fotos.FirstOrDefault()?.ImagemBinario.GetSrcImage()
            };
        }

        private IList<IdiomaViewModel> ObterIdiomas(Modelo modelo)
        {


            var idiomasModel = _idiomaApplication.Listar();
            var todosIdiomas = idiomasModel.Select(m => new IdiomaViewModel
            {
                Descricao = m.Descricao ?? "Sem nome",
                IdiomaId = m.Id
            }).ToList();

            if (modelo == null) return todosIdiomas;

            foreach (var item in modelo?.Idiomas)
            {
                var idiomaSelecionado = todosIdiomas.FirstOrDefault(m => m.IdiomaId == item.IdiomaId);
                if (idiomaSelecionado == null) continue;
                idiomaSelecionado.Selecionado = true;
            }
            return todosIdiomas;
        }

        private ModeloCompositeViewModelo ToModeloCompositeViewModel(Modelo modeloComposite)
        {
            var fotos = modeloComposite?.Fotos?//.Where(m => m.Status == StatusFotoModeloEnum.Aceito)
                                               .Select(MapearFotosModelo).ToList();

            var perfil = fotos.FirstOrDefault();//fotos.FirstOrDefault(m => m.Principal);

            var modeloItem = new ModeloCompositeViewModelo
            {
                ModeloId = modeloComposite?.Id ?? 0,
                PrimeiroNomeDadosPessoais = modeloComposite?.DadosPessoais?.PrimeiroNome,
                UltimoNomeDadosPessoais = modeloComposite?.DadosPessoais?.UltimoNome,
                AlturaPerfiModelo = modeloComposite?.PerfilModelo?.Altura,
                CalcadoPerfiModelo = modeloComposite?.PerfilModelo?.Calcado,
                CinturaPerfiModelo = modeloComposite?.PerfilModelo?.Cintura,
                CorCabeloPerfilModelo = (modeloComposite?.PerfilModelo?.CorCabelo ?? 0).ToString(),
                CorOlhosPerfiModeloTexto = (modeloComposite?.PerfilModelo?.CorOlhos ?? 0).ToString(),
                CorPelePerfiModelo = (Int32)(modeloComposite?.PerfilModelo?.CorPele ?? 0),
                PesoPerfiModelo = modeloComposite?.PerfilModelo?.Peso,
                QuadrilPerfiModelo = modeloComposite?.PerfilModelo?.Quadril,
                ManequimPerfiModelo = modeloComposite?.PerfilModelo?.Manequim,
                FotosExistentes = fotos,
                FotoPerfil = perfil?.ImageURL
            };

            return modeloItem;
        }//COMPOSITE

        private ModeloBaseViewModel ToViewModel(Modelo modelo)
        {


            var modeloItem = new ModeloCadastrarViewModel
            {
                ModeloId = modelo?.Id ?? 0,
                UsuarioId = modelo?.IdUsuario,
                Cpf = modelo?.Documento?.Cpf?.Numero,
                DataAprovacao = modelo?.DataAprovacao,
                DataNascimentoDadosPessoais = modelo?.DadosPessoais?.DataNascimento,
                CateiraProfissionalDocumento = modelo?.Documento?.CarteiraProfissional,
                PisDocumento = modelo?.Documento?.Pis,
                RgDocumento = modelo?.Documento?.Rg,
                PrimeiroNomeDadosPessoais = modelo?.DadosPessoais?.PrimeiroNome,
                UltimoNomeDadosPessoais = modelo?.DadosPessoais?.UltimoNome,
                AgenciaBanco = modelo?.Banco?.Agencia,
                AlturaPerfiModelo = modelo?.PerfilModelo?.Altura,
                BairroEndereco = modelo?.Endereco?.Cep?.Bairro,
                BustoPerfiModelo = modelo?.PerfilModelo?.Busto,
                CalcadoPerfiModelo = modelo?.PerfilModelo?.Calcado,
                CategoriaModelo = (Int32)(modelo?.CategoriaModelo ?? 0),
                CelularDadosPessoais = modelo?.DadosPessoais?.Celular,
                CepEndereco = modelo?.Endereco?.Cep?.CepNumero,
                CidadeEndereco = modelo?.Endereco?.Cep?.Cidade,
                CinturaPerfiModelo = modelo?.PerfilModelo?.Cintura,
                ComplementoEndereco = modelo?.Endereco?.Complemento,
                ContaBanco = modelo?.Banco?.Conta,
                CorCabeloPerfilModelo = (Int32)(modelo?.PerfilModelo?.CorCabelo ?? 0),
                //IdCorCabeloPerfilModelo = modelo?.PerfilModelo?.CorCabelo?.Id,
                CorOlhosPerfiModeloTexto = (modelo?.PerfilModelo?.CorOlhos ?? 0).ToString(),
                CorOlhosPerfiModelo = (Int32)(modelo?.PerfilModelo?.CorOlhos ?? 0),
                CorPelePerfiModelo = (Int32)(modelo?.PerfilModelo?.CorPele ?? 0),
                Idiomas = ObterIdiomas(modelo),
                //todo: arrumar tatoo
                DescricaoTatuagem = modelo?.Tatuagem?.Descricao,
                IdTatuagem = modelo?.Tatuagem?.Id,
                ParteCorpoTatuagem = modelo?.Tatuagem?.ParteCorpo,


                DisponibilidadeHorario = modelo?.DisponibilidadeHorario ?? false,
                DisponibilidadeViagem = modelo?.DisponibilidadeViagem ?? false,
                Email = modelo?.DadosPessoais?.Email,
                EstadoCivilIdDadosPessoais = (Int32)(modelo?.DadosPessoais?.EstadoCivil ?? 0),
                EstadoEndereco = modelo?.Endereco?.Cep?.Estado,
                Escolaridade = modelo.Escolaridade,
                GDEEndereco = modelo?.Endereco?.Cep?.GDE,
                IdBanco = modelo?.Banco?.Id,
                IdEndereco = modelo?.Endereco?.Id,
                //IdIdioma = modelo?.Idioma?.Id ?? 0,
                IdPerfilModelo = modelo?.PerfilModelo?.Id ?? 0,
                LogradouroEndereco = modelo?.Endereco?.Cep?.Logradouro,
                ManequimPerfiModelo = modelo?.PerfilModelo?.Manequim,
                NomeBanco = modelo?.Banco?.Nome,
                NumeroEndereco = modelo?.Endereco?.Numero,
                OutrosPerfiModelo = modelo?.PerfilModelo?.Outros,
                Passaporte = modelo?.Documento?.Passaporte,
                PesoPerfiModelo = modelo?.PerfilModelo?.Peso,
                PossuiVeiculo = modelo?.PossuiVeiculo ?? false,
                QuadrilPerfiModelo = modelo?.PerfilModelo?.Quadril,
                SexoIdDadosPessoais = (Int32)(modelo?.DadosPessoais?.Sexo ?? 0),
                StatusModelo = (Int32)(modelo?.StatusModelo ?? 0),
                //SuperiorEscolaridade = modelo?.Escolaridade?.Superior,
                //TecnicoEscolaridade = modelo?.Escolaridade?.Tecnico,             
                TelefoneDadosPessoais = modelo?.DadosPessoais?.Telefone,
                TipoContaBanco = (Int32)(modelo?.Banco?.TipoConta ?? 0),
                TipoModelo = (Int32)(modelo?.TipoModelo ?? 0),
                ZonaEndereco = modelo?.Endereco?.Cep?.Zona//,
                                                          //FotosExistentes = modelo?.Fotos?.Select(MapearFotosModelo).ToList(),
            };
            return modeloItem;

        }

        private Modelo ToModel(ModeloBaseViewModel modelo)
        {
            return new Modelo
            {

                StatusModelo = StatusModeloEnum.Aprovada,
                Id = modelo?.ModeloId ?? 0,
                DadosPessoais = new DadosPessoais
                {
                    Celular = modelo?.CelularDadosPessoais,
                    DataNascimento = modelo?.DataNascimentoDadosPessoais ?? DateTime.Now,
                    Telefone = modelo?.TelefoneDadosPessoais,
                    Email = modelo?.Email,
                    EstadoCivil = (EstadoCivilEnum)(modelo?.EstadoCivilIdDadosPessoais ?? 0),
                    PrimeiroNome = modelo?.PrimeiroNomeDadosPessoais,
                    Sexo = (SexoEnum)(modelo?.SexoIdDadosPessoais ?? 0),
                    UltimoNome = modelo?.UltimoNomeDadosPessoais
                },

                Documento = new Documento
                {
                    Cpf = new Cpf { Numero = modelo?.Cpf, IsValido = true },
                    CarteiraProfissional = modelo?.CateiraProfissionalDocumento,
                    Pis = modelo?.PisDocumento,
                    Rg = modelo?.RgDocumento

                },
                Banco = new Banco
                {
                    Agencia = modelo?.AgenciaBanco,
                    Conta = modelo?.ContaBanco,
                    Nome = modelo?.NomeBanco,
                    TipoConta = (TipoContaEnum)(modelo?.TipoContaBanco ?? 0)
                },
                Escolaridade = modelo.Escolaridade,

                //Idioma = new Idioma
                //{
                //    Descricao = modelo?.DescricaoIdioma
                //},
                PerfilModelo = new PerfilModelo
                {
                    Altura = modelo?.AlturaPerfiModelo ?? 0,
                    Busto = modelo?.BustoPerfiModelo ?? 0,
                    Calcado = modelo?.CalcadoPerfiModelo ?? 0,
                    Cintura = modelo?.CinturaPerfiModelo ?? 0,
                    CorCabelo = (CorCabeloEnum)(modelo?.CorCabeloPerfilModelo ?? 0),
                    CorOlhos = (CorOlhosEnum)(modelo?.CorOlhosPerfiModelo ?? 0),
                    CorPele = (CorPeleEnum)(modelo?.CorPelePerfiModelo ?? 0),
                    Manequim = (decimal)modelo?.ManequimPerfiModelo,
                    Outros = modelo?.OutrosPerfiModelo,
                    Peso = modelo?.PesoPerfiModelo ?? 0,
                    Quadril = modelo?.QuadrilPerfiModelo ?? 0
                },
                //todo: colocar pra receber mais de uma tatoo
                Tatuagem =
                    new Tatuagem
                    {
                        Descricao = modelo?.DescricaoTatuagem,
                        ParteCorpo = modelo?.ParteCorpoTatuagem
                    },
                Endereco = new Endereco
                {
                    Cep = new Cep
                    {
                        Cidade = modelo?.CidadeEndereco,
                        Estado = modelo?.EstadoEndereco,
                        Logradouro = modelo?.LogradouroEndereco,
                        Bairro = modelo?.BairroEndereco,
                        CepNumero = modelo?.CepEndereco,
                        GDE = modelo?.GDEEndereco,
                        UF = (UfEnum)(modelo?.UfEndereco ?? 0),
                        Zona = modelo?.ZonaEndereco
                    },
                    Complemento = modelo?.ComplementoEndereco,
                    Numero = modelo?.NumeroEndereco,

                },
                CategoriaModelo = (CategoriaModeloEnum)(modelo?.CategoriaModelo ?? 0),
                TipoModelo = (TipoModeloEnum)modelo?.TipoModelo,
                DisponibilidadeHorario = (modelo?.DisponibilidadeHorario ?? false),
                DisponibilidadeViagem = (modelo?.DisponibilidadeViagem ?? false),
                PossuiVeiculo = (modelo?.PossuiVeiculo ?? false),
                DataAprovacao = modelo?.DataAprovacao


            };
        }

        private ModeloEditarPereciveisViewModel ToEditarPereciveisModel(PerfilModelo perfilModelo, Int32 id)
        {
            var vm = new ModeloEditarPereciveisViewModel
            {
                ModeloId = id,
                AlturaPerfiModelo = perfilModelo?.Altura ?? 0,
                BustoPerfiModelo = perfilModelo?.Busto ?? 0,
                CalcadoPerfiModelo = perfilModelo?.Calcado ?? 0,
                CinturaPerfiModelo = perfilModelo?.Cintura ?? 0,
                CorCabeloPerfilModelo = (Int32)perfilModelo?.CorCabelo,
                //IdCorCabeloPerfilModelo = perfilModelo?.CorCabelo?.Id,
                CorOlhosPerfiModelo = (Int32)perfilModelo?.CorOlhos,
                CorPelePerfiModelo = (Int32)perfilModelo?.CorPele,
                ManequimPerfiModelo = (decimal)perfilModelo?.Manequim,
                OutrosPerfiModelo = perfilModelo?.Outros,
                PesoPerfiModelo = perfilModelo?.Peso ?? 0,
                QuadrilPerfiModelo = perfilModelo?.Quadril ?? 0

            };
            return vm;
        }

        private PerfilModelo ToEditarPereciveisVm(ModeloEditarPereciveisViewModel modelo)
        {
            var perfil = new PerfilModelo
            {
                Altura = (modelo?.AlturaPerfiModelo ?? 0),
                Busto = (modelo?.BustoPerfiModelo ?? 0),
                Calcado = (modelo?.CalcadoPerfiModelo ?? 0),
                Cintura = (modelo?.CinturaPerfiModelo ?? 0),
                CorCabelo = (CorCabeloEnum)(modelo?.CorCabeloPerfilModelo ?? 0),
                CorOlhos = (CorOlhosEnum)(modelo?.CorOlhosPerfiModelo ?? 0),
                CorPele = (CorPeleEnum)(modelo?.CorPelePerfiModelo ?? 0),
                Manequim = (decimal)modelo?.ManequimPerfiModelo,
                Outros = modelo?.OutrosPerfiModelo,
                Peso = (modelo?.PesoPerfiModelo ?? 0),
                Quadril = (modelo?.QuadrilPerfiModelo ?? 0)
            };
            return perfil;
        }

        private IdiomaViewModel ToIdiomaViewModel(Idioma idioma)
        {
            return new IdiomaViewModel { Descricao = idioma.Descricao, IdiomaId = idioma.Id };
        }
        private void CarregarSelects(Modelo modelo = null)
        {

            var sexos = default(SexoEnum).ToSelectListItem(modelo?.DadosPessoais?.Sexo);
            var statusModelo = default(StatusModeloEnum).ToSelectListItem(modelo?.StatusModelo);
            var tipoModelo = default(TipoModeloEnum).ToSelectListItem(modelo?.TipoModelo);
            var estadoCivil = default(EstadoCivilEnum).ToSelectListItem(modelo?.DadosPessoais?.EstadoCivil);
            var corPeleEnum = default(CorPeleEnum).ToSelectListItem(modelo?.PerfilModelo?.CorPele);
            var corOlhosEnum = default(CorOlhosEnum).ToSelectListItem(modelo?.PerfilModelo?.CorOlhos);
            var tipoContaEnum = default(TipoContaEnum).ToSelectListItem(modelo?.Banco?.TipoConta);
            var categoria = default(CategoriaModeloEnum).ToSelectListItem(modelo?.CategoriaModelo);
            var corCabeloEnum = default(CorCabeloEnum).ToSelectListItem(modelo?.PerfilModelo?.CorCabelo);
            var escolaridade = default(EscolaridadeEnum).ToSelectListItem(modelo?.Escolaridade);

            ViewBag.Sexos = sexos;
            ViewBag.StatusModelo = statusModelo;
            ViewBag.TipoModelo = tipoModelo;
            ViewBag.EstadoCivil = estadoCivil;
            ViewBag.CorPele = corPeleEnum;
            ViewBag.CorOlhos = corOlhosEnum;
            ViewBag.TipoConta = tipoContaEnum;
            ViewBag.CategoriaModelo = categoria;
            ViewBag.CorCabelo = corCabeloEnum;
            ViewBag.Escolaridade = escolaridade;
        }

        private void CarregarSelectsPereciveis(PerfilModelo perfilModelo = null)
        {
            var corPele = default(CorPeleEnum).ToSelectListItem(perfilModelo?.CorPele);
            var corOlhos = default(CorOlhosEnum).ToSelectListItem(perfilModelo?.CorOlhos);
            var corCabelo = default(CorCabeloEnum).ToSelectListItem(perfilModelo?.CorCabelo);

            ViewBag.CorOlhos = corOlhos;
            ViewBag.CorPele = corPele;
            ViewBag.CorCabelo = corCabelo;
        }

        private FacebookViewModel MapearFotosModelo(FotoModelo foto)
        {
            var facebook = new FacebookViewModel
            {
                ImageURL = foto.ImagemBinario.GetSrcImage(),
                Id = foto.Id,
                Principal = foto.Principal ?? false,
                Status = foto.Status.Value.ToString()

            };
            return facebook;
        }

        private async Task<IList<FacebookViewModel>> ObterFotosFacebook(String idUsuario)
        {
            if (string.IsNullOrWhiteSpace(idUsuario)) return new List<FacebookViewModel>();

            var user = await UserManager.FindByIdAsync(idUsuario);

            var fotos = await _usuario.ObterFotosUsuarioAsync(user?.FacebookToken);
            var vm = fotos.Select(m => new FacebookViewModel()
            {
                Id = m.Id,
                ImageURL = m.ImageURL
            }).ToList();

            return vm;
        }


        #endregion
    }
}

