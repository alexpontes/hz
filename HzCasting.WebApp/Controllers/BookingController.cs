﻿using HzCasting.Application;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Repositories;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Models.Modelos;
using HzCasting.Infra.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.Services;
using System.Diagnostics;

namespace HzCasting.WebApp.Controllers
{
    public class BookingController : Base.BaseController
    {
        private readonly ICastingApplication _castingApplication;
        private readonly IModeloApplication _modeloApplication;
        private readonly IBookingApplication _bookingApplication;
        private readonly IInviteApplication _inviteApp;

        public BookingController(ICastingApplication castingApplication, IModeloApplication modeloApplication,
            IBookingApplication bookingApplication, IInviteApplication inviteApp)
        {
            _castingApplication = castingApplication;
            _modeloApplication = modeloApplication;
            _bookingApplication = bookingApplication;
            _inviteApp = inviteApp;
            //_viewModel = new ClienteViewModel();
        }
        // GET: Booking
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult Editar()
        {
            return View();
        }

        public ViewResult ModeloSelecionadasCasting(Int64 idCasting, Int64 perfilId = 0)
        {
            try
            {
                var casting = _castingApplication.ListarModelosCasting(idCasting)
                    .Select(ToModelosSelecionadasViewModel);

                ViewBag.CastingId = idCasting;
                ViewBag.PerfilId = perfilId;
                return View(casting);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var vm = new ModelosSelecionadasViewModel();
                return View(vm);
            }
        }

        public ActionResult ModelosCompativeis(Int32 perfilId, Int32 castingId = 0)
        {
            try
            {
                string nomePerfil = string.Empty;
                var modelos = _modeloApplication.ObterModelosPerfilFiltro(perfilId, castingId, out nomePerfil)
                                    .Select(m => ToBookingViewModel(m, nomePerfil, castingId, perfilId));
                return View(modelos);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                var vm = new List<BookingViewModel>();
                return View(vm);
            }
        }

        public ActionResult Listar(Int32 castingId, Int32 perfilId = 0)
        {
            try
            {
                ViewBag.CastingId = castingId;
                ViewBag.PerfiId = perfilId;
                var booking = _bookingApplication.Obter(m => m.PerfilFiltro?.Casting?.Id == castingId &&
                                                            (perfilId == 0 || m.PerfilFiltro?.Id == perfilId))
                                             .Select(m => ToBookingViewModel(m, castingId));
                
                return View(booking);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                var vm = new List<BookingViewModel>();
                return View(vm);
            }
        }

        private BookingViewModel ToBookingViewModel(Booking booking, int castingId)
        {
            var invite = _inviteApp.Obter(m => m.Booking?.Id == booking.Id).FirstOrDefault();
            return new BookingViewModel
            {
                IdBooking = booking.Id,
                IdModelo = booking.Modelo.Id,
                IdCasting = castingId,
                IdPerfil = (Int32)booking.PerfilFiltro.Id,
                ImagemModelo = booking.Modelo.Fotos.FirstOrDefault()?.ImagemBinario.GetSrcImage() ?? string.Empty,
                NomeModelo = $"{booking.Modelo.DadosPessoais.PrimeiroNome} {booking.Modelo.DadosPessoais.UltimoNome}",
                NomePerfil = booking.PerfilFiltro.Titulo,
                TípoSelecao = booking.TipoSelecao.ToString(),
                DataEnvioInvite = invite?.DataEnvio,
                DataRetornoInvite = invite?.DataRetorno,
                StatusInvite = invite?.StatusTrackInvite.ToString(),
                Selecionada = booking.PerfilFiltro?.Casting?.Id == castingId
            };
        }

        public ActionResult Visualizar(Int32 castingId = 0)
        {
            try
            {

                return View();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                var vm = new List<BookingViewModel>();
                return View(vm);
            }
        }


        public ActionResult AdicionarModeloAoBooking(Int32 castingId, Int32 idModelo, Int32 perfilId)
        {
            try
            {
                _bookingApplication.Registrar(castingId, idModelo, perfilId);
                return RedirectToAction("ModelosCompativeis", new { perfilId = perfilId, castingId = castingId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return RedirectToAction("ModelosCompativeis", new { perfilId = perfilId, castingId = castingId });
            }
        }

        #region Helpers
        private ModelosSelecionadasViewModel ToModelosSelecionadasViewModel(Modelo modelo)
        {
            return new ModelosSelecionadasViewModel
            {
                NomeModelo = $"{modelo.DadosPessoais?.PrimeiroNome } {modelo.DadosPessoais?.UltimoNome }",
                ImagemSrc = modelo.Fotos.FirstOrDefault()?.ImagemBinario.GetSrcImage()
            };
        }

        private static ModeloBaseViewModel ToModeloBaseViewModel(Modelo modelo)
        {
            var modeloItem = new ModeloBaseViewModel
            {
                ModeloId = modelo?.Id ?? 0,
                UsuarioId = modelo?.IdUsuario,
                Cpf = modelo?.Documento?.Cpf?.Numero,
                DataAprovacao = modelo?.DataAprovacao,
                DataNascimentoDadosPessoais = modelo?.DadosPessoais?.DataNascimento,
                CateiraProfissionalDocumento = modelo?.Documento?.CarteiraProfissional,
                PisDocumento = modelo?.Documento?.Pis,
                RgDocumento = modelo?.Documento?.Rg,
                PrimeiroNomeDadosPessoais = modelo?.DadosPessoais?.PrimeiroNome,
                UltimoNomeDadosPessoais = modelo?.DadosPessoais?.UltimoNome,
                AgenciaBanco = modelo?.Banco?.Agencia,
                AlturaPerfiModelo = modelo?.PerfilModelo?.Altura,
                BairroEndereco = modelo?.Endereco?.Cep?.Bairro,
                BustoPerfiModelo = modelo?.PerfilModelo?.Busto,
                CalcadoPerfiModelo = modelo?.PerfilModelo?.Calcado,
                CategoriaModelo = (Int32)(modelo?.CategoriaModelo ?? 0),
                CelularDadosPessoais = modelo?.DadosPessoais?.Celular,
                CepEndereco = modelo?.Endereco?.Cep?.CepNumero,
                CidadeEndereco = modelo?.Endereco?.Cep?.Cidade,
                CinturaPerfiModelo = modelo?.PerfilModelo?.Cintura,
                ComplementoEndereco = modelo?.Endereco?.Complemento,
                ContaBanco = modelo?.Banco?.Conta,
                CorCabeloPerfilModelo = (Int32)(modelo?.PerfilModelo?.CorCabelo ?? 0),
				//IdCorCabeloPerfilModelo = modelo?.PerfilModelo?.CorCabelo?.Id,
                CorOlhosPerfiModelo = (Int32)(modelo?.PerfilModelo?.CorOlhos ?? 0),
                CorPelePerfiModelo = (Int32)(modelo?.PerfilModelo?.CorPele ?? 0),
                //DescricaoIdioma = modelo?.Idioma?.Descricao,
                //todo: arrumar tatoo
                DescricaoTatuagem = modelo?.Tatuagem?.Descricao,
                IdTatuagem = modelo?.Tatuagem?.Id,
                ParteCorpoTatuagem = modelo?.Tatuagem?.ParteCorpo,


                DisponibilidadeHorario = modelo?.DisponibilidadeHorario ?? false,
                DisponibilidadeViagem = modelo?.DisponibilidadeViagem ?? false,
                Email = modelo?.DadosPessoais?.Email,
                EstadoCivilIdDadosPessoais = (Int32)(modelo?.DadosPessoais?.EstadoCivil ?? 0),
                EstadoEndereco = modelo?.Endereco?.Cep?.Estado,
                Escolaridade = modelo.Escolaridade,
                GDEEndereco = modelo?.Endereco?.Cep?.GDE,
                IdBanco = modelo?.Banco?.Id,
                IdEndereco = modelo?.Endereco?.Id,
                //IdIdioma = modelo?.Idioma?.Id ?? 0,
                IdPerfilModelo = modelo?.PerfilModelo?.Id ?? 0,
                LogradouroEndereco = modelo?.Endereco?.Cep?.Logradouro,
                ManequimPerfiModelo = modelo?.PerfilModelo?.Manequim,
                NomeBanco = modelo?.Banco?.Nome,
                NumeroEndereco = modelo?.Endereco?.Numero,
                OutrosPerfiModelo = modelo?.PerfilModelo?.Outros,
                Passaporte = modelo?.Documento?.Passaporte,
                PesoPerfiModelo = modelo?.PerfilModelo?.Peso,
                PossuiVeiculo = modelo?.PossuiVeiculo ?? false,
                QuadrilPerfiModelo = modelo?.PerfilModelo?.Quadril,
                SexoIdDadosPessoais = (Int32)(modelo?.DadosPessoais?.Sexo ?? 0),
                StatusModelo = (Int32)(modelo?.StatusModelo ?? 0),             
                TelefoneDadosPessoais = modelo?.DadosPessoais?.Telefone,
                TipoContaBanco = (Int32)(modelo?.Banco?.TipoConta ?? 0),
                TipoModelo = (Int32)(modelo?.TipoModelo ?? 0),
                ZonaEndereco = modelo?.Endereco?.Cep?.Zona,
                FotosExistentes = modelo?.Fotos?.Select(MapearFotosModelo).ToList(),
            };
            return modeloItem;

        }

        private static FacebookViewModel MapearFotosModelo(FotoModelo foto)
        {
            var facebook = new FacebookViewModel
            {
                ImageURL = foto.ImagemBinario.GetSrcImage(),
                Id = foto.Id

            };
            return facebook;
        }

        private BookingViewModel ToBookingViewModel(Modelo modelo, String nomePerfil, Int32 castingId = 0, Int32 perfilId = 0)
        {

            return new BookingViewModel
            {
                IdModelo = modelo.Id,
                NomePerfil = nomePerfil,
                ImagemModelo = modelo.Fotos?.FirstOrDefault()?.ImagemBinario.GetSrcImage(),
                NomeModelo = $"{ modelo.DadosPessoais?.PrimeiroNome} { modelo.DadosPessoais?.UltimoNome}",

                Selecionada = modelo.Bookings.Any(m => m.Modelo.Castings.Any(n => n.Id == castingId)),
                IdCasting = castingId,
                IdPerfil = perfilId
            };

        }
        #endregion
    }

}