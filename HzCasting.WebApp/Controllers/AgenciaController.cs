﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Common;
using HzCasting.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;

namespace HzCasting.WebApp.Controllers
{
    public class AgenciaController : Base.BaseController
    {
        private readonly IAgenciaApplication _agenciaApplication;

        public AgenciaController(IAgenciaApplication agenciaApplication)
        {
            _agenciaApplication = agenciaApplication;
        }
        // GET: Agencia
        public ActionResult Cadastrar()
        {
            CarregarSelects();
            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(AgenciaViewModel dadosView)
        {
            try
            {
                UfEnum ufEnum = (UfEnum)Enum.Parse(typeof(UfEnum), dadosView.EstadoEndereco);

                dadosView.UFEndereco = (int)ufEnum;

                if (!ModelState.IsValid)
                {
                    CarregarSelects();
                    return View(dadosView);
                }
                var model = ToAgenciaModel(dadosView);

                _agenciaApplication.Registrar(model);

                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
                CarregarSelects();
                return View(dadosView);
            }
        }


        public ActionResult Listar(String nome, String cnpj, String email)
        {
            try
            {
                nome = nome ?? String.Empty;
                cnpj = cnpj ?? String.Empty;
                email = email ?? String.Empty;
                
                var dados = _agenciaApplication.Listar(nome, cnpj, email);
                var agencia = dados.Select(ToAgenciaViewModel);

                return View(agencia);

            }
            catch (Exception ex)
            {
                var list = new List<AgenciaViewModel>();
                Debug.Write(ex);
                return View(list);
            }

        }


        public RedirectToRouteResult Deletar(Int32 id)
        {
            try
            {
                _agenciaApplication.Deletar(id);
                return RedirectToAction("Listar");

            }
            catch (Exception ex)
            {
                Debug.Write(ex);
                return RedirectToAction("Listar");
            }


        }


        public ActionResult Editar(Int32 id)
        {
            CarregarSelects();

            try
            {
                var model = _agenciaApplication.Obter(id);
                var vm = ToAgenciaViewModel(model);
                return View(vm);
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
                var model = new AgenciaViewModel();
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Editar(AgenciaViewModel dadosView)
        {
            try
            {
                UfEnum ufEnum = (UfEnum)Enum.Parse(typeof(UfEnum), dadosView.EstadoEndereco);

                dadosView.UFEndereco = (int)ufEnum;

                if (!ModelState.IsValid)
                {
                    CarregarSelects();
                    ModelState.AddModelError("", GetValidationErrors(ModelState));
                    return View(dadosView);
                }
                var model = ToAgenciaModel(dadosView);

                _agenciaApplication.Atualizar(model);

                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
                CarregarSelects();
                ModelState.AddModelError("", ex.Message);
                return View(dadosView);
            }
        }
        #region Helpers
        private static AgenciaViewModel ToAgenciaViewModel(Agencia dados)
        {
            var agenciaVm = new AgenciaViewModel
            {
                BairroEndereco = dados?.Endereco?.Cep?.Bairro,
                CepNumeroEndereco = dados?.Endereco?.Cep?.CepNumero,
                CidadeEndereco = dados?.Endereco?.Cep?.Cidade,
                ComplementoEndereco = dados?.Endereco?.Complemento,
                EstadoEndereco = dados?.Endereco?.Cep?.Estado,
                LogradouroEndereco = dados?.Endereco?.Cep?.Logradouro,
                //GDEEndereco = dados?.Endereco?.Cep?.GDE,
                EnderecoId = dados?.Endereco?.Id,
                NumeroEndereco = dados?.Endereco?.Numero,
                UFEndereco = (Int32)(dados?.Endereco?.Cep?.UF ?? 0),
                Celular = dados?.Celular,
                //ZonaEndereco = dados?.Endereco?.Cep?.Zona,
                Email = dados?.Email,
                Nome = dados?.Nome,
                CPNJ = dados?.CNPJ,
                Id = dados?.Id,
                Telefone = dados?.Telefone
            };
            return agenciaVm;
        }

        private static Agencia ToAgenciaModel(AgenciaViewModel dadosView)
        {
            var model = new Agencia
            {
                Celular = dadosView.Celular,
                CNPJ = dadosView.CPNJ.Replace(".", "").Replace("-", "").Replace("/", ""),
                Email = dadosView.Email,
                Endereco = new Endereco
                {
                    Cep = new Cep
                    {
                        Bairro = dadosView.BairroEndereco,
                        CepNumero = dadosView.CepNumeroEndereco,
                        Cidade = dadosView.CidadeEndereco,
                        Estado = dadosView.EstadoEndereco,
                        //GDE = dadosView.GDEEndereco,
                        UF = (UfEnum)dadosView.UFEndereco,
                        Logradouro = dadosView.LogradouroEndereco 
                    },
                    Complemento = dadosView.ComplementoEndereco,
                    Numero = dadosView.NumeroEndereco

                },
                Nome = dadosView.Nome,
                Telefone = dadosView.Telefone,
                Id = dadosView.Id ?? 0
            };
            return model;
        }

        private  void CarregarSelects(Endereco endereco = null)
        {

            var uf = default(UfEnum).ToSelectListItem();

            ViewBag.Uf = uf;

        }
        #endregion
    }
}