﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Common;
using HzCasting.WebApp.Controllers.Base;
using HzCasting.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.WebApp.Controllers
{

    public class EventoController : BaseController
    {
        private readonly IEventoApplication _eventoApplication;
        private readonly EventoViewModel _viewModel;
        private readonly IInviteApplication _inviteApplication;
        public EventoController(IEventoApplication eventoApplication, IInviteApplication inviteApplication)
        {
            _inviteApplication = inviteApplication;
            _eventoApplication = eventoApplication;
            _viewModel = new EventoViewModel();
        }

        public ViewResult Index()
        {
            return View();
        }

        public ViewResult Listar(string titulo, string dataInicio, string dataFim, string realizacao)
        {

            try
            {
                titulo = titulo ?? string.Empty;
                DateTime? dataInicioFiltro = null;
                DateTime? dataFimFiltro = null;
                if (dataInicio != null && dataInicio != string.Empty) dataInicioFiltro = dataInicio.ToDate();
                if (dataFim != null && dataFim != string.Empty) dataFimFiltro = dataFim.ToDate();

                var passado = realizacao == Realizacao.Passado.ToString();
                var futuro = realizacao == Realizacao.Futuro.ToString();

                var dados = _eventoApplication.Listar(titulo, dataInicioFiltro, dataFimFiltro, passado, futuro);
                var eventos = dados.Select(ToEventoViewModel).ToList();
                return View(eventos);


            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var eventos = new List<EventoViewModel>();
                return View(eventos);
            }
        }


        public ViewResult Cadastrar()
        {

            CarregarSelects();
            return View();
        }

        public ViewResult NoRadar()
        {
            return View();
        }

        public ViewResult Details(int id)
        {
            var evento = _eventoApplication.Obter(id);

            return View(ToEventoViewModel(evento));
        }

        public ActionResult EventosModelo()
        {
            var userId = User.Identity.GetUserId();

            var roles = ObterRoles();

            var eventos = _inviteApplication.Obter(m => (m.Booking.Modelo.IdUsuario == userId || roles.Contains("admin")) &&
                                                    m.StatusTrackInvite == StatusTrackInviteEnum.Aceito &&
                                                    m.Booking.StatusModeloCastingEnum == StatusModeloCastingEnum.Aprovada)
                                            .Select(m => m.Booking.PerfilFiltro.Casting.Evento)
                                            .Select(ToEventoViewModel);

            return View(eventos);
        }


        [HttpPost]
        public ActionResult Cadastrar(EventoViewModel dadosView)
        {
            try
            {
                UfEnum ufEnum = (UfEnum)Enum.Parse(typeof(UfEnum), dadosView.EstadoEndereco);

                dadosView.UFEndereco = (int)ufEnum;


                if (!ModelState.IsValid)
                {
                    CarregarSelects(null, dadosView.UFEndereco);
                    return View(dadosView);
                }

                var dataInicio = AdicionarHoras(dadosView.HoraInicio, dadosView.DataInicio);
                var dataFim = AdicionarHoras(dadosView.HoraFim, dadosView.DataFim);

                var evento = ToEventoModel(dadosView);

                _eventoApplication.Registrar(evento);

                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                CarregarSelects(null, dadosView.UFEndereco);
                Debug.WriteLine($"Erro: {ex}");
                ModelState.AddModelError("", ex.Message);
                return View(dadosView);
            }
        }


        public ActionResult Editar(Int32 id)
        {
            try
            {
                ViewBag.Id = id;
                var evento = _eventoApplication.Obter(id);
                var eventoViewModel = ToEventoViewModel(evento);
                CarregarSelects(null, eventoViewModel.UFEndereco);
                return View("Cadastrar", eventoViewModel);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return RedirectToAction("Listar");
            }
        }

        [HttpPost]
        public ActionResult Editar(EventoViewModel evento, Int64 idEvento = 0)
        {
            try
            {
                UfEnum ufEnum = (UfEnum)Enum.Parse(typeof(UfEnum), evento.EstadoEndereco);

                evento.UFEndereco = (int)ufEnum;

                if (!ModelState.IsValid)
                {
                    ViewBag.Id = idEvento;
                    CarregarSelects(null, evento.UFEndereco);
                    SetValidationErrors(ModelState);
                    return View("Cadastrar", evento);

                }

                var eventoModel = ToEventoModel(evento);
                eventoModel.Id = idEvento;
                _eventoApplication.Atualizar(eventoModel);

                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                CarregarSelects(null, evento.UFEndereco);
                Debug.WriteLine($"Erro: {ex}");
                ViewBag.Id = idEvento;
                return View("Cadastrar", evento);
            }
        }

        public RedirectToRouteResult Deletar(Int32 id)
        {
            try
            {

                _eventoApplication.Deletar(id);
                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return RedirectToAction("Listar");

            }
        }

        #region Private Methods
        private void CarregarSelects(Evento evento = null, Int32? idUF = null)
        {
            var ufEnum = (UfEnum)((int?)evento?.Endereco?.Cep?.UF ?? idUF ?? 0);
            var uf = default(UfEnum).ToSelectListItem(ufEnum);
            ViewBag.Uf = uf;

        }

        private DateTime AdicionarHoras(string hora, DateTime data)
        {

            var horaReferencia = hora.Split(':');
            var dataReferencia = data
                                            .AddHours(double.Parse(horaReferencia[0]))
                                            .AddMinutes(double.Parse(horaReferencia[1]));

            return dataReferencia;
        }

        private Evento ToEventoModel(EventoViewModel dadosView)
        {

            var dataInicio = AdicionarHoras(dadosView.HoraInicio, dadosView.DataInicio);
            var dataFim = AdicionarHoras(dadosView.HoraFim, dadosView.DataFim);
            var evento = new Evento
            {
                Id = (dadosView.EventoId ?? 0),
                DataFim = dataFim,
                DataInicio = dataInicio,
                Local = dadosView.Local,
                Obs = dadosView.Obs,
                Titulo = dadosView.Titulo,
                Endereco = new Endereco
                {
                    Cep = new Cep
                    {
                        CepNumero = dadosView.CepNumeroEndereco,
                        Bairro = dadosView.BairroEndereco,
                        Cidade = dadosView.CidadeEndereco,
                        Estado = dadosView.EstadoEndereco,
                        Logradouro = dadosView.LogradouroEndereco,
                        UF = (UfEnum)dadosView.UFEndereco
                    },
                    Complemento = dadosView.ComplementoEndereco,
                    Numero = dadosView.NumeroEndereco
                },
                Foto = new Foto
                {
                    Descricao = dadosView.FotoEvento?.FileName,
                    Tipo = dadosView.FotoEvento?.ContentType,
                    ImagemBinario = dadosView.FotoEvento?.GetArrayBytes()

                }
            };
            return evento;
        }

        private EventoViewModel ToEventoViewModel(Evento evento)
        {
            var minuteInicio = evento?.DataInicio.Minute.ToString();
            minuteInicio = minuteInicio.Length == 1 ? minuteInicio + "0" : minuteInicio;

            var minuteFim = evento?.DataFim.Minute.ToString();
            minuteFim = minuteFim.Length == 1 ? minuteFim + "0" : minuteFim;

            var eventoVm = new EventoViewModel
            {
                EventoId = (evento?.Id ?? 0),
                Titulo = evento?.Titulo,
                Local = evento?.Local,
                DataInicio = (evento?.DataInicio ?? DateTime.MinValue),
                DataFim = (evento?.DataFim ?? DateTime.MinValue),
                Obs = evento?.Obs,
                HoraFim = $"{evento?.DataFim.Hour}:{minuteFim}",
                HoraInicio = $"{evento?.DataInicio.Hour}:{minuteInicio}",

                BairroEndereco = evento?.Endereco?.Cep?.Bairro,
                CidadeEndereco = evento?.Endereco?.Cep?.Cidade,
                CepNumeroEndereco = evento?.Endereco?.Cep?.CepNumero,
                ComplementoEndereco = evento?.Endereco?.Complemento,
                EstadoEndereco = evento?.Endereco?.Cep?.Estado,
                LogradouroEndereco = evento?.Endereco?.Cep?.Logradouro,
                NumeroEndereco = evento?.Endereco?.Numero,
                UFEndereco = (int)(evento?.Endereco?.Cep?.UF ?? 0),
                EnderecoId = evento?.Endereco?.Id


            };

            return eventoVm;
        }
        #endregion

    }
}
