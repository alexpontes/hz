﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Mvc;
using System.Linq;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Controllers.Base;

namespace HzCasting.WebApp.Controllers
{
    //    [Authorize]
    public sealed class ClienteController : BaseController
    {
        private readonly ClienteViewModel _viewModel;
        private readonly IClienteApplication _clienteApplication;
        private readonly IUsuarioApplication _usuarioApplication;
        private readonly IEnderecoApplication _enderecoApplication;

        public ClienteController(IClienteApplication clienteApplication, IUsuarioApplication usuarioApplication,
                                 IEnderecoApplication enderecoApplication)
        {
            _clienteApplication = clienteApplication;
            _usuarioApplication = usuarioApplication;
            _enderecoApplication = enderecoApplication;

            _viewModel = new ClienteViewModel();
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cliente/Cadastrar
        public ActionResult Cadastrar()
        {
            return View();
        }

        // POST: Cliente/Create
        [HttpPost]
        public ActionResult Cadastrar(ClienteViewModel dadosView)
        {
            //UfEnum ufEnum = (UfEnum)Enum.Parse(typeof(UfEnum), dadosView.Endereco.Estado);

            //dadosView.Endereco.UF = (int)ufEnum;

            try
            {
                if (!ModelState.IsValid) return View(dadosView);

                var cliente = ToModel(dadosView);

                var clienteExistente = _clienteApplication.VerificarUsuarioExistente(cliente);


                if (clienteExistente)
                {

                    ModelState.AddModelError("", "CNPJ já cadastrado.");

                    return View(dadosView);
                }
                else
                {
                    _clienteApplication.Registrar(cliente);
                    return RedirectToAction("Listar");
                }




            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return View(dadosView);
            }
        }

        public ActionResult Editar(int clienteId)
        {
            var cliente = _clienteApplication.Obter(clienteId);


            var model = ToViewModel(cliente);

            return View(model);
        }

        // POST: Cliente/Create
        [HttpPost]
        public ActionResult Editar(ClienteViewModel dadosView)
        {
            try
            {
                //UfEnum ufEnum = (UfEnum)Enum.Parse(typeof(UfEnum), dadosView.Endereco.Estado);

                //dadosView.Endereco.UF = (int)ufEnum;

                if (!ModelState.IsValid) return View(dadosView);

                var cliente = ToModel(dadosView);
                var clienteExistente = _clienteApplication.VerificarUsuarioExistente(cliente);

                if (clienteExistente)
                {

                    ModelState.AddModelError("", "CNPJ já cadastrado.");

                    return View(dadosView);
                }


                _clienteApplication.Atualizar(cliente);
                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return View(dadosView);
            }
        }

        public RedirectToRouteResult Deletar(Int32 id)
        {
            try
            {
                _clienteApplication.Deletar(id);
                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {

                return RedirectToAction("Listar");
            }
        }

        public ViewResult Listar(String RazaoSocial, String NomeFantasia, String CPNJ)
        {
            try
            {
                RazaoSocial = RazaoSocial ?? string.Empty;
                NomeFantasia = NomeFantasia ?? string.Empty;
                CPNJ = CPNJ != null ? CPNJ.Replace(".", "").Replace("-", "").Replace("/", "") : string.Empty;


                var clientes = _clienteApplication.Listar(RazaoSocial, NomeFantasia, CPNJ);
                var dados = clientes.Select(ToViewModel).ToList();
                return View(dados);

                //.Select(m => new ClienteViewModel
                // {
                //     NomeFantasia = m.NomeFantasia,
                //     CPNJ = m.CPNJ,
                //     RazaoSocial = m.RazaoSocial,
                //     ClientId = m.Id
                // }).ToList();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var list = new List<ClienteViewModel>();
                return View(list);
            }
        }

        public ActionResult ListarUsuarios(int clienteId)
        {
            var usuarios = _usuarioApplication.Obter(x => x.ClienteId == clienteId);
            var cliente = _clienteApplication.Obter(clienteId);

            var model = new ListarUsuariosClienteViewModel()
            {
                ClienteId = clienteId,
                NomeFantasia = cliente.NomeFantasia
            };

            foreach (var item in usuarios)
            {
                model.Usuarios.Add(new UsuariosClienteViewModel()
                {
                    Cpf = item.CPF,
                    Email = item.Email,
                    UsuarioId = item.Id
                });
            }
            return View(model);
        }
        public ActionResult ListarUsuariosComFiltro(int clienteId, string cpf, string email)
        {
            
            var usuarios = _usuarioApplication.Obter(x => x.ClienteId == clienteId);
            if(cpf.Length > 0)
                usuarios = usuarios.Where(x => x.CPF.ToString().Contains(cpf));
            if(email.Length > 0)
            usuarios = usuarios.Where(x => x.Email.ToLower().ToString().Contains(email.ToLower()));
            var cliente = _clienteApplication.Obter(clienteId);

            var model = new ListarUsuariosClienteViewModel()
            {
                ClienteId = clienteId,
                NomeFantasia = cliente.NomeFantasia
            };

            foreach (var item in usuarios)
            {
                model.Usuarios.Add(new UsuariosClienteViewModel()
                {
                    Cpf = item.CPF,
                    Email = item.Email,
                    UsuarioId = item.Id
                });
            }
            return View("ListarUsuarios", model);
        }

        #region helpers

        public Cliente ToModel(ClienteViewModel dadosView)
        {
            var cliente = new Cliente
            {
                Id = dadosView.ClientId,
                RazaoSocial = dadosView?.RazaoSocial,
                NomeFantasia = dadosView?.NomeFantasia,
                CPNJ = dadosView?.CPNJ.Replace(".", "").Replace("-", "").Replace("/", ""),
                Endereco = new Endereco
                {
                    Numero = dadosView?.Endereco?.Numero,
                    Complemento = dadosView?.Endereco?.Complemento,
                    Cep = new Cep
                    {
                        CepNumero = dadosView?.Endereco?.CepNumero,
                        Bairro = dadosView?.Endereco?.Bairro,
                        Cidade = dadosView?.Endereco?.Cidade,
                        Estado = dadosView?.Endereco?.Estado,
                        Logradouro = dadosView?.Endereco?.Logradouro
                        //UF = (UfEnum)(dadosView?.Endereco?.UF ?? 0)
                    },

                }

            };
            return cliente;
        }

        public ClienteViewModel ToViewModel(Cliente cliente)
        {
            var dadosView = new ClienteViewModel
            {
                ClientId = cliente.Id,
                RazaoSocial = cliente?.RazaoSocial,
                NomeFantasia = cliente?.NomeFantasia,
                CPNJ = cliente?.CPNJ,
                Endereco = new EnderecoViewModel()
                {
                    Bairro = cliente?.Endereco?.Cep.Bairro,
                    CepNumero = cliente?.Endereco?.Cep.CepNumero,
                    Logradouro = cliente?.Endereco?.Cep.Logradouro,
                    Cidade = cliente?.Endereco.Cep?.Cidade,
                    Complemento = cliente?.Endereco?.Complemento,
                    Numero = cliente?.Endereco?.Numero,
                    Estado = cliente?.Endereco?.Cep.Estado
                    //Estado = ((UfEnum)cliente.Endereco.Cep.UF).ToString()
                }
            };
            return dadosView;
        }
        #endregion

    }
}
