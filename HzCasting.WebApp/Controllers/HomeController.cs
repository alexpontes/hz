﻿using AutoMapper;
using FastMapper;
using HzCasting.Domain.Entities;
using HzCasting.WebApp.Helpers;
using HzCasting.WebApp.Models.Modelos;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.WebApp.Controllers
{

    public sealed class HomeController : Base.BaseController
    { 

        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }  
    }
}