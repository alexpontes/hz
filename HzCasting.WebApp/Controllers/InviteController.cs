﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Diagnostics;
using System.Linq;
using HzCasting.WebApp.Controllers.Base;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Models.Modelos;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.WebApp.ModelsView;
using System.Collections.Generic;
using HzCasting.Infra.Common;
using AutoMapper;
using FastMapper;
using HzCasting.WebApp.Mappers;

namespace HzCasting.WebApp.Controllers
{

    public class InviteController : BaseController
    {
        #region fields
        private readonly IInviteApplication _inviteApplication;
        private readonly IAgendaCastingApplication _agendaCastingApplication;
        private readonly ICastingApplication _castingApplication;
        private readonly IBookingApplication _bookingApplication;
        #endregion
        public InviteController(IInviteApplication inviteApplication, IAgendaCastingApplication agendaCastingApplication, ICastingApplication castingApplication,
                                IBookingApplication bookingApplication)
        {
            _bookingApplication = bookingApplication;
            _castingApplication = castingApplication;
            _agendaCastingApplication = agendaCastingApplication;
            _inviteApplication = inviteApplication;
        }

        // GET: Invite/Cadastrar
        public ActionResult Pendentes()
        {
            IList<InviteViewModel> convitesVm = new List<InviteViewModel>();

            try
            {
                convitesVm = RetornarConvites(StatusTrackInviteEnum.Pendente);
            }
            catch (Exception ex)
            {                
                throw;
            }

            return View(convitesVm);
        }

        public ActionResult Aceitos()
        {
            IList<InviteViewModel> convitesVm = new List<InviteViewModel>();

            try
            {
                convitesVm = RetornarConvites(StatusTrackInviteEnum.Aceito);
            }
            catch (Exception)
            {
                //VERIFICAR
                //throw;
            }

            return View(convitesVm);
        }

        public ActionResult Rejeitados()
        {
            IList<InviteViewModel> convitesVm = new List<InviteViewModel>();

            try
            {
                convitesVm = RetornarConvites(StatusTrackInviteEnum.Rejeitado);
            }
            catch (Exception ex)
            {
                //VERIFICAR
                //throw;
            }

            return View(convitesVm);
        }

        public ActionResult Details(int id)
        {
            IList<Invite> convites = new List<Invite>();

            convites.Add(_inviteApplication.Obter(id));

            return View(ToInviteViewModel(convites));
        }

        // POST:
        [HttpPost]
        public ActionResult Aceitar(int inviteId, int agendaCastingId)
        {
            var agendaCasting = _agendaCastingApplication.Obter(agendaCastingId);

            var invite = _inviteApplication.Obter(inviteId);

            invite.StatusTrackInvite = StatusTrackInviteEnum.Aceito;

            invite.DataRetorno = DateTime.Now;

            invite.Booking.AgendaCasting = agendaCasting;

            _inviteApplication.Atualizar(invite);

            return RedirectToAction("Pendentes");
        }

        [HttpPost]
        public ActionResult Recusar(int inviteId)
        {
            var invite = _inviteApplication.Obter(inviteId);

            if (invite.StatusTrackInvite == StatusTrackInviteEnum.Aceito)
            {
                invite.StatusTrackInvite = StatusTrackInviteEnum.Cancelado;
            }
            else
            {
                invite.StatusTrackInvite = StatusTrackInviteEnum.Rejeitado;
            }

            invite.DataRetorno = DateTime.Now;

            _inviteApplication.Atualizar(invite);

            return RedirectToAction("Aceitos");
        }

        //verificar se nao está sendo usado e remover
        public ActionResult DispararInvites(Int32 castingId)
        {
            _inviteApplication.RegistrarInvites(castingId);
            return View();
        }

        [HttpPost]
        public JsonResult DispararInvites(IList<InviteRegistrarViewModel> dados)
        {
            try
            {
                IList<Invite> listaInvite = new List<Invite>();

                foreach (var item in dados)
                {
                    var booking = _bookingApplication.Obter(item.IdBooking);

                    var invite = new Invite()
                    {
                        Booking = booking,
                        StatusTrackInvite = StatusTrackInviteEnum.Pendente,
                        Validade = booking.PerfilFiltro.AgendasCasting.OrderByDescending(x => x.DataHoraInicio).FirstOrDefault().DataHoraInicio,
                        DataEnvio = DateTime.Now

                    };

                    listaInvite.Add(invite);                 
                }

                _inviteApplication.RegistrarInvites(listaInvite);


                return Json(new { success = "salvo com sucesso!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro ***", ex);
                return Json(new { error = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #region Helpers
        private static List<InviteViewModel> ToInviteViewModel(IList<Invite> invites)
        {
            var lisInvisteViewModel = new List<InviteViewModel>();

            foreach (var invite in invites)
            {

                var inviteVm = new InviteViewModel
                {
                    NomeModelo = string.Format("{0} {1}", invite.Booking.Modelo.DadosPessoais.PrimeiroNome, invite.Booking.Modelo.DadosPessoais.UltimoNome),
                    Id = invite.Id,
                    DataEnvio = invite.DataEnvio,
                    DataRetorno = invite.DataRetorno,
                    BookingId = invite.Booking.Id,
                    StatusTrackInvite = (int)invite.StatusTrackInvite,
                    NomeEvento = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Titulo,
                    Validade = invite.Validade,
                    DataEvento = invite.Booking?.PerfilFiltro?.Casting?.Evento?.DataInicio,
                    EnderecoBairro = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Endereco?.Cep?.Bairro,
                    EnderecoCep = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Endereco?.Cep?.CepNumero,
                    EnderecoCidade = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Endereco?.Cep?.Cidade,
                    EnderecoEstado = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Endereco?.Cep?.Estado,
                    EnderecoLogradouro = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Endereco?.Cep?.Logradouro,
                    EnderecoNumero = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Endereco?.Numero,
                    EnderecoZona = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Endereco?.Cep?.Zona,
                    Observacao = invite.Booking?.PerfilFiltro?.Casting?.Evento?.Obs,
                    Agendas = ToAgendaViewModel(invite.Booking?.PerfilFiltro?.AgendasCasting)
                };

                lisInvisteViewModel.Add(inviteVm);
            }
            return lisInvisteViewModel;
        }

        private static Invite ToInviteModel(InviteViewModel dadosView)
        {
            var model = new Invite
            {
                DataEnvio = dadosView.DataEnvio,
                DataRetorno = dadosView.DataRetorno,
                StatusTrackInvite = (StatusTrackInviteEnum)dadosView.StatusTrackInvite,
                Validade = dadosView.Validade,
                Booking = new Booking
                {

                },
                Id = dadosView.Id ?? 0
            };
            return model;
        }

        private static IList<AgendaViewModel> ToAgendaViewModel(IList<AgendaCasting> dadosView)
        {
            var modelVws = new List<AgendaViewModel>();

            if (dadosView != null)
            {                
                foreach (var item in dadosView.ToList())
                {

                    var modelVw = new AgendaViewModel
                    {
                        DataInicio = item.DataHoraInicio.Date,
                        HoraInicio = item.DataHoraInicio.Date,
                        HoraFim = item.DataHoraFim.Date,
                        AgendaCastingId = item.Id

                    };

                    modelVws.Add(modelVw);
                }
            }
          

            return modelVws;
        }

        private IList<InviteViewModel> RetornarConvites(StatusTrackInviteEnum status)
        {
            var userId = User.Identity.GetUserId();

            var roles = ObterRoles();            

            var convites = _inviteApplication.Obter(m => (m.Booking.Modelo.IdUsuario == userId || roles.Contains("admin")) && m.StatusTrackInvite == status);

            return ToInviteViewModel(convites.ToList());
        }
        #endregion

    }
}
