﻿using HzCasting.Application;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Common;
using HzCasting.WebApp.Controllers.Base;
using HzCasting.WebApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.WebApp.Controllers
{
    public class UsuarioController : BaseController
    {
        private readonly IUsuarioApplication _usuarioApplication;
        private readonly IBackOfficeApplication _backOfficeApplication;
        private readonly ILoginApplication _loginApplication;
        private readonly IClienteApplication _clienteApplication;
        private readonly IUsuarioClienteApplication _usuarioClienteApplication;


        public UsuarioController(IUsuarioApplication usuarioApplication, IBackOfficeApplication backOfficeApplication,
            IClienteApplication clienteApplication, IUsuarioClienteApplication usuarioClienteApplication)
        {
            _usuarioApplication = usuarioApplication;
            _backOfficeApplication = backOfficeApplication;
            _clienteApplication = clienteApplication;
            _usuarioClienteApplication = usuarioClienteApplication;

        }

        public UsuarioController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;


        }

        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CadastrarUsuarioOperacional()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CadastrarUsuarioOperacional(UsuarioOperacionalViewModel model)
        {
            var login = new LoginApplication(UserManager, SignInManager);

            //verifica se  usuario existe (pode ser que o usuario tenha se logado com o facebook e nao tinha uma conta atrelada)
            var usuario = new Usuario { CPF = model.Cpf };
            var usuarioExistente = _usuarioApplication.VerificarUsuarioExistente(usuario);

            //caso seja um usuario existente, associa a conta existe
            if (usuarioExistente != null && usuarioExistente?.PasswordHash == null)
            {
                usuario = usuarioExistente;
                usuario.Email = model.Email;
                usuario.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                AssociarConta(usuario);

                await login.LogarUserAsync(usuarioExistente);


                return RedirectToAction("Editar", "Modelo", new { usuario.Id });
            }

            if (usuarioExistente?.PasswordHash != null)
                AddErrors(new IdentityResult("Usuario ja cadastrado!"));

            else
            {
                var user = new Usuario
                {
                    UserName = model.Email,
                    Email = model.Email,
                    CPF = model.Cpf,
                    Senha = model.Password,
                    Perfild = (Int32)PerfilAcessoEnum.Admin //Admin
                };

                var result = await login.CadastrarUsuarioAsync(user);

                if (result.Succeeded)
                {
                    var usuarioBackOffice = _usuarioApplication.VerificarUsuarioExistente(user);

                    var backOffice = new BackOffice()
                    {
                        CpfNumero = model.Cpf,
                        CpfIsValido = true,
                        DadosPessoais_Celular = model.Celular,
                        DadosPessoais_Email = model.Email,
                        DadosPessoais_Telefone = model.Telefone,
                        IdUsuario = usuarioBackOffice.Id,
                        DataNascimento = DateTime.Now,
                        PrimeiroNome = model.PrimeiroNome,
                        UltimoNome = model.UltimoNome
                    };

                    _backOfficeApplication.Registrar(backOffice);

                    if (result.Succeeded) return RedirectToAction("Index", "Home");

                    AddErrors(result);
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult EditarUsuarioOperacional(string usuarioId)
        {
            var usuario = _usuarioApplication.Obter(x => x.Id == usuarioId).FirstOrDefault();

            var backOffice = _backOfficeApplication.Obter(x => x.IdUsuario == usuarioId).FirstOrDefault();

            if (usuario != null)
            {
                UsuarioOperacionalViewModel model = new UsuarioOperacionalViewModel()
                {
                    UsuarioId = usuario.Id,
                    Cpf = usuario.CPF,
                    Email = usuario.Email
                };

                if (backOffice != null)
                {
                    model.Celular = backOffice.DadosPessoais_Celular;
                    model.DataNascimento = backOffice.DataNascimento;
                    model.PrimeiroNome = backOffice.PrimeiroNome;
                    model.UltimoNome = backOffice.UltimoNome;
                    model.Telefone = backOffice.DadosPessoais_Telefone;
                }

                return View(model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult EditarUsuarioOperacional(UsuarioOperacionalViewModel model)
        {
            var usuario = new Usuario { CPF = model.Cpf };
            var usuarioExistente = _usuarioApplication.VerificarUsuarioExistente(usuario);

            var backOffice = _backOfficeApplication.Obter(x => x.IdUsuario == model.UsuarioId).FirstOrDefault();

            if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.ConfirmPassword))
            {
                if (model.Password.Equals(model.ConfirmPassword))
                {
                    usuarioExistente.Senha = model.Password;
                }
            }

            backOffice.PrimeiroNome = model.PrimeiroNome;
            backOffice.UltimoNome = model.UltimoNome;
            backOffice.DadosPessoais_Celular = model.Celular;
            backOffice.DadosPessoais_Telefone = model.Telefone;

            _backOfficeApplication.Atualizar(backOffice);
            _usuarioApplication.Atualizar(usuarioExistente);

            return View(model);
        }

        [HttpGet]
        public ActionResult CadastrarUsuarioCliente(int clienteId)
        {
            UsuarioClienteViewModel cliente = new UsuarioClienteViewModel();
            cliente.ClienteId = clienteId;
            return View(cliente);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CadastrarUsuarioCliente(UsuarioClienteViewModel model)
        {
            var login = new LoginApplication(UserManager, SignInManager);

            //verifica se  usuario existe (pode ser que o usuario tenha se logado com o facebook e nao tinha uma conta atrelada)
            var usuario = new Usuario { CPF = model.Cpf };
            var usuarioExistente = _usuarioApplication.VerificarUsuarioExistente(usuario);

            //caso seja um usuario existente, associa a conta existe
            if (usuarioExistente != null && usuarioExistente?.PasswordHash == null)
            {
                usuario = usuarioExistente;
                usuario.Email = model.Email;
                usuario.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                AssociarConta(usuario);

                await login.LogarUserAsync(usuarioExistente);

                return RedirectToAction("Editar", "Modelo", new { usuario.Id });
            }

            if (usuarioExistente?.PasswordHash != null)
                AddErrors(new IdentityResult("Usuario ja cadastrado!"));

            else
            {
                var user = new Usuario
                {
                    UserName = model.Email,
                    Email = model.Email,
                    CPF = model.Cpf,
                    Senha = model.Password,
                    Perfild = (Int32)PerfilAcessoEnum.Cliente
                };

                if (model.ClienteId > 0)
                {
                    user.ClienteId = model.ClienteId;
                }

                var result = await login.CadastrarUsuarioAsync(user);

                if (result.Succeeded)
                {
                    //Tabela UsuarioCliente
                    var usuarioCliente = new UsuarioCliente()
                    {
                        CpfNumero = model.Cpf,
                        DadosPessoais_Celular = model.Celular,
                        DadosPessoais_Email = model.Email,
                        DadosPessoais_Telefone = model.Telefone,
                        IdUsuario = user.Id,
                        DataNascimento = DateTime.Now,
                        ClienteId = model.ClienteId,
                        PrimeiroNome = model.PrimeiroNome,
                        UltimoNome = model.UltimoNome
                    };

                    _usuarioClienteApplication.Registrar(usuarioCliente);

                    if (result.Succeeded) return RedirectToAction("ListarUsuarios", "Cliente",new { clienteId = model.ClienteId });
                }

                AddErrors(result);
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult EditarUsuarioCliente(string usuarioId)
        {
            var usuario = _usuarioApplication.Obter(x => x.Id == usuarioId).FirstOrDefault();
            var usuarioCliente = _usuarioClienteApplication.Obter(x => x.IdUsuario == usuarioId).FirstOrDefault();

            if (usuario != null)
            {
                var model = new UsuarioClienteViewModel()
                {
                    UsuarioId = usuarioId,
                    Cpf = usuario.CPF,
                    Email = usuario.Email,
                    ClienteId = (int)usuario.ClienteId.GetValueOrDefault()
                };

                if (usuarioCliente != null)
                {
                    model.Celular = usuarioCliente.DadosPessoais_Celular;
                    model.DataNascimento = usuarioCliente.DataNascimento;
                    model.PrimeiroNome = usuarioCliente.PrimeiroNome;
                    model.UltimoNome = usuarioCliente.UltimoNome;
                    model.Telefone = usuarioCliente.DadosPessoais_Telefone;
                    model.ClienteId = usuarioCliente.ClienteId;
                }

                return View(model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult EditarUsuarioCliente(UsuarioClienteViewModel model)
        {
            var usuario = new Usuario { CPF = model.Cpf };
            var usuarioExistente = _usuarioApplication.VerificarUsuarioExistente(usuario);

            var usuarioCliente = _usuarioClienteApplication.Obter(x => x.IdUsuario == usuarioExistente.Id).FirstOrDefault();

            if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.ConfirmPassword))
            {
                if (model.Password.Equals(model.ConfirmPassword))
                {
                    usuarioExistente.Senha = model.Password;
                }
            }

            usuarioCliente.PrimeiroNome = model.PrimeiroNome;
            usuarioCliente.UltimoNome = model.UltimoNome;
            usuarioCliente.DadosPessoais_Celular = model.Celular;
            usuarioCliente.DadosPessoais_Telefone = model.Telefone;
            usuarioCliente.ClienteId = model.ClienteId;
            usuarioCliente.DadosPessoais_Email = model.Email;

            usuarioExistente.Email = model.Email;
            usuarioExistente.UserName = model.Email;

            _usuarioClienteApplication.Atualizar(usuarioCliente);
            _usuarioApplication.Atualizar(usuarioExistente);

            return RedirectToAction("ListarUsuarios", "Cliente", new { clienteId = usuarioCliente.ClienteId });
        }

        [HttpPost]
        public JsonResult Excluir(string usuarioId)
        {
            _usuarioApplication.Deletar(usuarioId);
            _backOfficeApplication.Deletar(usuarioId);

            return Json(true);
        }

        private void AssociarConta(Usuario user)
        {
            _usuarioApplication.Atualizar(user);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}