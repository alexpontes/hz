﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Common;
using HzCasting.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.WebApp.Controllers
{
    public class PerfilController : Base.BaseController
    {
        private readonly IPerfilFiltroApplication _perfilCastingApplication;
        private readonly ICastingApplication _castingApplication;
        private readonly IAgenciaApplication _agenciaApplication;
        private readonly IAgendaCastingApplication _agendaCastingApplication;
        private readonly IModeloApplication _modeloApplcation;
        private readonly IUsuarioClienteApplication _usuarioClienteApplication;

        public PerfilController(IPerfilFiltroApplication perfilCastingApplication,
                                ICastingApplication castingApplication,
                                IAgenciaApplication agenciaApplication,
                                IAgendaCastingApplication agendaCastingApplication,
                                IModeloApplication modeloApplcation,
                                IUsuarioClienteApplication usuarioClienteApplication)
        {
            _perfilCastingApplication = perfilCastingApplication;
            _castingApplication = castingApplication;
            _agenciaApplication = agenciaApplication;
            _agendaCastingApplication = agendaCastingApplication;
            _modeloApplcation = modeloApplcation;
            _usuarioClienteApplication = usuarioClienteApplication;
        }

        public ActionResult Cadastrar(Int32 idCasting)
        {
            var vm = new PerfilCastingViewModel();
            vm.CastingId = idCasting;

            CarregarSelects();
            return View(vm);
        }


        [HttpPost]
        public ActionResult Cadastrar(PerfilCastingViewModel dadosTela)
        {
            try
            {
                _perfilCastingApplication.Registrar(ToModel(dadosTela));

                return RedirectToAction("Listar", new { idCasting = dadosTela.CastingId });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Erro **" + ex);
                CarregarSelects();
                SetValidationErrors(ModelState);
                return View(dadosTela);
            }
        }

        public ActionResult Editar(int id)
        {
            try
            {

                var modeloApp = _perfilCastingApplication.Obter(id);

                var vm = ToViewModel(modeloApp);
                CarregarSelects(modeloApp);

                return View(vm);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                return View();
            }
        }

        [HttpPost]
        public ActionResult Editar(PerfilCastingViewModel dadosTela)
        {

            try
            {
                _perfilCastingApplication.Atualizar(ToModel(dadosTela));

                CarregarSelects();

                return RedirectToAction("Listar", new { idCasting = dadosTela.CastingId });
            }
            catch (Exception ex)
            {
                CarregarSelects();

                Debug.WriteLine($"Erro: {ex}");
                return View();
            }
        }

        public ActionResult AssociarAgendaAoPerfil(Int32 perfilId, Int32 castingId, Int32 agendaId)
        {
            var agenda = _agendaCastingApplication.Obter(agendaId);
            var perfil = _perfilCastingApplication.Obter(perfilId);
            (perfil.AgendasCasting ?? new List<AgendaCasting>()).Add(agenda);
            var casting = _castingApplication.Obter(castingId);
            (casting.PerfisFiltros ?? new List<PerfilFiltro>()).Add(perfil);
            _castingApplication.Atualizar(casting);

            return RedirectToAction("Listar", new { idCasting = castingId });
        }

        public ActionResult Listar(Int32 idCasting)
        {
            try
            {
                var casting = _castingApplication.Obter(idCasting);
                var perfis = casting.PerfisFiltros?.Select(ToViewModel);

                ViewBag.CastingTitulo = casting?.Titulo;
                ViewBag.CastingId = idCasting;
                var helper = new ClienteHelper(_usuarioClienteApplication);

                var userId = User.Identity.GetUserId();
                var isCliente = helper.IsUserLoggedCliente(userId);

                ViewBag.IsCliente = isCliente;
                //var selects = formatarAgendas(casting);

                //ViewBag.Agendas = selects;

                return View(perfis);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Erro: {ex}");
                var perfil = new List<PerfilCastingViewModel>();

                return View(perfil);
            }
        }

        public void CarregarSelects(PerfilFiltro perfilcasting = null)
        {
            var corPele = default(CorPeleEnum).ToSelectListItem(perfilcasting?.CorPele);
            var corCabelo = default(CorCabeloEnum).ToSelectListItem(perfilcasting?.CorCabelo);
            var corOlhos = default(CorOlhosEnum).ToSelectListItem(perfilcasting?.CorOlhos);
            var tipoSelecao = default(TipoSelecaoEnum).ToSelectListItem(perfilcasting?.TipoSelecao);


            ViewBag.CorPeleList = corPele;
            ViewBag.CorCabeloList = corCabelo;
            ViewBag.CorOlhosList = corOlhos;
            ViewBag.TipoSelecaoList = tipoSelecao;
        }

        public ActionResult Modelos(Int64 castingId)
        {
            var modelos = _castingApplication.ListarModelosCasting(castingId);
            var vm = modelos.Select(m => new PerfilCastingViewModel
            {
                NomeModelo = m.DadosPessoais.PrimeiroNome,
                IdModelo = m.Id
            });

            return View(vm);
        }



        #region Helpers
        private static IEnumerable<SelectListItem> formatarAgendas(Casting casting)
        {
            var agendas = casting?.Agendas;
            var agendasComPerfis = agendas?
                        .Select(m => new SelectListItem()
                        {
                            Text = $"{m.DataHoraInicio} - {m.DataHoraFim}",
                            Value = m.Id.ToString(),
                        });


            var selects = agendasComPerfis.ToList();
            selects?.Insert(0, new SelectListItem
            {
                Text = "Selecione...",
                Value = "0",
                Selected = true
            });
            return selects;
        }

        private static PerfilCastingViewModel ToViewModel(PerfilFiltro p)
        {
            return new PerfilCastingViewModel
            {
                PerfilId = Convert.ToInt32(p.Id),
                TipoSelecao = (Int32)p.TipoSelecao,
                Titulo = p.Titulo,

                CorPele = Convert.ToInt32(p.CorPele),
                CorCabelo = Convert.ToInt32(p.CorCabelo),
                CorOlhos = Convert.ToInt32(p.CorOlhos),

                AlturaMin = p.AlturaMin,
                AlturaMax = p.AlturaMax,

                IdadeMin = p.IdadeMin,
                IdadeMax = p.IdadeMax,

                PesoMin = p.PesoMin,
                PesoMax = p.PesoMax,

                ManequinMin = p.ManequimMin,
                ManequinMax = p.ManequimMax,

                CinturaMin = p.CinturaMin,
                CinturaMax = p.CinturaMax,

                QuadrilMin = p.QuadrilMin,
                QuadrilMax = p.QuadrilMax,

                BustoMin = p.BustoMin,
                BustoMax = p.BustoMax,

                CalcadoMin = p.CalcadoMin,
                CalcadoMax = p.CalcadoMax,
				QuantidadeCandidato = p.QuantidadeCandidato,
				Tatuagem = p.Tatuagem,
				CastingId = p.Casting.Id
            };
        }

        private static PerfilFiltro ToModel(PerfilCastingViewModel dadosTela)
        {

	  var perfil = new PerfilFiltro {
			  Casting = new Casting { Id = (Int64)dadosTela.CastingId },
				Id = dadosTela.Id.HasValue ? dadosTela.Id.Value : 0,
                Titulo = dadosTela.Titulo,
                QuantidadeCandidato = dadosTela.QuantidadeCandidato,
                TipoSelecao = (TipoSelecaoEnum)dadosTela.TipoSelecao,
                CorPele = (CorPeleEnum)dadosTela.CorPele,
                CorCabelo = (CorCabeloEnum)dadosTela.CorCabelo,
                CorOlhos = (CorOlhosEnum)dadosTela.CorOlhos,
                IdadeMin = dadosTela.IdadeMin,
                IdadeMax = dadosTela.IdadeMax,
                PesoMin = dadosTela.PesoMin,
                PesoMax = dadosTela.PesoMax,
                AlturaMin = dadosTela.AlturaMin,
                AlturaMax = dadosTela.AlturaMax,
                ManequimMin = dadosTela.ManequinMin,
                ManequimMax = dadosTela.ManequinMax,
                CinturaMin = dadosTela.CinturaMin,
                CinturaMax = dadosTela.CinturaMax,
                QuadrilMin = dadosTela.QuadrilMin,
                QuadrilMax = dadosTela.QuadrilMax,
                BustoMin = dadosTela.BustoMin,
                BustoMax = dadosTela.BustoMax,
                CalcadoMin = dadosTela.CalcadoMin,
                CalcadoMax = dadosTela.CalcadoMax,
                Tatuagem = dadosTela.Tatuagem
            };
            if (dadosTela.AgendaId == null) return perfil;
            perfil.AgendasCasting = new List<AgendaCasting> {
                    new AgendaCasting {
                        Id =  (Int64)dadosTela.AgendaId
                    }
                };

            return perfil;
        }

        private IEnumerable<AgendaCasting> CarregarAgendasCasting(string idCasting)
        {
            var agendas = _perfilCastingApplication.Obter(m => m.Id == int.Parse(idCasting)).SelectMany(m => m.AgendasCasting);
            return agendas;
        }


        #endregion
    }
}