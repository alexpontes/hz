﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HzCasting.WebApp.Startup))]
namespace HzCasting.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
