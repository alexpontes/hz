﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed  class ModelosSelecionadasViewModel
    {
        public String ImagemSrc { get; set; }
        public Int32 ModeloId { get; set; }
        public String NomeModelo { get; set; }
        public String UsuarioId { get; set; }
        public String PerfilModelo { get; set; }
          
        



    }
}