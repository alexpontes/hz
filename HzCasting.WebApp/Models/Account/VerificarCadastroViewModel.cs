﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Account
{
    public sealed class VerificarCadastroViewModel
    {
        public string FacebookToken { get; set; }
        public string FacebookEmail { get; set; }
        [Display(Name = "CPF")]
        public string Cpf { get; set; }
    }
}
