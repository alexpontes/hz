﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed class ModeloEditarPereciveisViewModel
    {
        #region Caracteristicas
        public Int32 CorPelePerfiModelo { get; set; }
        public Int32 CorOlhosPerfiModelo { get; set; }

        public Int32 CorCabeloPerfilModelo { get; set; }
       // public Int64?  IdCorCabeloPerfilModelo { get; set; }
        public Decimal PesoPerfiModelo { get; set; }
        public Decimal AlturaPerfiModelo { get; set; }
        public Decimal ManequimPerfiModelo { get; set; }
        public Decimal CinturaPerfiModelo { get; set; }
        public Decimal QuadrilPerfiModelo { get; set; }
        public Decimal BustoPerfiModelo { get; set; }
        public Int32 CalcadoPerfiModelo { get; set; }

        public String OutrosPerfiModelo { get; set; }
        public Int32 ModeloId { get; internal set; }
        #endregion
    }
}