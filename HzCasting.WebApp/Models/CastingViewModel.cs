﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed class CastingViewModel
    {
        public Int64? Id { get; set; }
        public Int64 IdEvento { get; set; }
        [Display(Name = "TituloEvento", ResourceType = typeof(Resources.Resources))]
        public String TituloEvento { get; set; }

        public Int64 IdCliente { get; set; }
        [Display(Name = "NomeCliente", ResourceType = typeof(Resources.Resources))]
        public String NomeCliente { get; set; }

        public Int64? IdAgencia { get; set; }
        [Display(Name = "NomeAgencia", ResourceType = typeof(Resources.Resources))]
        public String NomeAgencia { get; set; }

        public HttpPostedFileBase Imagem { get; set; }
        public String ImagemSrc { get; set; }
        //public DateTime? DataInicioCasting { get; set; }

        public Int32? Status { get; set; }
        public Boolean Cliente { get; set; }
        #region Agenda
        public IList<AgendaViewModel> Agendas { get; set; }
        #endregion

    }
}