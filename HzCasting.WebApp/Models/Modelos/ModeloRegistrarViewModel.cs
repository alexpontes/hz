﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HzCasting.WebApp.Helpers;

namespace HzCasting.WebApp.Models.Modelos
{
    public class ModeloRegistrarViewModel
    {
        public ModeloRegistrarViewModel()
        {
            Sexo = new List<SelectListItem>();
            EstadoCivil = new List<SelectListItem>();
        }

        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

        [Display(Name = "Email", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public string Email { get; set; }

        [Display(Name = "CPF")]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public string Cpf { get; set; }

        [Display(Name = "Senha", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
		[Senha(ErrorMessage = "Senha Inválida. (Minimo 6 caracteres, 1 letra maiúscula, 1 caractere especial e 1 valor numérico).", SenhaTamanhoMinimo = 6, SenhaTamanhoMaximo = 30, SenhaForteRequerida = true, CaracterEspecialRequerido = true)]
		public string Senha { get; set; }

        [Display(Name = "ConfirmarSenha", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [System.ComponentModel.DataAnnotations.Compare("Senha", ErrorMessage= "A nova senha e confirmação de senha não coincidem!")]
        public string ConfirmarSenha { get; set; }

        [Display(Name = "Nome", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public string Nome { get; set; }

        [Display(Name = "Sobrenome", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public string Sobrenome { get; set; }

        [Display(Name = "DataNascimento", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public DateTime? DataNascimento { get; set; }

        public string FacebookEmail { get; set; }

        public string FacebookToken { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public EstadoCivilEnum EstadoCivilId { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public SexoEnum SexoId { get; set; }

        [Display(Name = "Sexo", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public IList<SelectListItem> Sexo { get; set; }

        [Display(Name = "EstadoCivil", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public IList<SelectListItem> EstadoCivil { get; set; }
    }
}
