﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloDadosCadastraisViewModel
    {
        public string UsuarioId { get; set; }

        public int ModeloId { get; set; }

        [Required]
        public string PrimeiroNomeDadosPessoais { get; set; }

        [Required]
        public string UltimoNomeDadosPessoais { get; set; }

        [Required]
        public DateTime DataNascimentoDadosPessoais { get; set; }

        public int EstadoCivilId { get; set; }

        public int SexoId { get; set; }
    }
}
