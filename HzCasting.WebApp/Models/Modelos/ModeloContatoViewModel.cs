﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloContatoViewModel
    {
        public string UsuarioId { get; set; }
        public int ModeloId { get; set; }
        public string CelularDadosPessoais { get; set; }
        public string TelefoneDadosPessoais { get; set; }
    }
}
