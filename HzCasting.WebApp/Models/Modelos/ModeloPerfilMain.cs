﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using HzCasting.WebApp.Controllers.Base;
using HzCasting.WebApp.Models;
using HzCasting.WebApp.Models.Modelos;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.WebApp.ModelsView;
using System.Collections.Generic;
using HzCasting.Infra.Common;
using Microsoft.AspNet.Identity.Owin;
using System.ComponentModel.DataAnnotations;

namespace HzCasting.WebApp.Models.Modelos
{
    public class ModeloPerfilMainViewModel
    {
        public ModeloPerfilMainViewModel()
        {
            DadosPessoais = new ModeloDadosPessoaisModel();
            Endereco = new ModeloEnderecoModel();
            Documentos = new ModeloDocumentosModel();
            Contato = new ModeloContatoModel();
            Documentos = new ModeloDocumentosModel();
            DadosBancarios = new ModeloDadosBancariosModel();
           // Escolaridade = new ModeloEscolaridadeModel();
            Idiomas = new ModeloIdiomaModel();
            EscolaridadeDrop = default(EscolaridadeEnum).ToSelectListItem();
            Caracteristicas = new ModeloCaracteristicasModel();
        }

        public int ModeloId { get; set; }

        public ModeloDadosPessoaisModel DadosPessoais { get; set; }

        public ModeloEnderecoModel Endereco { get; set; }

        public ModeloContatoModel Contato { get; set; }

        public ModeloDocumentosModel Documentos { get; set; }

        public ModeloDadosBancariosModel DadosBancarios { get; set; }

        public EscolaridadeEnum EscolaridadeId { get; set; }

        public ModeloIdiomaModel Idiomas { get; set; }

        public ModeloCaracteristicasModel Caracteristicas { get; set; }

        public bool DisponibilidadeHorario { get; set; }

        public bool DisponibilidadeViagem { get; set; }

        public bool PossuiVeiculo { get; set; }

        public IList<SelectListItem> EscolaridadeDrop { get; set; }
    }

    public sealed class ModeloDadosPessoaisModel
    {
        public ModeloDadosPessoaisModel()
        {
            Sexo = default(SexoEnum).ToSelectListItem();
            EstadoCivil = default(EstadoCivilEnum).ToSelectListItem();
         
        } 
        public int ModeloId { get; set; }

        [Display(Name = "PrimeiroNomeDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String PrimeiroNome{ get; set; }

        [Display(Name = "UltimoNomeDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String UltimoNome { get; set; }


        [DataType(DataType.DateTime)]
        [Display(Name = "DataNascimentoDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public DateTime? DataNascimento { get; set; }

        [Display(Name = "EstadoCivilIdDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public EstadoCivilEnum EstadoCivilId { get; set; }

        [Display(Name = "SexoIdDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public SexoEnum SexoId { get; set; }

        [Display(Name = "TelefoneDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String Telefone { get; set; }

        [Display(Name = "CelularDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String Celular { get; set; }

        [Display(Name = "DisponibilidadeHorario", ResourceType = typeof(Resources.Resources))]
        public bool DisponibilidadeHorario { get; set; }

        [Display(Name = "DisponibilidadeViagem", ResourceType = typeof(Resources.Resources))]
        public bool DisponibilidadeViagem { get; set; }

        [Display(Name = "PossuiVeiculo", ResourceType = typeof(Resources.Resources))]
        public bool PossuiVeiculo { get; set; }

        public IList<SelectListItem> EstadoCivil { get; set; }

        public IList<SelectListItem> Sexo { get; set; }


        //   CorOlhos = default(EstadoEnum).ToSelectListItem();
        // CorPele = default(CorPeleEnum).ToSelectListItem();
    }

    public sealed class ModeloEnderecoModel
    {
        public string UsuarioId { get; set; }

        public int ModeloId { get; set; }

        public string Cidade { get; set; }

        public string Estado { get; set; }

        public string Logradouro { get; set; }

        public string Bairro { get; set; }

        [Display(Name = "CEP")]
        public string Cep { get; set; }

        public int Uf { get; set; }

        public string Complemento { get; set; }

        [Display(Name ="NÚMERO")]
        public string Numero { get; set; }
    }

    public sealed class ModeloContatoModel
    {
        public string Telefone { get; set; }
        public string Celular { get; set; }
    }

    public sealed class ModeloDocumentosModel
    {
        [Display(Name = "RG")]
        public string Rg { get; set; }

        [Display(Name = "CPF")]
        public string Cpf { get; set; }

        [Display(Name = "PIS")]
        public string Pis { get; set; }

        [Display(Name = "CTPS")]
        public string Ctps { get; set; }

        [Display(Name = "PASSAPORTE")]
        public string Passaporte { get; set; }
    }

    public sealed class ModeloDadosBancariosModel
    {
        [Display(Name = "BANCO")]
        public string Banco { get; set; }

        [Display(Name = "AGÊNCIA")]
        public string Agencia { get; set; }

        [Display(Name = "CONTA CORRENTE")]
        public string ContaCorrente { get; set; }
    }

    public sealed class ModeloEscolaridadeModel
    {
        [Display(Name = "ESCOLARIDADE")]
        public string Escolaridade { get; set; }

        [Display(Name = "CURSO TÉCNICO")]
        public string CursoTecnico { get; set; }

        [Display(Name = "CURSO SUPERIOR")]
        public string CursoSuperior { get; set; }
    }

    public sealed class ModeloIdiomaModel
    {
        public ModeloIdiomaModel()
        {
            Idiomas = new List<Tuple<int, string, bool>>();
        }

        //id, descricao, checked
        public List<Tuple<int, string, bool>> Idiomas { get; set; }

        public void AddIdioma(int id, string descricao, bool checado)
        {
            Idiomas.Add(new Tuple<int, string, bool>(id, descricao, checado));
        }
    }

    public sealed class ModeloCaracteristicasModel
    {
        public ModeloCaracteristicasModel()
        {
            CorOlhos = default(CorOlhosEnum).ToSelectListItem();
            CorPele = default(CorPeleEnum).ToSelectListItem();
            CorCabelo = default(CorCabeloEnum).ToSelectListItem();
            TipoModelo = default(TipoModeloEnum).ToSelectListItem();
            Tatuagem = new TatuagemModel();
            CategoriaModelo = default(CategoriaModeloEnum).ToSelectListItem();
        }

        public IList<SelectListItem> CorOlhos { get; set; }

        public CorOlhosEnum CorOlhosId { get; set; }

        public IList<SelectListItem> CorPele { get; set; }

        public CorPeleEnum CorPeleId { get; set; }

        public IList<SelectListItem> CorCabelo { get; set; }

        public CorCabeloEnum CorCabeloId { get; set; }

        public TipoModeloEnum TipoModeloId { get; set; }

        public decimal Peso { get; set; }

        public decimal Altura { get; set; }

        public decimal Manequim { get; set; }

        public decimal Cintura { get; set; }

        public decimal Busto { get; set; }

        public decimal Quadril { get; set; }

        public decimal Calcado { get; set; }

        public string Outros { get; set; }

        public CategoriaModeloEnum CategoriaModeloId { get; set; }

        public IList<SelectListItem> TipoModelo { get; set; }

        public TatuagemModel Tatuagem { get; set; }

        public IList<SelectListItem> CategoriaModelo { get; set; }

    }

    public class TatuagemModel
    {
        public string Descricao { get; set; }

        public string LocalDoCorpo { get; set; }
    }
}
