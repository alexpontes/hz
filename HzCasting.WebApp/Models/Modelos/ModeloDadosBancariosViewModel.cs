﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloDadosBancariosViewModel
    {
        public int ModeloId { get; set; }
        public string AgenciaBanco { get; set; }
        public string ContaBanco { get; set; }
        public string NomeBanco { get; set; }
        public int TipoContaBanco { get; set; }
    }
}
