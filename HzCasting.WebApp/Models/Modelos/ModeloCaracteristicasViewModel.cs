﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloCaracteristicasViewModel
    {
        public int ModeloId { get; set; }
        public int AlturaPerfiModelo { get; set; }
        public int BustoPerfiModelo { get; set; }
        public int CalcadoPerfiModelo { get; set; }
        public int CinturaPerfiModelo { get; set; }
        public int CorCabeloPerfilModelo { get; set; }
        public int CorOlhosPerfiModelo { get; set; }
        public int CorPelePerfiModelo { get; set; }
        public decimal ManequimPerfiModelo { get; set; }
        public string OutrosPerfiModelo { get; set; }
        public int PesoPerfiModelo { get; set; }
        public int QuadrilPerfiModelo { get; set; }
        public int CategoriaModelo { get; set; }
        public int TipoModelo { get; set; }
        public DateTime DataAprovacao { get; set; }
    }
}
