﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos {
  public class ModeloUsuarioViewModel {

	private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

	public string UsuarioId { get; set; }
	[Required]
	public string Email { get; set; }

	[Required]
	public string Cpf { get; set; }

	[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
	[StringLength(100, ErrorMessage = "O {0} deve ser de pelo menos {2} caracteres.", MinimumLength = 6)]
	[DataType(DataType.Password)]
	[Display(Name = "Password", ResourceType = typeof(Resources.Resources))]
	public string Password { get; set; }

	[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
	[DataType(DataType.Password)]
	[Display(Name = "ConfirmPassword", ResourceType = typeof(Resources.Resources))]
	[Compare("Password", ErrorMessage = "A Senha e a confirmação não coincidem")]
	public string ConfirmPassword { get; set; }
  }
}
