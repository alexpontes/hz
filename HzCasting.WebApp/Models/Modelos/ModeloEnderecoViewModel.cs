﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloEnderecoViewModel
    {
        public string UsuarioId { get; set; }
        public int ModeloId { get; set; }
        public string CidadeEndereco { get; set; }
        public string EstadoEndereco { get; set; }
        public string LogradouroEndereco { get; set; }
        public string BairroEndereco { get; set; }
        public string CepEndereco { get; set; }
        public string GDEEndereco { get; set; }
        public int UfEndereco { get; set; }
        public string ZonaEndereco { get; set; }
        public string ComplementoEndereco { get; set; }
        public string NumeroEndereco { get; set; }
    }
}
