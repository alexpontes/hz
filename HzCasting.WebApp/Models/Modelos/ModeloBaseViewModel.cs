﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace HzCasting.WebApp.Models.Modelos
{
    public class ModeloBaseViewModel : UsuarioBase
    {
        private const String MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";
        public ModeloBaseViewModel()
        {
            FotosExistentes = new List<FacebookViewModel>();
            FotosFacebook = new List<FacebookViewModel>();
        }

        public Int64? ModeloId { get; set; }
        public String UsuarioId { get; set; }

        #region Dados Pessoais
        [Display(Name = "PrimeiroNomeDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String PrimeiroNomeDadosPessoais { get; set; }

        [Display(Name = "UltimoNomeDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String UltimoNomeDadosPessoais { get; set; }

        
        [DataType(DataType.DateTime)]
        [Display(Name = "DataNascimentoDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public DateTime? DataNascimentoDadosPessoais { get; set; }

        [Display(Name = "EstadoCivilIdDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public Int32 EstadoCivilIdDadosPessoais { get; set; }

        [Display(Name = "SexoIdDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public Int32 SexoIdDadosPessoais { get; set; }

        [Display(Name = "TelefoneDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String TelefoneDadosPessoais { get; set; }

        [Display(Name = "CelularDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String CelularDadosPessoais { get; set; }

        public EscolaridadeEnum Escolaridade { get; set; }
        #endregion
        
        #region Fotos

        public IList<FacebookViewModel> FotosExistentes { get; set; } 
        public IList<FacebookViewModel> FotosFacebook { get; set; }

        #endregion

        #region Documentos
        [Display(Name = "RgDocumento", ResourceType = typeof(Resources.Resources))]
        // [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public String RgDocumento { get; set; }

        [Display(Name = "PisDocumento", ResourceType = typeof(Resources.Resources))]
        // [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public String PisDocumento { get; set; }

        //public Cpf Cpf { get; set; }

        [Display(Name = "CateiraProfissionalDocumento", ResourceType = typeof(Resources.Resources))]
        //[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public String CateiraProfissionalDocumento { get; set; }

        [Display(Name = "Passaporte", ResourceType = typeof(Resources.Resources))]
        public String Passaporte { get; set; }
        #endregion

        #region DocumenosBancarios
        
        public Int64? IdBanco { get; set; }

        [Display(Name = "NomeBanco", ResourceType = typeof(Resources.Resources))]
        public String NomeBanco { get; set; }

        [Display(Name = "AgenciaBanco", ResourceType = typeof(Resources.Resources))]
        public String AgenciaBanco { get; set; }

        [Display(Name = "ContaBanco", ResourceType = typeof(Resources.Resources))]
        public String ContaBanco { get; set; }
        public Int32 TipoContaBanco { get; set; }
        #endregion

        #region Escolaridade

        public Int32? EscolaridadeId { get; set; }
        #endregion

        #region Idiomas

        public IList<IdiomaViewModel> Idiomas { get; set; }
        #endregion

        #region Caracteristicas
        public Int64 IdPerfilModelo { get; set; }
        public Int32? CorPelePerfiModelo { get; set; }

        [Display(Name = "CorOlhosPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Int32? CorOlhosPerfiModelo { get; set; }
     
        [Display(Name = "CorOlhosPerfiModeloTexto", ResourceType = typeof(Resources.Resources))]
        public String CorOlhosPerfiModeloTexto { get; set; }

        [Display(Name = "CorCabeloPerfilModelo", ResourceType = typeof(Resources.Resources))]
        public Int32? CorCabeloPerfilModelo { get; set; }

        [Display(Name = "PesoPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? PesoPerfiModelo { get; set; }

        [Display(Name = "AlturaPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? AlturaPerfiModelo { get; set; }

        [Display(Name = "ManequimPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? ManequimPerfiModelo { get; set; }

        [Display(Name = "CinturaPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? CinturaPerfiModelo { get; set; }

        [Display(Name = "QuadrilPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? QuadrilPerfiModelo { get; set; }

        [Display(Name = "BustoPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? BustoPerfiModelo { get; set; }

        [Display(Name = "CalcadoPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Int32? CalcadoPerfiModelo { get; set; }

        [Display(Name = "OutrosPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public String OutrosPerfiModelo { get; set; }
        #endregion

        #region Tatuagem
        public Int64? IdTatuagem { get; set; }
        [Display(Name = "DescricaoTatuagem", ResourceType = typeof(Resources.Resources))]
        public String DescricaoTatuagem { get; set; }

        [Display(Name = "ParteCorpoTatuagem", ResourceType = typeof(Resources.Resources))]
        public String ParteCorpoTatuagem { get; set; }

        #endregion

        #region Dados Complementares
        [Display(Name = "DisponibilidadeHorario", ResourceType = typeof(Resources.Resources))]
        public Boolean DisponibilidadeHorario { get; set; }

        [Display(Name = "DisponibilidadeViagem", ResourceType = typeof(Resources.Resources))]
        public Boolean DisponibilidadeViagem { get; set; }

        [Display(Name = "PossuiVeiculo", ResourceType = typeof(Resources.Resources))]
        public Boolean PossuiVeiculo { get; set; }
        #endregion

        #region TipoModelo
        [Display(Name = "TipoModelo", ResourceType = typeof(Resources.Resources))]
        public Int32 TipoModelo { get; set; }

        #endregion

        #region Categoria
        [Display(Name = "CategoriaModelo", ResourceType = typeof(Resources.Resources))]
        public Int32 CategoriaModelo { get; set; }

        #endregion

        #region Endereco
        public Int64? IdEndereco { get; set; }

        [Display(Name = "NumeroEndereco", ResourceType = typeof(Resources.Resources))]
        public String NumeroEndereco { get; set; }

        [Display(Name = "ComplementoEndereco", ResourceType = typeof(Resources.Resources))]
        public String ComplementoEndereco { get; set; }

        [Display(Name = "CepEndereco", ResourceType = typeof(Resources.Resources))]
        public String CepEndereco { get; set; }

        [Display(Name = "LogradouroEndereco", ResourceType = typeof(Resources.Resources))]
        public String LogradouroEndereco { get; set; }

        [Display(Name = "BairroEndereco", ResourceType = typeof(Resources.Resources))]
        public String BairroEndereco { get; set; }

        [Display(Name = "ZonaEndereco", ResourceType = typeof(Resources.Resources))]
        public String ZonaEndereco { get; set; }
        public String GDEEndereco { get; set; }

        [Display(Name = "CidadeEndereco", ResourceType = typeof(Resources.Resources))]
        public String CidadeEndereco { get; set; }

        [Display(Name = "EstadoEndereco", ResourceType = typeof(Resources.Resources))]
        public String EstadoEndereco { get; set; }

        [Display(Name = "UFEndereco", ResourceType = typeof(Resources.Resources))]
        public Int32 UfEndereco { get; set; }
        #endregion


        [Display(Name = "DataAprovacao", ResourceType = typeof(Resources.Resources))]
        //[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [DataType(DataType.DateTime)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DataAprovacao { get; set; }

        //todo: adicionar ao form
        [Display(Name = "StatusModelo", ResourceType = typeof(Resources.Resources))]
        public Int32 StatusModelo { get; set; }
    }
    public class IdiomaViewModel
    {
        public Int64? IdiomaId { get; set; }
        public String Descricao { get; set; }
        public Boolean Selecionado { get; set; }
    }
}