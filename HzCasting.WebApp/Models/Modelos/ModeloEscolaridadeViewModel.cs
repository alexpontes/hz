﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloEscolaridadeViewModel
    {
        public int ModeloId { get; set; }
        public Int32? EscolaridadeId { get; set; }
        //public string FormacaoEscolaridade { get; set; }
        //public string SuperiorEscolaridade { get; set; }
        //public string TecnicoEscolaridade { get; set; }
    }
}
