﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloIdiomaViewModel
    {
        public int ModeloId { get; set; }
        public string DescricaoIdioma { get; set; }
        public List<IdiomasViewModel> Idiomas { get; set; }
    }

    public sealed class IdiomasViewModel
    {
        public int IdiomaId { get; set; }
    }
}
