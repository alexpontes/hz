﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloDocumentoViewModel
    {
        public int ModeloId { get; set; }
        public string PisDocumento { get; set; }
        public string RgDocumento { get; set; }
        public string CateiraProfissionalDocumento { get; set; }
        public string Cpf { get; set; }
    }
}
