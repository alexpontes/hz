﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloDadosComplementaresViewModel
    {
        public int ModeloId { get; set; }
        public bool DisponibilidadeHorario { get; set; }
        public bool DisponibilidadeViagem { get; set; }
        public bool PossuiVeiculo { get; set; } 
    }
}
