﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models.Modelos
{
    public sealed class ModeloFotoViewModel
    {
        public HttpPostedFileBase Imagem { get; set; }
        public Int32 IdUsuario { get; set; }
    }
}