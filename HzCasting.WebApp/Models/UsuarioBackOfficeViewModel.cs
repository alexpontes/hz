﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models
{
    public sealed class UsuarioBackOfficeViewModel
    {
        public string UsuarioId { get; set; }

        public string PrimeiroNome { get; set; }

        public string UltimoNome { get; set; }

        public string Email { get; set; }

        public string Cpf { get; set; }
    }
}
