﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models
{
    public sealed class ModeloSocialLogin
    {
        public string FacebookEmail { get; set; }
        public string FacebookToken { get; set; }
        public string Cpf { get; set; }
    }
}
