﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HzCasting.WebApp.Models
{
    public class UsuarioClienteViewModel : UsuarioBase
    {
        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

        public string UsuarioId { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "PrimeiroNome", ResourceType = typeof(Resources.Resources))]
        public string PrimeiroNome { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "UltimoNome", ResourceType = typeof(Resources.Resources))]
        public string UltimoNome { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "DataNascimento", ResourceType = typeof(Resources.Resources))]
        public DateTime DataNascimento { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Telefone", ResourceType = typeof(Resources.Resources))]
        public string Telefone { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Celular", ResourceType = typeof(Resources.Resources))]
        public string Celular { get; set; }

        [Display(Name = "ClienteId", ResourceType = typeof(Resources.Resources))]
        public int ClienteId { get; set; }
    }
}
