﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public class FacebookViewModel
    {
        public Int64 Id { get; set; }
        public String ImageURL { get; set; }
        public Boolean Principal { get; set; }
        public String Status { get;  set; }
    }
}