﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public class ModeloCompositeViewModelo
    {

        public Int64? ModeloId { get; set; }        
        public String UsuarioId { get; set; }

        #region Dados Pessoais
        [Display(Name = "PrimeiroNomeDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String PrimeiroNomeDadosPessoais { get; set; }

        [Display(Name = "UltimoNomeDadosPessoais", ResourceType = typeof(Resources.Resources))]
        public String UltimoNomeDadosPessoais { get; set; }

        #endregion

        #region Fotos

        //public Int64 Id { get; set; }
        //public IEnumerable<String> ImageURL { get; set; }

        public IList<FacebookViewModel> FotosExistentes { get; set; }

        #endregion

        #region Escolaridade

        [Display(Name = "FormacaoEscolaridade", ResourceType = typeof(Resources.Resources))]
        public String FormacaoEscolaridade { get; set; }

        [Display(Name = "TecnicoEscolaridade", ResourceType = typeof(Resources.Resources))]
        public String TecnicoEscolaridade { get; set; }

        [Display(Name = "SuperiorEscolaridade", ResourceType = typeof(Resources.Resources))]
        public String SuperiorEscolaridade { get; set; }
        #endregion
        
        #region Caracteristicas
        public Int64 IdPerfilModelo { get; set; }
        public Int32? CorPelePerfiModelo { get; set; }

        [Display(Name = "CorOlhosPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Int32? CorOlhosPerfiModelo { get; set; }
        public Int64? IdCorCabeloPerfilModelo { get; set; }

        [Display(Name = "CorOlhosPerfiModeloTexto", ResourceType = typeof(Resources.Resources))]
        public String CorOlhosPerfiModeloTexto { get; set; }

        [Display(Name = "CorCabeloPerfilModelo", ResourceType = typeof(Resources.Resources))]
        public String CorCabeloPerfilModelo { get; set; }

        [Display(Name = "PesoPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? PesoPerfiModelo { get; set; }

        [Display(Name = "AlturaPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? AlturaPerfiModelo { get; set; }

        [Display(Name = "ManequimPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? ManequimPerfiModelo { get; set; }

        [Display(Name = "CinturaPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? CinturaPerfiModelo { get; set; }

        [Display(Name = "QuadrilPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? QuadrilPerfiModelo { get; set; }

        [Display(Name = "BustoPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Decimal? BustoPerfiModelo { get; set; }

        [Display(Name = "CalcadoPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public Int32? CalcadoPerfiModelo { get; set; }

        [Display(Name = "OutrosPerfiModelo", ResourceType = typeof(Resources.Resources))]
        public String OutrosPerfiModelo { get; set; }
        #endregion
        
        #region Categoria
        [Display(Name = "CategoriaModelo", ResourceType = typeof(Resources.Resources))]
        public Int32 CategoriaModelo { get; set; }  
        public  String FotoPerfil { get; internal set; }

        #endregion

    }
}