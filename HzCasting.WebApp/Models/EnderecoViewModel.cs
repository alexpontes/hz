﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public class EnderecoViewModel
    {
        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

        public Int64 EnderecoId { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Numero", ResourceType = typeof(Resources.Resources))]
        public String Numero { get; set; }
        
        [Display(Name = "Complemento", ResourceType = typeof(Resources.Resources))]
        public String Complemento { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "CepNumero", ResourceType = typeof(Resources.Resources))]
        public String CepNumero { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Estado", ResourceType = typeof(Resources.Resources))]
        public String Estado { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Cidade", ResourceType = typeof(Resources.Resources))]
        public String Cidade { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Logradouro", ResourceType = typeof(Resources.Resources))]
        public String Logradouro { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Bairro", ResourceType = typeof(Resources.Resources))]
        public String Bairro { get; set; }

        public int UF { get; set; }


    }
}