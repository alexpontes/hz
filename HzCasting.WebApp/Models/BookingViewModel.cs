﻿using HzCasting.WebApp.Models.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public class BookingViewModel
    {
        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "NomeModelo", ResourceType = typeof(Resources.Resources))]
        public String NomeModelo { get; set; }

        public Int64? IdModelo { get; set; }
        public Int32 IdCasting { get; set; }
        public Int32 IdPerfil { get; set; }
        public String ImagemModelo { get; set; }
        public Boolean Selecionada { get; set; }
        public String NomePerfil { get; set; }
        public String StatusInvite { get; set; }
        public DateTime? DataEnvioInvite{ get; set; }
        public string TípoSelecao { get; internal set; }
        public long IdBooking { get; internal set; }
        public DateTime? DataRetornoInvite { get; internal set; }
    }
}