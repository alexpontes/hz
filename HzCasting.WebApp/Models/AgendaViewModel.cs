﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed class AgendaViewModel
    {
        public Int32 CastingId { get; set; }


        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "DataInicio", ResourceType = typeof(Resources.Resources))]
        public DateTime? DataInicio { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "HoraInicio", ResourceType = typeof(Resources.Resources))]
        public DateTime? HoraInicio { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "QuantidadeCandidatos", ResourceType = typeof(Resources.Resources))]
        public Int32? QuantidadeCandidatos { get; set; }

        public Int64 AgendaCastingId { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "HoraFim", ResourceType = typeof(Resources.Resources))]
        public DateTime? HoraFim { get; set; }

        public Int32? PerfilId { get; set; }
        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Local", ResourceType = typeof(Resources.Resources))]
        public String Local { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "CEP", ResourceType = typeof(Resources.Resources))]
        public String Cep { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Cidade", ResourceType = typeof(Resources.Resources))]
        public String Cidade { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Uf", ResourceType = typeof(Resources.Resources))]
        public Int32 Uf { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Bairro", ResourceType = typeof(Resources.Resources))]
        public String Bairro { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Logradouro", ResourceType = typeof(Resources.Resources))]
        public String Logradouro { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Numero", ResourceType = typeof(Resources.Resources))]
        public String Numero { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Complemento", ResourceType = typeof(Resources.Resources))]
        public String Complemento { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Estado", ResourceType = typeof(Resources.Resources))]
        public String Estado { get; set; }

        public string GDE { get; internal set; }
        public string Zona { get; internal set; }

    }
}