﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed class InviteViewModel
    {
        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";
        public Int64? Id { get; set; }
        [Display(Name = "Validade", ResourceType = typeof(Resources.Resources))]
        public DateTime Validade { get; set; }

        [Display(Name = "DataEnvio", ResourceType = typeof(Resources.Resources))]
        public DateTime? DataEnvio { get; set; }

        [Display(Name = "DataRetorno", ResourceType = typeof(Resources.Resources))]
        public DateTime? DataRetorno { get; set; }
        public Int32 StatusTrackInvite { get; set; }
        public Int64 BookingId { get; set; }

        [Display(Name = "NomeEvento", ResourceType = typeof(Resources.Resources))]
        public string NomeEvento { get; set; }

        [Display(Name = "DataEvento", ResourceType = typeof(Resources.Resources))]
        public DateTime? DataEvento { get; set; }

        [Display(Name = "EnderecoLogradouro", ResourceType = typeof(Resources.Resources))]
        public string EnderecoLogradouro { get; set; }

        [Display(Name = "EnderecoBairro", ResourceType = typeof(Resources.Resources))]
        public string EnderecoBairro { get; set; }

        [Display(Name = "EnderecoZona", ResourceType = typeof(Resources.Resources))]
        public string EnderecoZona { get; set; }

        [Display(Name = "EnderecoCep", ResourceType = typeof(Resources.Resources))]
        public string EnderecoCep { get; set; }

        [Display(Name = "EnderecoCidade", ResourceType = typeof(Resources.Resources))]
        public string EnderecoCidade { get; set; }

        [Display(Name = "EnderecoEstado", ResourceType = typeof(Resources.Resources))]
        public string EnderecoEstado { get; set; }

        [Display(Name = "EnderecoNumero", ResourceType = typeof(Resources.Resources))]
        public string EnderecoNumero { get; set; }

        [Display(Name = "Observacao", ResourceType = typeof(Resources.Resources))]
        public string Observacao { get; set; }

        [Display(Name = "Modelo", ResourceType = typeof(Resources.Resources))]
        public string NomeModelo { get; set; }

        public IList<AgendaViewModel> Agendas { get; set; }
    }
}