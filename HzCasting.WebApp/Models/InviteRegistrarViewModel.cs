﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public class InviteRegistrarViewModel
    {
        public Int32 IdCasting { get; set; }
        public Int32 IdModelo { get; set; }
        public Int32 IdPerfil { get; set; }
        public Int32 IdBooking { get; set; }
    }
}