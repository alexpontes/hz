﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed class EventoViewModel
    {
        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";


        public Int64? EventoId { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public HttpPostedFileBase FotoEvento { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Titulo", ResourceType = typeof(Resources.Resources))]
        public String Titulo { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Local", ResourceType = typeof(Resources.Resources))]
        public String Local { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Display(Name = "DataInicio", ResourceType = typeof(Resources.Resources))]
        public DateTime DataInicio { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "HoraInicio", ResourceType = typeof(Resources.Resources))]
        public string HoraInicio { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Display(Name = "DataFim", ResourceType = typeof(Resources.Resources))]
        public DateTime DataFim { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "HoraFim", ResourceType = typeof(Resources.Resources))]
        public string HoraFim { get; set; }

        //[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        //public EnderecoViewModel Endereco { get; set; }

        //[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Obs", ResourceType = typeof(Resources.Resources))]
        public String Obs { get; set; }
        #region Endereco

        public Int64? EnderecoId { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "NumeroEndereco", ResourceType = typeof(Resources.Resources))]
        public String NumeroEndereco { get; set; }

        [Display(Name = "ComplementoEndereco", ResourceType = typeof(Resources.Resources))]
        public String ComplementoEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "CepNumeroEndereco", ResourceType = typeof(Resources.Resources))]
        public String CepNumeroEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "EstadoEndereco", ResourceType = typeof(Resources.Resources))]
        public String EstadoEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "CidadeEndereco", ResourceType = typeof(Resources.Resources))]
        public String CidadeEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "LogradouroEndereco", ResourceType = typeof(Resources.Resources))]
        public String LogradouroEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "BairroEndereco", ResourceType = typeof(Resources.Resources))]
        public String BairroEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "UFEndereco", ResourceType = typeof(Resources.Resources))]
        public int UFEndereco { get; set; }

        #endregion

        public string Data
        {
            get
            {
                return $"{this.DataInicio.ToShortDateString()} a {this.DataFim.ToShortDateString()}";
            }
        }
        public string Horario
        {
            get
            {
                return $"{ this.HoraInicio } a  {this.HoraFim }";
            }
        }

        [Display(Name = "Realizacao", ResourceType = typeof(Resources.Resources))]
        public string Realizacao
        {
            get
            {
                var dataPassada = DataFim <= DateTime.Now;
                return dataPassada ? "Passado" : "Futuro";
            }
        }


        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        //[DataType(DataType.Date)]
        //public DateTime? DataInicioFiltro { get; set; }

        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        //[DataType(DataType.Date)]
        //public DateTime? DataFimFiltro { get; set; }


        //public String TituloFiltro { get; set; }

    }
}