﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed class AgenciaViewModel
    {
        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

        public Int64? Id { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Nome", ResourceType = typeof(Resources.Resources))]
        public String Nome { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public String CPNJ { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Telefone", ResourceType = typeof(Resources.Resources))]
        public String Telefone { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Celular", ResourceType = typeof(Resources.Resources))]
        public String Celular { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "Email", ResourceType = typeof(Resources.Resources))]
        public String Email { get; set; }

        #region Endereco
        public Int64? EnderecoId { get; set; }


        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "NumeroEndereco", ResourceType = typeof(Resources.Resources))]
        public String NumeroEndereco { get; set; }

        [Display(Name = "ComplementoEndereco", ResourceType = typeof(Resources.Resources))]
        public String ComplementoEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "CepNumeroEndereco", ResourceType = typeof(Resources.Resources))]
        public String CepNumeroEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "EstadoEndereco", ResourceType = typeof(Resources.Resources))]
        public String EstadoEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "CidadeEndereco", ResourceType = typeof(Resources.Resources))]
        public String CidadeEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "LogradouroEndereco", ResourceType = typeof(Resources.Resources))]
        public String LogradouroEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "BairroEndereco", ResourceType = typeof(Resources.Resources))]
        public String BairroEndereco { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "UFEndereco", ResourceType = typeof(Resources.Resources))]
        public Int32 UFEndereco { get; set; }

        //[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        //[Display(Name = "GDEEndereco", ResourceType = typeof(Resources.Resources))]
        //public String GDEEndereco { get;  set; }


        //[Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        //[Display(Name = "ZonaEndereco", ResourceType = typeof(Resources.Resources))]
        //public String ZonaEndereco { get;  set; }
        #endregion
    }
}