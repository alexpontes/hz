﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models
{
    public class ClienteHelper
    {
        private readonly IUsuarioClienteApplication _usuarioClienteApplication;

        public ClienteHelper(IUsuarioClienteApplication usuarioClienteApplication)
        {
            _usuarioClienteApplication = usuarioClienteApplication;
        }

        public bool IsUserLoggedCliente(string usuarioId)
        {
            var clientes = _usuarioClienteApplication.Obter(x => x.IdUsuario == usuarioId);

            return clientes.Any();
        }
    }
}
