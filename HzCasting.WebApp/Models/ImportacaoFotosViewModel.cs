﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public class ImportacaoFotosViewModel
    {
        public String UsuarioId { get; set; }
        public Int32 QuantidadeFotosEscolhidas { get; set; }
        public Boolean BackOffice { get; set; }
    }
}