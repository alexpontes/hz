﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HzCasting.WebApp.Models
{
    public sealed class ClienteViewModel  
    {
        private const string MENSAGEM_CAMPO_OBRIGATORIO = "Campo obrigatorio";

        public ClienteViewModel()
        {
            Endereco = new EnderecoViewModel();
        }
        public Int64 ClientId { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "RazaoSocial", ResourceType = typeof(Resources.Resources))]
        public String RazaoSocial { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        [Display(Name = "NomeFantasia", ResourceType = typeof(Resources.Resources))]
        public String NomeFantasia { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public String CPNJ { get; set; }

        [Required(ErrorMessage = MENSAGEM_CAMPO_OBRIGATORIO)]
        public  EnderecoViewModel Endereco { get; set; }

    }
}