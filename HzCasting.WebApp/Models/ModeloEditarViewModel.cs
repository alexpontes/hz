﻿using HzCasting.WebApp.Models.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public class ModeloEditarViewModel : ModeloBaseViewModel
    {
        public string UsuarioId { get; set; }
    }
}