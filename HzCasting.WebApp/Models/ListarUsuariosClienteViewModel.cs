﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.WebApp.Models
{
    public sealed class ListarUsuariosClienteViewModel
    {
        public int ClienteId { get; set; }

        public string NomeFantasia { get; set; }

        public List<UsuariosClienteViewModel> Usuarios { get; set; }

        public ListarUsuariosClienteViewModel()
        {
            Usuarios = new List<UsuariosClienteViewModel>();
        }
    }

    public sealed class UsuariosClienteViewModel
    {
        public string UsuarioId { get; set; }

        public string PrimeiroNome { get; set; }

        public string UltimoNome { get; set; }

        public string Email { get; set; }

        public string Cpf { get; set; }
    }
}
