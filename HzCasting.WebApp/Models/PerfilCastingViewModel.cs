﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HzCasting.WebApp.Models
{
    public sealed class PerfilCastingViewModel
    {
        public Int64? Id { get; set; }
        public String Casting { get; set; }
        public Int64? CastingId { get; set; }
        public Int32? TipoSelecao { get; set; }
        public String Objetivo { get; set; }

        public Int32 PerfilId { get; set; }
        [Display(Name = "Titulo", ResourceType = typeof(Resources.Resources))]
        public String Titulo { get; set; }

        [Display(Name = "QuantidadeCandidato", ResourceType = typeof(Resources.Resources))]
        public Int32? QuantidadeCandidato { get; set; }

        public Int32? IdadeMin { get; set; }
        public Int32? IdadeMax { get; set; }

        public Int32? PesoMin { get; set; }
        public Int32? PesoMax { get; set; }

        public Decimal? AlturaMin { get; set; }
        public Decimal? AlturaMax { get; set; }

        public Int32? ManequinMin { get; set; }
        public Int32? ManequinMax { get; set; }

        public Int32? CinturaMin { get; set; }
        public Int32? CinturaMax { get; set; }

        public Int32? QuadrilMin { get; set; }
        public Int32? QuadrilMax { get; set; }

        public Int32? BustoMin { get; set; }
        public Int32? BustoMax { get; set; }

        public Int32? CalcadoMin { get; set; }
        public Int32? CalcadoMax { get; set; }

        public Boolean Tatuagem { get; set; }

        public Int32? CorPele { get; set; }
        public Int32? CorCabelo { get; set; }
        public Int32? CorOlhos { get; set; }

        public String NomeModelo { get; set; }
        public Int64? IdModelo { get; set; }

        public Int32? AgendaId { get; set; }
    }
}