﻿using System;
using Ninject;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Web.Http.Filters;
using System.Security.Principal;
using System.Web.Http.Controllers;
using HzCasting.Domain.Interfaces.Application;

namespace HzCasting.Api.Authorization
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute
    {
        [Inject]
        public IApiLoginApplication ApiLogin { get; set; }

        [Inject]
        public IUsuarioApplication UserApplication { get; set; }

        bool requireAuthentication = true;
        public bool RequireAuthentication
        {
            get { return requireAuthentication; }
            set { requireAuthentication = value; }
        }

        public BasicAuthenticationAttribute() { }

        public BasicAuthenticationAttribute(bool requireAuthentication)
        {
            this.requireAuthentication = requireAuthentication;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (this.requireAuthentication)
            {
                var identity = ParseAuthorizationHeader(actionContext);
                if (identity == null)
                {
                    UnauthorizedRequest(actionContext);
                    return;
                }

                if (!OnAuthorizeUser(identity.Name, identity.Password, actionContext))
                {
                    UnauthorizedRequest(actionContext);
                    return;
                }

                var user = UserApplication.GetUserByEmail(identity.Name);
                identity.UserId = user.Id;

                var principal = new GenericPrincipal(identity, null);
                Thread.CurrentPrincipal = principal;

                base.OnAuthorization(actionContext);
            }
        }

        protected virtual bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            if (Thread.CurrentPrincipal.Identity.Name.Length == 0)
                return ApiLogin.ValidateApiUser(username, password);

            var currentIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
            return username == currentIdentity.Name && password == currentIdentity.Password;
        }

        protected virtual BasicAuthenticationIdentity ParseAuthorizationHeader(HttpActionContext actionContext)
        {
            string authHeader = null;
            var auth = actionContext.Request.Headers.Authorization;
            if (auth != null && auth.Scheme == "Basic")
                authHeader = auth.Parameter;

            if (string.IsNullOrEmpty(authHeader))
                return null;

            authHeader = Encoding.Default.GetString(Convert.FromBase64String(authHeader));

            var credentials = authHeader.Split(':');
            if (credentials.Length < 2)
                return null;

            var userName = credentials[0];
            var password = credentials[1];

            return new BasicAuthenticationIdentity(userName, password);
        }

        void UnauthorizedRequest(HttpActionContext actionContext)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }
    }
}