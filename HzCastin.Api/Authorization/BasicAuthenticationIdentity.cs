﻿using System.Security.Principal;

namespace HzCasting.Api.Authorization
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        public string Password { get; set; }
        public string UserId { get; set; }

        public BasicAuthenticationIdentity(string name, string password)
        : base(name, "Basic")
        {
            this.Password = password;
        }
    }
}