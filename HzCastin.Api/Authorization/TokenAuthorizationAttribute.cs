﻿using HzCasting.Domain.Interfaces.Application;
using Ninject;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HzCastin.Api.Authorization
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class TokenAuthorizationAttribute : ActionFilterAttribute
    {
        private const string Token = "Token";

        [Inject]
        public ITokenApplication TokenApplication { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Contains(Token))
            {
                var tokenValue = actionContext.Request.Headers.GetValues(Token).First();
                                
                if (TokenApplication != null && !TokenApplication.ValidateToken(tokenValue))
                {
                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                    actionContext.Response = responseMessage;
                }
            }
            else
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            base.OnActionExecuting(actionContext);
        }
    }
}