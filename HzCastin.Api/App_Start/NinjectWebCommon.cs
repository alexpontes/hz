﻿using HzCasting.Application;
using HzCasting.Domain.Interfaces;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Domain.Interfaces.Repository;
using HzCasting.Infra.Repositories;
using HzCasting.Infra.Repositories.EF;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Microsoft.Practices.ServiceLocation;
using CommonServiceLocator.NinjectAdapter.Unofficial;
using Ninject;
using Ninject.Web.Common;
using System;
using System.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(HzCasting.Api.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(HzCasting.Api.App_Start.NinjectWebCommon), "Stop")]

namespace HzCasting.Api.App_Start
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(kernel));

            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>        
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IApiLoginApplication>().To<ApiLoginApplication>();
            kernel.Bind<ITokenApplication>().To<TokenApplication>();

            kernel.Bind<IClienteRepository>().To<ClienteRepository>();
            kernel.Bind<IEventoRepository>().To<EventoRepository>();
            kernel.Bind<IEnderecoRepository>().To<EnderecoRepository>();
            kernel.Bind<IModeloRepository>().To<ModeloRepository>();
            kernel.Bind<IUsuarioRepository>().To<UsuarioRepository>();
            kernel.Bind<ICastingAgendaCastingRepository>().To<CastingAgendaCastingRepository>();
            kernel.Bind<IAgenciaRepository>().To<AgenciaRepository>();
            kernel.Bind<ICastingRepository>().To<CastingRepository>();
            kernel.Bind<IPerfilFiltroCastingRepository>().To<PerfilFiltroCastingRepository>();
            kernel.Bind<IBackOfficeRepository>().To<BackOfficeRepository>();
            kernel.Bind<IAgendaCastingRepository>().To<AgendaCastingRepository>();
            kernel.Bind<IInviteRepository>().To<InviteRepository>();
            kernel.Bind<IUsuarioClienteRepository>().To<UsuarioClienteRepository>();
            kernel.Bind<IBookingRepository>().To<BookingRepository>();
            kernel.Bind<IIdiomaRepository>().To<IdiomaRepository>();

            kernel.Bind<IModeloApplication>().To<ModeloApplication>();
            kernel.Bind<IClienteApplication>().To<ClienteApplication>();
            kernel.Bind<IAgenciaApplication>().To<AgenciaApplication>();
            kernel.Bind<IEventoApplication>().To<EventoApplication>();
            kernel.Bind<IUsuarioApplication>().To<UsuarioApplication>();
            kernel.Bind<ILoginApplication>().To<LoginApplication>();
            kernel.Bind<ICastingApplication>().To<CastingApplication>();
            kernel.Bind<IPerfilFiltroApplication>().To<PerfilFiltroApplication>();
            kernel.Bind<ICastingAgendaCastingApplication>().To<CastingAgendaCastingApplication>();
            kernel.Bind<IBackOfficeApplication>().To<BackOfficeApplication>();
            kernel.Bind<IUsuarioClienteApplication>().To<UsuarioClienteApplication>();
            kernel.Bind<IInviteApplication>().To<InviteApplication>();
            kernel.Bind<IAgendaCastingApplication>().To<AgendaCastingApplication>();
            kernel.Bind<IBookingApplication>().To<BookingApplication>();
            kernel.Bind<IIdiomaApplication>().To<IdiomaApplication>();

            kernel.Bind<IEnderecoApplication>().To<EnderecoApplication>();

            kernel.Bind<ContextManager>().ToSelf();
        }
    }
}
