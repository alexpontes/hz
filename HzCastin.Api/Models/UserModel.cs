﻿using System;
using HzCasting.Domain.Entities;

namespace HzCasting.Api.Models
{
    public class UserModel
    {
        public string Nome { get; set; }
        public string SobreNome { get; set; }
        public string Cpf { get; set; }
        public string Senha { get; set; }
        public string ConfirmarSenha { get; set; }
        public string Email { get; set; }
        public DateTime DataNascimento { get; set; }
        public EstadoCivilEnum EstadoCivil { get; set; }
        public SexoEnum Sexo { get; set; }
    }
}