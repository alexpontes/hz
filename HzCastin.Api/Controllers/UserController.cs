﻿using HzCasting.Api.Models;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace HzCasting.Api.Controllers
{
    public class UserController : ApiController
    {
        private IUsuarioApplication _usuarioApplication;
        private IModeloApplication _modeloApplication;

        public UserController(IUsuarioApplication usuarioApplication, IModeloApplication modeloApplication)
        {
            this._usuarioApplication = usuarioApplication;
            this._modeloApplication = modeloApplication;
        }

        [Route("api/user")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateUserAccount(UserModel model)
        {
            try
            {
                var usuario = new Usuario { CPF = model.Cpf };
                var usuarioExistente = _usuarioApplication.VerificarUsuarioExistente(usuario);

                if (usuarioExistente != null)
                    throw new InvalidOperationException("O usuário já existe.");

                PasswordHasher pwd = new PasswordHasher();

                usuario.UserName = model.Email;
                usuario.Email = model.Email;
                usuario.CPF = model.Cpf;
                usuario.Senha = model.Senha;
                usuario.Perfild = (Int32)PerfilAcessoEnum.Cliente;
                usuario.PasswordHash = pwd.HashPassword(model.Senha);
                //_usuarioApplication.Registrar(usuario);

                Modelo modelo = new Modelo
                {
                    DadosPessoais = new DadosPessoais
                    {
                        PrimeiroNome = model.Nome,
                        UltimoNome = model.SobreNome,
                        Sexo = model.Sexo,
                        DataNascimento = model.DataNascimento,
                        Email = model.Email,
                        EstadoCivil = model.EstadoCivil
                    },
                    DataInclusao = DateTime.Now,
                    IdUsuario = usuario.Id
                };

                await _modeloApplication.Registrar(modelo, usuario);

                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {

                return InternalServerError(ex);
            }
        }
    }
}
