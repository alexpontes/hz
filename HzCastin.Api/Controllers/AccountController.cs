﻿using System;
using System.Threading;
using System.Web.Http;
using HzCasting.Api.Authorization;
using HzCasting.Domain.Interfaces.Application;
using System.Configuration;
using System.Net.Http;
using System.Net;
using HzCastin.Api.Authorization;
using System.Linq;

namespace HzCasting.Api.Controllers
{
    public class AccountController : ApiController
    {
        private IApiLoginApplication _apiLoginApplication;
        private ITokenApplication _tokenApplication;

        public AccountController(IApiLoginApplication apiLoginApplication, ITokenApplication tokenApplication)
        {
            this._apiLoginApplication = apiLoginApplication;
            this._tokenApplication = tokenApplication;
        }

        [Route("api/login")]
        [HttpPost]
        [BasicAuthentication]
        public IHttpActionResult Login()
        {
            try
            {
                if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    var basicAuthenticationIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                    if (basicAuthenticationIdentity != null)
                    {
                        var userId = basicAuthenticationIdentity.UserId;
                        return GetAuthToken(userId);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private IHttpActionResult GetAuthToken(string userId)
        {
            IHttpActionResult response;
            var token = _tokenApplication.GenerateToken(userId);
            HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);
            responseMsg.Headers.Add("Token", token.AuthToken);
            responseMsg.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["AuthTokenExpiry"]);
            responseMsg.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
            response = ResponseMessage(responseMsg);
            return response;
        }

        [Route("api/logout")]
        [HttpPost]
        [TokenAuthorization]
        public IHttpActionResult Logout()
        {
            try
            {
                if (Request.Headers.Contains("Token"))
                {
                    var tokenValue = Request.Headers.GetValues("Token").First();
                    _tokenApplication.Kill(tokenValue);
                    return Ok<Boolean>(true);
                }
                return InternalServerError();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/registerTokenDevice")]
        [HttpPost]
        [TokenAuthorization]
        public IHttpActionResult AddTokenDevice(string tokenDevice)
        {
            try
            {
                //TODO:
                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/registerTokenDevice")]
        [HttpDelete]
        [TokenAuthorization]
        public IHttpActionResult DeleteTokenDevice(string tokenDevice)
        {
            try
            {
                //TODO:
                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/forgotPassword")]
        [HttpPost]
        public IHttpActionResult ForgotPassword(string email)
        {
            try
            {
                this._apiLoginApplication.ForgotPassword(email);
                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/loginFaceBook")]
        [HttpPost]
        public IHttpActionResult LoginFaceBook(string facebookId)
        {
            try
            {
                //TODO:
                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/loginFacebook/linkUserAccount")]
        [HttpPost]
        public IHttpActionResult LinkFacebookUserAccount(string facebookId)
        {
            try
            {
                //TODO:
                return Ok<Boolean>(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
