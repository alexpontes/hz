﻿using HzCasting.Domain.Interfaces;
using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;

namespace HzCasting.Application
{
    public class EnderecoApplication : ApplicationBase, IEnderecoApplication
    {
        private readonly IEnderecoRepository _enderecoRepository;

        public EnderecoApplication(IEnderecoRepository enderecoRepository)
        {
            _enderecoRepository = enderecoRepository;
        }

        public void Atualizar(Endereco item)
        {
            BeginTransaction();
            _enderecoRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            var endereco = this.Obter(id);

            _enderecoRepository.Delete(endereco);
            Commit();
        }

        public IEnumerable<Endereco> Listar()
        {
            return _enderecoRepository.Get();
        }

        public IEnumerable<Endereco> Obter(Func<Endereco, bool> query)
        {
            return _enderecoRepository.Get().Where(query);

        }

        public Endereco Obter(int id)
        {
            var endereco = _enderecoRepository.Get(id);
            if (endereco == null) throw new ApplicationException("endereco não encontrado");
            return endereco;
        }

        public void Registrar(Endereco item)
        {
            BeginTransaction();
            _enderecoRepository.Add(item);
            Commit();
        }
    }
}
