﻿using HzCasting.Application;
using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public sealed class BookingApplication : ApplicationBase, IBookingApplication
    {
        private readonly ICastingApplication _castingApp;
        private readonly IBookingRepository _ibookingRepository;
        private readonly IModeloApplication _modeloApp;
        private readonly IPerfilFiltroApplication _perfilApp;

        public BookingApplication(IBookingRepository ibookingRepository, ICastingApplication castingApp, IModeloApplication modeloApp, IPerfilFiltroApplication perfilApp)
        {
            _perfilApp = perfilApp;
            _castingApp = castingApp;
            _modeloApp = modeloApp;
            _ibookingRepository = ibookingRepository;
        }
        public void Atualizar(Booking item)
        {
            BeginTransaction();
            _ibookingRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            _ibookingRepository.Delete(id);
            Commit();
        }

        public IEnumerable<Booking> Listar()
        {
            return _ibookingRepository.Get();

        }

        public IEnumerable<Booking> Obter(Func<Booking, bool> query)
        {
            return _ibookingRepository.Get().Where(query);
        }

        public Booking Obter(int id)
        {
            return _ibookingRepository.Get(id);
        }

        public void Registrar(Booking item)
        {
            BeginTransaction();
            _ibookingRepository.Add(item);
            Commit();
        }

        public void Registrar(int castingId, int idModelo, int perfilId)
        {
            var casting = _castingApp.Obter(castingId);
            var modelo = _modeloApp.Obter(idModelo);
            //gambi
            modelo.Endereco = modelo.Endereco ?? new Endereco();
            modelo.Endereco.Cep = modelo.Endereco?.Cep ?? new Cep();
            
            var perfil = _perfilApp.Obter(perfilId);
            
            var booking = new Booking
            {
                AgendaCasting = null,
                TipoSelecao = perfil.TipoSelecao,
                Modelo = modelo,
                PerfilFiltro = perfil,
                StatusModeloCastingEnum = StatusModeloCastingEnum.EmAvaliacao
            };

            BeginTransaction();
            _ibookingRepository.Add(booking);
            Commit();
        }
    }
}
