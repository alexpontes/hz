﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;
using Microsoft.Owin.Security.Facebook;
using System.Web.Script.Serialization;
using Facebook;
using System.Dynamic;

namespace HzCasting.Application
{
    public sealed class UsuarioApplication : IUsuarioApplication
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioApplication(IUsuarioRepository usuario)
        {
            _usuarioRepository = usuario;
        }
        public void Atualizar(Usuario item)
        {
            _usuarioRepository.Update(item);
        }

        public void Deletar(int id)
        {
            _usuarioRepository.Delete(id);
        }

        public IEnumerable<Usuario> GetWithRoles()
        {
            return _usuarioRepository.GetWithRoles();
        }

        public void AssociarFacebook(Usuario obj)
        {
            _usuarioRepository.AssociarFacebook(obj);
        }

        public void Deletar(string id)
        {
            _usuarioRepository.Delete(id);
        }

        public IEnumerable<Usuario> Listar()
        {
            return _usuarioRepository.Get();
        }
        
        public IEnumerable<Usuario> Listar(String cpf, String email)
        {
            return this.Obter(m => (cpf != string.Empty ? m.CPF.ToLower().Contains(cpf.ToLower()) : m.CPF != string.Empty) &&
                                   (email != string.Empty ? m.Email.ToLower().Contains(email.ToLower()) : m.Email != string.Empty));

        }

        public IEnumerable<Usuario> Obter(Func<Usuario, bool> query)
        {

            return _usuarioRepository.Get().Where(query);
        }

        public Usuario Obter(int id)
        {
            var usuario = _usuarioRepository.Get(id);
            if (usuario == null) throw new ApplicationException("usuario não encontrado");
            return usuario;
        }

        public void Registrar(Usuario item)
        {
            var usuarioExistente = this.Obter(m => m.Email == item.Email).Any();
            if (usuarioExistente) throw new ApplicationException("Este usuário ja foi registrado");
            _usuarioRepository.Add(item);
        }

        public Usuario VerificarUsuarioExistente(Usuario usuario)
        {

            var user = this.Obter(m =>
                                    m.CPF == usuario.CPF).FirstOrDefault();
            return user;
        }

        public async Task<List<UsuarioFacebook>> ObterFotosUsuarioAsync(string fbTOken, int take = 10, int skip = 0)
        {
            if (fbTOken == null)
                throw new ApplicationException("A modelo selecionada não possui login com facebook associado");

            var fb = new FacebookClient(fbTOken);
            var fotosUsuario = new List<UsuarioFacebook>();
            var parameters = new
            {
                limit = take,
                offset = skip
            };


            var fotos = (IDictionary<string, dynamic>)await fb.GetTaskAsync("/me/photos", parameters);
            var count = 0;

            foreach (IDictionary<string, dynamic> item in fotos["data"])
            {
                var imagem = (item["images"] as  JsonArray).ElementAtOrDefault(0) as IDictionary<string, dynamic>;
                //{
                count += 1;
                fotosUsuario.Add(new UsuarioFacebook
                {
                    Id = count,
                    ImageURL = imagem["source"]
                }); 


            }
            return fotosUsuario;

        }

        public Usuario GetUserByEmail(string email)
        {
            var user = _usuarioRepository.GetUserByEmail(email);
            return user;
        }
    }
}
