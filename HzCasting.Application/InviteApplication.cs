﻿using HzCasting.Domain.Interfaces.Application;
using HzCasting.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;

namespace HzCasting.Application
{
    public class InviteApplication : ApplicationBase, IInviteApplication
    {
        private IInviteRepository _inviteRepository;
        private readonly IBookingApplication _bookingApp;

        public InviteApplication(IInviteRepository inviteRepository, IBookingApplication bookingApp)
        {
            _inviteRepository = inviteRepository;
            _bookingApp = bookingApp;
        }

        public void Atualizar(Invite item)
        {
            BeginTransaction();
            _inviteRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            throw new NotImplementedException();
        }

        public void RegistrarInvites(int castingId)
        {
            BeginTransaction();
            throw new NotImplementedException();
        }

        public IEnumerable<Invite> Listar()
        {
            return _inviteRepository.Get().ToList();
        }

        public IEnumerable<Invite> ListarInvitesModelo(Int64 Id, StatusTrackInviteEnum? status)
        {
            return this.Obter(m => m.Booking?.Modelo?.Id == Id && m.StatusTrackInvite == status || status == null);
        }

        public IEnumerable<Invite> Obter(Func<Invite, bool> query)
        {
            return _inviteRepository.Get().Where(query).ToList();
        }

        public Invite Obter(int id)
        {
            return _inviteRepository.Get(id);
        }

        public void Registrar(Invite item)
        {
            BeginTransaction();
            _inviteRepository.Add(item);
            Commit();
        }



        //public void RegistrarInviteModelo(int castingId, int modeloId)
        //{
        //    throw new NotImplementedException();
        //}

        public void RegistrarInvites(IList<Invite> invites)
        {

            foreach (var item in invites)
            {
               item.Booking = _bookingApp.Obter((Int32)item.Booking.Id);

            }
            this.Registrar(invites);
        }

        public void Registrar(IEnumerable<Invite> invites)
        {
            BeginTransaction();
            invites.ToList().ForEach(m => _inviteRepository.Add(m));
            Commit();
        }
    }
}
