﻿using System;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Domain.Entities;
using System.Configuration;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public class TokenApplication : ITokenApplication
    {
        private readonly ITokenRepository _tokenRepository;

        public TokenApplication(ITokenRepository tokenRepository)
        {
            this._tokenRepository = tokenRepository;
        }
        
        public void DeleteByUserId(string userId)
        {
            _tokenRepository.DeleteByUserId(userId);
        }

        public Token GenerateToken(string userId)
        {
            string token = Guid.NewGuid().ToString();
            DateTime issuedOn = DateTime.Now;
            DateTime expiredOn = DateTime.Now.AddSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]));
            var tokendomain = new Token
            {
                UserId = userId,
                AuthToken = token,
                IssuedOn = issuedOn,
                ExpiresOn = expiredOn
            };

            _tokenRepository.Add(tokendomain);

            return tokendomain;
        }

        public void Kill(string token)
        {
            _tokenRepository.DeleteByToken(token);            
        }

        public bool ValidateToken(string token)
        {
            var entity = _tokenRepository.GetByToken(token);
            if (token != null && DateTime.Now < entity.ExpiresOn)
            {
                entity.ExpiresOn = entity.ExpiresOn.AddSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]));
                _tokenRepository.Update(entity);
                return true;
            }
            return false;
        }
    }
}
