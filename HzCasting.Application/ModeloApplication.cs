﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces;
using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Infra.Common;
using System.Web;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using HzCasting.Infra.Repositories.EF.Context;
using System.Net;

namespace HzCasting.Application
{
    public sealed class ModeloApplication : ApplicationBase, IModeloApplication
    {
        private readonly IModeloRepository _modeloRepository;
        private readonly IUsuarioApplication _usuarioApplication;
        private readonly ApplicationSignInManager _signInManager;
        private readonly ApplicationUserManager _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private readonly ICastingApplication _castingApplication;
        private readonly IPerfilFiltroApplication _perfilFiltroApplication;

        public ModeloApplication(IModeloRepository modeloRepository,
            IUsuarioApplication usuarioApplication,
            ICastingApplication castingApplication,
            IPerfilFiltroApplication perfilFiltroApplication)
        {
            _usuarioApplication = usuarioApplication;
            _modeloRepository = modeloRepository;
            _userManager = new ApplicationUserManager(new UserStore<Usuario>(new ApplicationDbContext()));
            //_userManager = new ApplicationUserManager(new Applicatio;
            // _signInManager = new ApplicationSignInManager(_userManager, AuthenticationManager);
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            _castingApplication = castingApplication;
            _perfilFiltroApplication = perfilFiltroApplication;
        }

        public async void Registrar(Modelo modelo)
        {
            BeginTransaction();

            _modeloRepository.Add(modelo);

            Commit();

            var user = _usuarioApplication.Obter(x => x.Id == modelo.IdUsuario).FirstOrDefault();

            if (user != null)
            {
                await _userManager.AddToRolesAsync(user.Id, "Modelo");
            }
        }

        public async Task RegistrarAsync(Modelo modelo)
        {
            BeginTransaction();
            modelo.Documento = modelo.Documento ?? new Documento();
            modelo.Documento.Cpf = modelo.Documento.Cpf ?? new Cpf();
            modelo.Escolaridade = modelo.Escolaridade;
            _modeloRepository.Add(modelo);

            Commit();

            var user = _usuarioApplication.Obter(x => x.Id == modelo.IdUsuario).FirstOrDefault();

            if (user != null)
            {
                await _userManager.AddToRolesAsync(user.Id, "Modelo");
            }



        }

        public async Task Registrar(Modelo modelo, Usuario usuario)
        {
            BeginTransaction();

            modelo.IdUsuario = usuario.Id;

            _usuarioApplication.Registrar(usuario);
            _modeloRepository.Add(modelo);

            if (usuario != null)
            {
                await _userManager.AddToRolesAsync(usuario.Id, "Modelo");
            }

            Commit();

        }

        public IEnumerable<Modelo> Listar()
        {
            return _modeloRepository.Get();
        }

        public IEnumerable<Modelo> Listar(String nome, String rg, String cpf, DateTime? dataNascimento)
        {
            var modelos = this.Obter(m =>
                                  (nome != string.Empty ? (m.DadosPessoais?.PrimeiroNome ?? string.Empty).ToLower().Contains(nome.ToLower()) : m.DadosPessoais?.PrimeiroNome != null) &&
                                  (rg != string.Empty ? (m.Documento?.Rg ?? string.Empty).ToLower().Contains(rg.ToLower()) : (m.Documento != null)) &&
                                  (cpf != string.Empty ? (m.Documento?.Cpf?.Numero ?? string.Empty).ToLower().Contains(cpf.ToLower()) : m.Documento?.Cpf != null) &&
                                  (dataNascimento != null ? (m.DadosPessoais.DataNascimento == dataNascimento) : m.DadosPessoais?.DataNascimento != DateTime.MinValue)
                                  );

            return modelos;
        }

        public Modelo Obter(int id)
        {

            var modelo = _modeloRepository.Get(id);
            if (modelo == null) throw new ApplicationException("modelo não encontrada");
            return modelo;
        }

        public IEnumerable<Modelo> Obter(Func<Modelo, bool> query)
        {
            return _modeloRepository.Get().Where(query);
        }

        public void Atualizar(Modelo item)
        {

            BeginTransaction();
            _modeloRepository.Update(item);
            Commit();

        }

        public void AtualizarContato(Modelo item)
        {
            _modeloRepository.AtualizarContato(item);
        }

        public void AtualizarDocumento(Modelo item)
        {
            _modeloRepository.AtualizarDocumento(item);
        }

        public void AtualizarDadosBancarios(Modelo item)
        {
            _modeloRepository.AtualizarDadosBancarios(item);
        }

        public void AtualizarEscolaridade(Modelo item)
        {
            _modeloRepository.AtualizarEscolaridade(item);
        }

        public void AtualizarIdioma(Modelo item)
        {
            _modeloRepository.AtualizarIdioma(item);
        }

        public void AtualizarCaracteristicas(Modelo item)
        {
            _modeloRepository.AtualizarCaracteristicas(item);
        }

        public void AtualizarDadosComplementares(Modelo item)
        {
            _modeloRepository.AtualizarDadosComplementares(item);
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            _modeloRepository.Delete(id);
            Commit();
        }

        public decimal CalcularPercentualCadastro(string idUsuario)
        {
            var modelo = this.Obter(m => m.IdUsuario == idUsuario).FirstOrDefault();
            var propriedades = modelo?.GetType().GetProperties();


            var propriedadesSemValor = propriedades
                                        .Count(propertyInfo => (propertyInfo.GetValue(modelo) == null));

            var percentual = 100 - (((decimal)propriedadesSemValor / (decimal)propriedades?.Count()) * 100);
            return percentual;
        }

        public void AtualizarDadosPereciveis(int idModelo, PerfilModelo modelo)
        {

            var modeloRep = Obter(idModelo);
            modeloRep.PerfilModelo = modelo;
            Atualizar(modeloRep);
        }

        public void InserirImagens(string usuarioId, IList<HttpPostedFileBase> imagens)
        {
            var modelo = this.Obter(m => m.IdUsuario == usuarioId).FirstOrDefault();
            foreach (var imagem in imagens)
            {
                var foto = new FotoModelo
                {
                    Descricao = imagem.FileName,
                    Tipo = imagem.ContentType,
                    ImagemBinario = imagem.GetArrayBytes()

                };
                modelo?.Fotos?.Add(foto);
            }

            this.Atualizar(modelo);
        }


        public void CreateRoles()
        {
            _roleManager.Create(new IdentityRole { Name = "Admin" });
            _roleManager.Create(new IdentityRole { Name = "Modelo" });
            _roleManager.Create(new IdentityRole { Name = "Agencia" });
            _roleManager.Create(new IdentityRole { Name = "Cliente" });
        }

        public string Role(int perfilId)
        {
            switch (perfilId)
            {
                case 1: return "Admin";

                case 2: return "Modelo";

                case 3: return "Agencia";

                case 4: return "Cliente";

                default: return null;
            }
        }

        public IEnumerable<Modelo> ObterModelosPerfilFiltro(int perfilId, int castingId, out String nomePerfil)
        {
            var perfil = _perfilFiltroApplication.Obter(perfilId);
            nomePerfil = perfil.Titulo;
            var modeloFiltro = this.Obter(m =>
                                                 (m.PerfilModelo?.Altura >= perfil.AlturaMin || perfil.AlturaMin == null)
                                              && (m.PerfilModelo?.Altura <= perfil.AlturaMax || perfil.AlturaMax == null)
                                              && (m.PerfilModelo?.Busto >= perfil.BustoMin || perfil.BustoMin == null)
                                              && (m.PerfilModelo?.Busto <= perfil.BustoMax || perfil.BustoMax == null)
                                              && (m.PerfilModelo?.Calcado >= perfil.CalcadoMin || perfil.CalcadoMin == null)
                                              && (m.PerfilModelo?.Altura <= perfil.CalcadoMax || perfil.CalcadoMax == null)
                                              && (m.PerfilModelo?.Cintura >= perfil.CinturaMin || perfil.CinturaMin == null)
                                              && (m.PerfilModelo?.Cintura <= perfil.CinturaMax || perfil.CinturaMax == null)
                                              && (m.PerfilModelo?.Manequim >= perfil.ManequimMin || perfil.ManequimMin == null)
                                              && (m.PerfilModelo?.Manequim <= perfil.ManequimMax || perfil.ManequimMax == null)
                                              && (m.PerfilModelo?.Peso >= perfil.PesoMin || perfil.PesoMin == null)
                                              && (m.PerfilModelo?.Peso <= perfil.PesoMax || perfil.PesoMax == null)
                                              && (m.PerfilModelo?.Quadril >= perfil.QuadrilMin || perfil.QuadrilMin == null)
                                              && (m.PerfilModelo?.Quadril <= perfil.QuadrilMax || perfil.QuadrilMax == null)
                                              && (m.PerfilModelo?.CorCabelo == perfil.CorCabelo)
                                              && (m.PerfilModelo?.CorOlhos == perfil.CorOlhos)
                                              && (m.PerfilModelo?.CorPele == perfil.CorPele)
                                              && ((m.Tatuagem != null) == perfil.Tatuagem)
                                              );
            // var modeloFiltro = this.Listar();
            return modeloFiltro;
        }

        public void AdicionarModeloAoCasting(int castingId, int idModelo)
        {
            var modelo = this.Obter(idModelo);
            modelo.Castings = modelo.Castings ?? new List<Casting>();
            var casting = _castingApplication.Obter(castingId);
            modelo.Castings.Add(casting);
            this.Atualizar(modelo);

        }

        public bool VerificarSelecaoPerfilCasting(long IdModelo, long IdPerfilCasting)
        {
            throw new NotImplementedException();
        }

        public void RemoverFoto(String usuarioId, Int64 idFoto)
        {
            var modelo = this.Obter(m => m.IdUsuario == usuarioId)?.FirstOrDefault();
            var foto = modelo?.Fotos.FirstOrDefault(m => m.Id == idFoto);
            modelo.Fotos.Remove(foto);
            this.Atualizar(modelo);
            //var modelo = this.Obter(m => m.Fotos?);

        }

        public void AlterarFotoPrincipal(string usuarioId, long idFoto)
        {
            var modelo = this.Obter(m => m.IdUsuario == usuarioId).FirstOrDefault();
            modelo?.Fotos.ToList().ForEach(m => m.Principal = false);
            var foto = modelo?.Fotos.FirstOrDefault(m => m.Id == idFoto);
            foto.Principal = true;
            this.Atualizar(modelo);


        }

        public void AprovarFoto(string usuarioId, long idFoto)
        {

            var modelo = this.Obter(m => m.IdUsuario == usuarioId).FirstOrDefault();
            var foto = modelo?.Fotos.FirstOrDefault(m => m.Id == idFoto);
            foto.Status = StatusFotoModeloEnum.Aceito;
            this.Atualizar(modelo);
        }
    }
}
