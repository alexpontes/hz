﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public sealed class AgendaCastingApplication : ApplicationBase, IAgendaCastingApplication
    {
        private IAgendaCastingRepository _agendaRepository;
        private readonly ICastingApplication _castingApplication;

        public AgendaCastingApplication(IAgendaCastingRepository agendaRepository)
        {
            _agendaRepository = agendaRepository;
    }
         
        public void Registrar(AgendaCasting agenda)
        {
            BeginTransaction();

            _agendaRepository.Add(agenda);
            
            Commit();
        }
        public IEnumerable<AgendaCasting> Listar()
        {
            return _agendaRepository.Get().ToList();
        }

        public AgendaCasting Obter(int id)
        {
            var cliente = _agendaRepository.Get(id);
            if (cliente == null)
                throw new ApplicationException("AgendaCasting não encontrado !");
            return cliente;
        }

      
        public void Atualizar(AgendaCasting item)
        {
            BeginTransaction(); 
            _agendaRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction(); 
            _agendaRepository.Delete(id);
            Commit();
        }

        public IEnumerable<AgendaCasting>  Obter(Func<AgendaCasting, bool> query)
        {
            return _agendaRepository.Get().Where(query); 
        }
    }
}
