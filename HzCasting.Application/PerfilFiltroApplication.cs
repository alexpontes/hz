﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public sealed class PerfilFiltroApplication : ApplicationBase, IPerfilFiltroApplication
    {
        private readonly IAgendaCastingApplication _agendaApplication;
        private readonly ICastingApplication _castingApplication;
        private readonly IPerfilFiltroCastingRepository _perfilCastingRepository;
        public PerfilFiltroApplication(IPerfilFiltroCastingRepository perfilCastingRepository,
            ICastingApplication castingApplication,
            IAgendaCastingApplication agendaApplication)
        {
            _perfilCastingRepository = perfilCastingRepository;
            _castingApplication = castingApplication;
            _agendaApplication = agendaApplication;
        }
        public void Atualizar(PerfilFiltro item)
        {
            BeginTransaction();
            _perfilCastingRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            _perfilCastingRepository.Delete(id);
            Commit();
        }

        public IEnumerable<PerfilFiltro> Listar()
        {
            return _perfilCastingRepository.Get().ToList();
        }

        public IEnumerable<PerfilFiltro> Obter(Func<PerfilFiltro, bool> query)
        {
            return _perfilCastingRepository.Get().Where(query).ToList();
        }

        public PerfilFiltro Obter(int id)
        {
            return _perfilCastingRepository.Get(id);
        }



        public void Registrar(PerfilFiltro item)
        {
            BeginTransaction();
            var casting = _castingApplication.Obter((Int32)item.Casting.Id);
            var agendas = item.AgendasCasting?.Select(m => m.Id)?.Select(id => _agendaApplication.Obter((Int32)id));
            //agendas.ToList().ForEach((casting.Agendas ?? new List<AgendaCasting>()).Add);
            item.AgendasCasting = agendas?.ToList();
            item.Casting = casting;

            _perfilCastingRepository.Add(item);
            Commit();
        }
    }
}
