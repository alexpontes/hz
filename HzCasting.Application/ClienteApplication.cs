﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces;

namespace HzCasting.Application
{
    public sealed class ClienteApplication : ApplicationBase, IClienteApplication
    {
        private IClienteRepository _clienteRepository;

        public ClienteApplication(IClienteRepository ClienteRepository)
        {
            _clienteRepository = ClienteRepository;
        }

        public void Registrar(Cliente Cliente)
        {
            BeginTransaction();

            _clienteRepository.Add(Cliente);

            Commit();
        }

        public IEnumerable<Cliente> Listar()
        {
            return _clienteRepository.Get().ToList();
        }

        public IEnumerable<Cliente> Listar(String RazaoSocial, String NomeFantasia, String CPNJ)
        {
            return this.Obter(m => (RazaoSocial != string.Empty ? m.RazaoSocial.ToLower().Contains(RazaoSocial.ToLower()) : m.RazaoSocial != string.Empty) &&
                                   (NomeFantasia != string.Empty ? m.NomeFantasia.ToLower().Contains(NomeFantasia.ToLower()) : m.NomeFantasia != string.Empty) &&
                                   (CPNJ != string.Empty ? m.CPNJ.ToLower().Contains(CPNJ.ToLower()) : m.CPNJ != string.Empty));

        }

        public Cliente Obter(int id)
        {
            var cliente = _clienteRepository.Get(id);
            if (cliente == null)
                throw new ApplicationException("Cliente não encontrado !");
            return cliente;
        }

        public IEnumerable<Cliente> Obter(Func<Cliente, bool> query)
        {
            return _clienteRepository.Get().Where(query);
        }

        public void Atualizar(Cliente item)
        {
            BeginTransaction();
            item.Id = this.Obter(m => m.CPNJ == item.CPNJ)?.FirstOrDefault()?.Id ?? 0;
            _clienteRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            var cliente = this.Obter(id);
            _clienteRepository.Delete(cliente);
            Commit();
        }

	public bool VerificarUsuarioExistente(Cliente cliente) {

	 return this.Obter(m => m.CPNJ == cliente.CPNJ && m.Id != cliente.Id).Any();
	}

  }
}
