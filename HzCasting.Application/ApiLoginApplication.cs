﻿using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Common;
using System;
using System.Text;
using System.Security.Claims;

using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using System.Security.Cryptography;

namespace HzCasting.Application
{
    public sealed class ApiLoginApplication : IApiLoginApplication        
    {
        private IUsuarioApplication _usuarioapplication;

        public ApiLoginApplication(IUsuarioApplication usuarioapplication)
        {
            this._usuarioapplication = usuarioapplication;
        }

        public bool ValidateApiUser(string userName, string password)
        {   
            var usuario = _usuarioapplication.GetUserByEmail(userName);
            if(usuario != null)
            {
                PasswordHasher pwd = new PasswordHasher();
                PasswordVerificationResult ret = pwd.VerifyHashedPassword(usuario.PasswordHash, password);
                switch (ret)
                {
                    case PasswordVerificationResult.Success:
                    case PasswordVerificationResult.SuccessRehashNeeded: return true;
                    case PasswordVerificationResult.Failed:
                    default: return false;
                }
            }
            return false;
        }

        public void ForgotPassword(string email)
        {
            var user =  _usuarioapplication.GetUserByEmail(email);

            if (user != null)
            {
                var password = System.Web.Security.Membership.GeneratePassword(8, 2);
                password += "0";
                PasswordHasher pwd = new PasswordHasher();
                string hashPwd = pwd.HashPassword(password);

                user.PasswordHash = hashPwd;

                _usuarioapplication.Atualizar(user);

                Mail mail = new Mail();

                mail.Add(email);

                var mensagem = new StringBuilder();

                mensagem.AppendLine("Senha alterada com sucesso, sua nova senha é: <b>");
                mensagem.Append(password);
                mensagem.AppendLine(@" </b> <br/>
                                    <a href='castingh.hzeventos.com.br/Login/AlterarSenha'>
                                        Clique aqui para alterar sua senha
                                    </a>");

                mail.Send("[HzCasting] Reset de senha", mensagem.ToString());
            }
        }
    }
}