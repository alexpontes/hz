﻿using System;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Domain.Interfaces.Repository;
using HzCasting.Domain.Entities;

namespace HzCasting.Application
{
    public class TokenDeviceApplication : ITokenDeviceApplication
    {
        private readonly ITokenDeviceRepository _tokenDeviceRepository;

        public TokenDeviceApplication(ITokenDeviceRepository tokenDeviceRepository)
        {
            this._tokenDeviceRepository = tokenDeviceRepository;
        }

        public void Kill(string token)
        {
            _tokenDeviceRepository.Kill(token);
        }

        public void Register(string token, string userId)
        {
            var entity = new TokenDevice
            {
                Ativo = true,
                DataInclusao = DateTime.Now,
                Token = token,
                UserId = userId
            };

            _tokenDeviceRepository.Add(entity);
        }
    }
}
