﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public sealed class IdiomaApplication : ApplicationBase, IIdiomaApplication
    {
        private readonly IIdiomaRepository _repository;

        public IdiomaApplication(IIdiomaRepository repository)
        {
            _repository = repository;
        }

        public void Atualizar(Idioma item)
        {
            throw new NotImplementedException();
        }

        public void Deletar(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Idioma> Listar()
        {
            return _repository.Get();
        }

        public IEnumerable<Idioma> Obter(Func<Idioma, bool> query)
        {
            return _repository.Get().Where(query);
        }

        public Idioma Obter(int id)
        {
            throw new NotImplementedException();
        }

        public void Registrar(Idioma item)
        {
            throw new NotImplementedException();
        }
    }
}
