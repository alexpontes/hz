﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Infra.Common;
using HzCasting.Infra.Repositories.EF.Context;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HzCasting.Application
{
    public sealed class LoginApplication : ILoginApplication
    {
        private readonly ApplicationSignInManager _signInManager;
        private readonly ApplicationUserManager _userManager;
        private RoleManager<IdentityRole> _roleManager;

        public LoginApplication(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            CreateRoles();
        }

        public async Task<IdentityResult> CadastrarUsuarioAsync(Usuario model)
        {
            var result = await _userManager.CreateAsync(model, model.Senha);
            if (!result.Succeeded) return result;

            await LogarUserAsync(model);

            await VincularRole(model);

            return result;

            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
        }

        public async Task VincularRole(Usuario model)
        {
            var perfil = Role(model.Perfild);

            if (!string.IsNullOrEmpty(perfil))
            {
                await _userManager.AddToRolesAsync(model.Id, perfil);
            }
        }
        public async Task<object> LogarUserAsync(Usuario user)
        {
            await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            return Task.FromResult<object>(null);
        }

        public async Task ResetPassword(string email)
        {
            var user = await _userManager.FindByNameAsync(email);
            if (user != null)// || !(await _userManager.IsEmailConfirmedAsync(user.Id)))
            {
                var code = await _userManager.GeneratePasswordResetTokenAsync(user.Id);

                var password = System.Web.Security.Membership.GeneratePassword(8, 2);

                password += "0";

                var identity = _userManager.ResetPassword(user.Id, code, password);

                Mail mail = new Mail();

                mail.Add(email);

                var mensagem = new StringBuilder();

                mensagem.AppendLine("Senha alterada com sucesso, sua nova senha é: <b>");
                mensagem.Append(password);
                mensagem.AppendLine(@" </b> <br/>
                                    <a href='castingh.hzeventos.com.br/Login/AlterarSenha'>
                                        Clique aqui para alterar sua senha
                                    </a>");

                mail.Send("[HzCasting] Reset de senha", mensagem.ToString());
            }
        }

        public void CreateRoles()
        {
            _roleManager.Create(new IdentityRole { Name = "Admin" });
            _roleManager.Create(new IdentityRole { Name = "Modelo" });
            _roleManager.Create(new IdentityRole { Name = "Agencia" });
            _roleManager.Create(new IdentityRole { Name = "Cliente" });
        }

        public string Role(int perfilId)
        {
            switch (perfilId)
            {
                case 1: return "Admin";

                case 2: return "Modelo";

                case 3: return "Agencia";

                case 4: return "Cliente";

                default: return null;
            }
        }

        public bool ValidateApiUser(string name, string password)
        {
            return true;
        }
    }
}
