﻿using HzCasting.Domain.Interfaces;
using HzCasting.Infra.Repositories.EF;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Application
{
    public abstract class ApplicationBase
    {
        private IUnitOfWork _unitOfWork;
        public void BeginTransaction()
        {
            _unitOfWork = ServiceLocator.Current.GetInstance<UnitOfWork>();
            _unitOfWork.BeginTransaction();

        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

    }
}
