﻿
using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces;
using System.Linq.Expressions;

namespace HzCasting.Application
{
    public sealed class EventoApplication : ApplicationBase, IEventoApplication
    {
        private IEventoRepository _eventoRepository;

        public EventoApplication(IEventoRepository eventoRepository)
        {
            _eventoRepository = eventoRepository;
        }

        public void Atualizar(Evento item)
        {
            BeginTransaction();
            _eventoRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            var evento = this.Obter(id);
            if (evento == null) throw new ApplicationException("Evento não encontrado");

            _eventoRepository.Delete(evento);
            Commit();
        }

        public IEnumerable<Evento> Listar()
        {
            return _eventoRepository.Get();
        }

        public IEnumerable<Evento> Listar(String titulo, DateTime? dataInicio, DateTime? dataFim, Boolean passado, Boolean futuro)
        {
            return this.Obter(m => (titulo != string.Empty ? m.Titulo.ToLower().Contains(titulo.ToLower()) : m.Titulo != string.Empty) &&
                                    (dataFim != null ? (m.DataFim.Date == dataFim && m.DataInicio.Date < dataFim) : m.DataFim.Date != DateTime.MinValue) &&
                                    (dataInicio != null ? (m.DataInicio.Date == dataInicio && m.DataFim.Date > dataInicio) : m.DataInicio.Date != DateTime.MinValue) &&
                                    (passado == true ? (m.DataFim.Date <= DateTime.Now) : m.DataInicio.Date != DateTime.MinValue) &&
                                    (futuro == true ? (m.DataFim.Date > DateTime.Now) : m.DataInicio.Date != DateTime.MinValue));

        }
         

        public Evento Obter(int id)
        {
            var evento = _eventoRepository.Get(id);
            if (evento == null) throw new ApplicationException("evento não encontrado");
            return evento;
        }

        public IEnumerable<Evento> Obter(Func<Evento, bool> query)
        {
            return _eventoRepository.Get().Where(query);
        }

        public void Registrar(Evento item)
        {
            BeginTransaction();


            _eventoRepository.Add(item);

            Commit();
        }

    }
}
