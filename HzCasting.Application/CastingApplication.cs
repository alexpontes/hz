﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Application;
using HzCasting.Domain.Interfaces.Repository;
using HzCasting.Infra.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HzCasting.Application
{
    public class CastingApplication : ApplicationBase, ICastingApplication
    {
        private readonly ICastingRepository _castingRepository;
        private readonly IModeloApplication _modeloApplication;
        private readonly IClienteApplication _clienteApplication;
        private readonly IEventoApplication _eventoApplication;
        private readonly IAgenciaApplication _agenciaApplication;

        public CastingApplication(ICastingRepository castingRepository,
                                  //IModeloApplication modeloApplication,
                                  IClienteApplication clienteApplication,
                                  IEventoApplication eventoApplication,
                                  IAgenciaApplication agenciaApplication)
        {
            _castingRepository = castingRepository;
            //_modeloApplication = modeloApplication;
            _clienteApplication = clienteApplication;
            _eventoApplication = eventoApplication;
            _agenciaApplication = agenciaApplication;
        }

        public IEnumerable<Casting> Listar()
        {
            return _castingRepository.Get().ToList();
        }

        public void Atualizar(Casting item)
        {
            BeginTransaction();
            _castingRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            _castingRepository.Delete(id);
            Commit();
        }

        //public IEnumerable<Casting> Listar()
        //{
        //    return _castingRepository.GetComplete().ToList();
        //}

        public IEnumerable<Casting> Listar(String titulo, String cliente, String agencia, DateTime? dataFim, DateTime? dataInicio, Int32? statusId, bool? criadoCliente)
        {
            //    return this.Obter(m => (titulo != string.Empty ? m.Evento.Titulo.ToLower().Contains(titulo.ToLower()) : m.Titulo != string.Empty) &&
            //                           (cliente != string.Empty ? m.Cliente.RazaoSocial.ToLower().Contains(cliente.ToLower()) : m.Cliente.NomeFantasia != string.Empty) &&
            //                           (agencia != string.Empty ? m.Agencia.Nome.ToLower().Contains(agencia.ToLower()) : m.Agencia.Nome != string.Empty) &&
            //                           (statusId != null ? (int)m.StatusCasting == statusId : m.StatusCasting != null) &&
            //                           (criadoCliente != null ? m.CriadoCliente : m.CriadoCliente != null));
            //(dataFim != null ? (m.DataFim.Date == dataFim && m.DataInicio.Date < dataFim) : m.DataFim.Date != DateTime.MinValue) &&
            //(dataInicio != null ? (m.DataInicio.Date == dataInicio && m.DataFim.Date > dataInicio) : m.DataInicio.Date != DateTime.MinValue));
            return this.Obter(c => 
									(c.Evento?.Titulo.ToLower() == titulo?.ToLower() || string.IsNullOrEmpty(titulo)) &&
									(c.Cliente?.NomeFantasia.ToLower() == cliente?.ToLower() || string.IsNullOrEmpty(cliente)) &&
									(c.Agencia?.Nome.ToLower() == agencia?.ToLower() || string.IsNullOrEmpty(agencia)) &&
									(c.Agendas == null || c.Agendas.Count() == 0 ||  c.Agendas.Any(a => ((a.DataHoraInicio >= dataInicio || dataInicio == null) && (a.DataHoraFim <= dataFim || dataFim == null)))) &&
									((int)c.StatusCasting == statusId || statusId == null) &&
									(c.CriadoCliente == criadoCliente || criadoCliente == null));
        }

        public IEnumerable<Casting> Obter(Func<Casting, bool> query)
        {
            return _castingRepository.Get().Where(query).ToList();
        }

        public Casting Obter(int id)
        {
            return _castingRepository.Get(id);
        }

        public void Registrar(Casting item)
        {
            BeginTransaction();
            _castingRepository.Add(item);
            Commit();
        }

        public IEnumerable<Modelo> ListarModelosCasting(Int64 castingId)
        {

            var modelos = this.Obter(m => m.Id == castingId).SelectMany(m => m.Modelos);

            return modelos;
        }

        public void Registrar(long idCliente, long idEvento, HttpPostedFileBase imagem, long? idAgencia = null)
        {
            var casting = this.PopularCasting(idCliente, idEvento, imagem, idAgencia);
            this.Registrar(casting);
        }

        public void Atualizar(long idCliente, long idEvento, HttpPostedFileBase imagem, long? idAgencia, long? idCasting = null)
        {

            var casting = this.PopularCasting(idCliente, idEvento, imagem, idAgencia, idCasting);

            this.Atualizar(casting);
        }

        #region Private Methods

        private Casting PopularCasting(long idCliente, long idEvento, HttpPostedFileBase imagem, long? idAgencia = default(long?), long? idCasting = null)
        {
            var cliente = _clienteApplication.Obter((Int32)idCliente);
            var evento = _eventoApplication.Obter((Int32)idEvento);
            Agencia agencia = null;
            Casting casting = null;

            if (idAgencia != null) agencia = _agenciaApplication.Obter((Int32)idAgencia);
            if (idCasting != null) casting = this.Obter((Int32)idCasting);

            else casting = new Casting();

            casting.StatusCasting = StatusCastingEnum.Concluido;
            casting.Cliente = cliente;
            casting.Evento = evento;
            casting.Agencia = agencia;
            casting.Foto = new Foto
            {
                Descricao = imagem.FileName,
                Tipo = imagem.ContentType,
                ImagemBinario = imagem.GetArrayBytes()
            };



            return casting;
        }




        #endregion
    }
}
