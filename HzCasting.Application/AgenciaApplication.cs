﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public sealed class AgenciaApplication : ApplicationBase, IAgenciaApplication
    {
        private readonly IAgenciaRepository _agenciaRepository;

        public AgenciaApplication(IAgenciaRepository agenciaRepository)
        {
            _agenciaRepository = agenciaRepository;
        }
        public void Atualizar(Agencia item)
        {
            BeginTransaction();
            _agenciaRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            var evento = this.Obter(id);
             
            _agenciaRepository.Delete(evento);
            Commit();
        }

        public IEnumerable<Agencia> Listar()
        {
            return _agenciaRepository.Get(); 
        }

        public IEnumerable<Agencia> Listar(String nome, String cnpj, String email)
        {
            return this.Obter(m => (nome != string.Empty ? m.Nome.ToLower().Contains(nome.ToLower()) : m.Nome != string.Empty) &&
                                   (cnpj != string.Empty ? m.CNPJ.ToLower().Contains(cnpj.ToLower()) : m.CNPJ != string.Empty) &&
                                   (email != string.Empty ? m.Email.ToLower().Contains(email.ToLower()) : m.Email != string.Empty));

        }

        public IEnumerable<Agencia> Obter(Func<Agencia, bool> query)
        {
            return _agenciaRepository.Get().Where(query);

        }

        public Agencia Obter(int id)
        {
            var agencia = _agenciaRepository.Get(id);
            if (agencia == null) throw new ApplicationException("agencia não encontrada");
            return agencia; 
        }

        public void Registrar(Agencia item)
        {
            BeginTransaction(); 
            _agenciaRepository.Add(item); 
            Commit();
        }
    }


}
