﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public class UsuarioClienteApplication : ApplicationBase, IUsuarioClienteApplication
    {
        private readonly IUsuarioClienteRepository _repository;
        private readonly IUsuarioRepository _userRepository;

        public UsuarioClienteApplication(IUsuarioClienteRepository repository, IUsuarioRepository userRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
        }

        public void Atualizar(UsuarioCliente item)
        {
            BeginTransaction();
            _repository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            throw new NotImplementedException();
        }

        public void Deletar(string id)
        {

            var usuarioCliente = Obter(x => x.IdUsuario == id).FirstOrDefault();

            if (usuarioCliente != null)
            {
                _repository.Delete(usuarioCliente);
            }

            try
            {
                _userRepository.Delete(id);
            }

            catch (Exception ex) { }
        }

        public IEnumerable<UsuarioCliente> Listar()
        {
            return _repository.Get();
        }

        public IEnumerable<UsuarioCliente> Obter(Func<UsuarioCliente, bool> query)
        {
            return _repository.Get().Where(query);
        }

        public UsuarioCliente Obter(int id)
        {
            throw new NotImplementedException();
        }

        public void Registrar(UsuarioCliente item)
        {
            BeginTransaction();
            _repository.Add(item);
            Commit();
        }
    }
}
