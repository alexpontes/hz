﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public class BackOfficeApplication : ApplicationBase, IBackOfficeApplication
    {
        private IBackOfficeRepository _repository;
        private IUsuarioRepository _userRepository;

        public BackOfficeApplication(IBackOfficeRepository repository, IUsuarioRepository userRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
        }

        public void Atualizar(BackOffice item)
        {
            BeginTransaction();
            _repository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            throw new NotImplementedException();
        }

        public void Deletar(string id)
        {
            
            var backoffice = Obter(x => x.IdUsuario == id).FirstOrDefault();

            if(backoffice != null)
            {
                _repository.Delete(backoffice);
            }

            try
            {
                _userRepository.Delete(id);
            }

            catch (Exception ex) { }
        }

        public IEnumerable<BackOffice> Listar()
        {
            return _repository.Get();
        }

        public IEnumerable<BackOffice> Obter(Func<BackOffice, bool> query)
        {
            return _repository.Get().Where(query);
        }

        public BackOffice Obter(int id)
        {
            return Obter(x => x.IdUsuario == id.ToString()).FirstOrDefault();
        }

        public void Registrar(BackOffice item)
        {
            BeginTransaction();
            _repository.Add(item);
            Commit();
        }
    }
}
