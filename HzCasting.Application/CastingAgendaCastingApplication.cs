﻿using HzCasting.Domain.Interfaces.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;

namespace HzCasting.Application
{
    public sealed class CastingAgendaCastingApplication : ApplicationBase, ICastingAgendaCastingApplication
    {
        private readonly ICastingAgendaCastingRepository _castingAgendaCastingRepository;
        public CastingAgendaCastingApplication(ICastingAgendaCastingRepository castingAgendaCastingRepository)
        {
            _castingAgendaCastingRepository = castingAgendaCastingRepository;
        }
        public void Atualizar(CastingAgendaCasting item)
        {
            BeginTransaction();
            _castingAgendaCastingRepository.Update(item);
            Commit();
        }

        public void Deletar(int id)
        {
            BeginTransaction();
            var item = this.Obter(id);
            _castingAgendaCastingRepository.Delete(item);
            Commit();
        }

        public IEnumerable<CastingAgendaCasting> Listar()
        {
            return _castingAgendaCastingRepository.GetComplete().ToList();
        }

        public IEnumerable<CastingAgendaCasting> Listar(String titulo, String cliente, String agencia, DateTime? dataFim, DateTime? dataInicio)
        {
            return this.Obter(m => (titulo != string.Empty ? m.Titulo.ToLower().Contains(titulo.ToLower()) : m.Titulo != string.Empty) &&
                                   (cliente != string.Empty ? m.Cliente.ToLower().Contains(cliente.ToLower()) : m.Cliente != string.Empty) &&
                                   (agencia != string.Empty ? m.Agencia.ToLower().Contains(agencia.ToLower()) : m.Agencia != string.Empty) &&
                                   (dataFim != null ? (m.DataFim.Date == dataFim && m.DataInicio.Date < dataFim) : m.DataFim.Date != DateTime.MinValue) &&
                                   (dataInicio != null ? (m.DataInicio.Date == dataInicio && m.DataFim.Date > dataInicio) : m.DataInicio.Date != DateTime.MinValue));

        }

        public IEnumerable<CastingAgendaCasting> Obter(Func<CastingAgendaCasting, bool> query)
        {
            return _castingAgendaCastingRepository.Get().Where(query).ToList();
        }

        public CastingAgendaCasting Obter(int id)
        {
            return _castingAgendaCastingRepository.Get(id);
        }

        public void Registrar(CastingAgendaCasting item)
        {
            BeginTransaction();
            _castingAgendaCastingRepository.Add(item);
            Commit();
        }
    }
}
