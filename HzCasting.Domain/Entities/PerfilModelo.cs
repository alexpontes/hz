﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public class PerfilModelo : Base.Base
    {
        public PerfilModelo()
        {
            //CorCabelo = new CorCabelo();
        }
        public CorPeleEnum CorPele { get; set; }
        public CorOlhosEnum CorOlhos { get; set; }
        public CorCabeloEnum CorCabelo { get; set; }
        public Decimal Peso { get; set; }
        public Decimal Altura { get; set; }
        public Decimal Manequim { get; set; }
        public Decimal Cintura { get; set; }
        public Decimal Quadril { get; set; }
        public Decimal Busto { get; set; }
        public Int32 Calcado { get; set; }
        
        public String Outros { get; set; }


    }
}
