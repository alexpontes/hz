﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public class ModeloIdioma
    {
        [Column(Order = 0), Key, ForeignKey("Modelo")]
        public Int64 ModeloId { get; set; }

        [Column(Order = 1), Key, ForeignKey("Idioma")]
        public Int64 IdiomaId { get; set; }
         
        public virtual Modelo Modelo { get; set; }
        public virtual Idioma Idioma { get; set; }
    }
}
