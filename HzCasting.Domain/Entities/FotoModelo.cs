﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
   public class FotoModelo : Base.Base
    {
        public String Tipo { get; set; }
        public Byte[] ImagemBinario { get; set; }
        public String Descricao { get; set; }

        private Boolean? _principal;
        public Boolean? Principal
        {
            get { return _principal ?? false; }
            set { _principal = value; }
        }

        private StatusFotoModeloEnum? _status;

        public StatusFotoModeloEnum? Status
        {
            get { return _status ?? StatusFotoModeloEnum.Pendente; }
            set { _status = value; }
        }
    }
}
