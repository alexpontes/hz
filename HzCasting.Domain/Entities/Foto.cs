﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public sealed  class Foto : Base.Base
    {
        public String Tipo { get; set; }
        public Byte[] ImagemBinario { get; set; } 
        public String Descricao { get; set; }
        

        
    }
}
