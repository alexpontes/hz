﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HzCasting.Domain.Entities
{
    public class TokenDevice : Base.Base
    {
        [ForeignKey("Usuario")]
        public string UserId { get; set; }
        public string Token { get; set; }

        public Usuario Usuario { get; set; }
    }
}
