﻿using System;
using System.ComponentModel;

namespace HzCasting.Domain.Entities
{
    public enum UfEnum
    {
        AC = 1,
        AL,
        AP,
        AM,
        BA,
        CE,
        DF,
        ES,
        GO,
        MA,
        MT,
        MS,
        MG,
        PA,
        PB,
        PR,
        PE,
        PI,
        RJ,
        RN,
        RS,
        RO,
        RR,
        SC,
        SP,
        SE,
        TO

    }

    public enum SexoEnum
    {
        Masculino = 1,
        Feminino = 2
    }

    public enum EstadoCivilEnum 
    {
        Casado = 1,
        Solteiro = 2,
        Divorciado = 3,
        Viuvo = 4
    }


    public enum StatusModeloEnum
    {
        Aprovada = 1,
        Standby,
        Reprovada
    }

    public enum StatusModeloCastingEnum
    {
        EmAvaliacao = 1,
        Aprovada ,
        Standby,
        Reprovada
    }

    public enum PeriodoEnum
    {
        Todos = 1,
        Futuro,
        Passado
    }
    public enum PerfilAcessoEnum
    {
        Agencia = 1,
        Admin,
        Modelo,
        Cliente
    }

    public enum TipoContaEnum
    {
        Corrente = 1,
        Poupanca
    }

    public enum CorPeleEnum
    {
        Branco = 1,
        Moreno,
        Negro,
        Oriental,
        Pardo
    }

    public enum CorCabeloEnum
    {
        Castanho = 1,
        Loiro,
        Ruivo,
        Preto,
        Grisalho,
        Outros
    }

    public enum CorOlhosEnum
    {
        Azul = 1,
        Castanho,
        Preto,
        Mel,
        Verde,
        Indefinido
    }

    public enum TipoModeloEnum
    {
        Fotografo = 1,
        Publicidade,
        Moda,
        Fashion,
        Comercial,
        EventosCorporativoSocial,
        Tv,
        Infanil,
        AtorAtriz,
        CoordenadorSupervisor,
        PDV
    }

    public enum CategoriaModeloEnum
    {
        VIP = 1,
        Nivel_A,
        Nivel_B,
        Nivel_AB,
        PDV,
        Indefinida
    }
    public enum StatusCastingEnum
    {
        Pendente = 0,
        [Description("Concluído")]
        Concluido = 1,
        Cancelado = 2
    }
    public enum TipoSelecaoEnum
    {
        Presencial = 1,
        Foto
    }
    
    public enum StatusTrackInviteEnum
    {
        Pendente = 1,
        Enviado,
        Aceito,
        Rejeitado,
        Cancelado

    }
    public enum StatusFotoModeloEnum
    {
        Pendente = 1,
        Aceito,
        Rejeitado

    }

    public enum EscolaridadeEnum
    {
        Medio = 1,
        Superior,
        Tecnico,
        MBA,
        Doutorado,
        Mestrado
    }
    public enum Realizacao
    {
        Passado,
        Futuro
    }

}