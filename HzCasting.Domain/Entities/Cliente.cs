﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public  class Cliente : Base.Base
    {
        
        public String RazaoSocial { get; set; }
        public String NomeFantasia { get; set; }
        public String CPNJ { get; set; }
        public virtual Endereco Endereco { get; set; }

        [NotMapped]
        public List<Usuario> Usuarios { get; set; }
    }
}
