﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
  public  sealed class UsuarioFacebook
    {
        public String Name { get; set; }
        public String ImageURL { get; set; }
        public Int32 Id { get; set; }
    }
}
