﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities.Base
{
    public abstract class Base
    {
        public Int64 Id { get; set; }
        private Boolean? _ativo;
        public Boolean? Ativo
        {
            get { return _ativo ?? true; }
            set { _ativo = value; }
        }

        private DateTime? _dataInclusao;
        public DateTime? DataInclusao
        {
            get { return _dataInclusao ?? DateTime.Now; }
            set { _dataInclusao = value; }
        }

    }
}
