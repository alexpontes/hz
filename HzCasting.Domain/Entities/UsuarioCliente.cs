﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public class UsuarioCliente : Base.Base
    {
        public string PrimeiroNome { get; set; }
        public string UltimoNome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string DadosPessoais_Telefone { get; set; }
        public string DadosPessoais_Celular { get; set; }
        public string DadosPessoais_Email { get; set; }
        public string CpfNumero { get; set; }        
        public string IdUsuario { get; set; }
        public int ClienteId { get; set; }
    }
}
