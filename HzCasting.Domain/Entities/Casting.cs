﻿using System;
using System.Collections.Generic;

namespace HzCasting.Domain.Entities
{
    public class Casting : Base.Base
    {
        public StatusCastingEnum StatusCasting { get; set; }

        public virtual IList<PerfilFiltro> PerfisFiltros { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual Evento Evento { get; set; }

        public virtual Agencia Agencia { get; set; }

        public String Titulo { get; set; }

        public virtual IList<AgendaCasting> Agendas { get; set; }

        public virtual Foto Foto { get; set; }

        public virtual IList<Modelo> Modelos { get; set; }

        public bool CriadoCliente { get; set; }

    }
}
