﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public  class Evento : Base.Base
    {
        
        public String Titulo { get; set; }

        public String Local { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }

        public virtual Endereco Endereco { get; set; }
        public String Obs { get; set; }
        
        public virtual Foto Foto { get; set; }
    }
}
