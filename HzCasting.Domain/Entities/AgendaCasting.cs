﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public class AgendaCasting : Base.Base
    {
        public AgendaCasting()
        { 

        }

        public virtual IList<PerfilFiltro> PerfilFiltros { get; set; }         
        public virtual Endereco Endereco { get; set; }
        public DateTime DataHoraInicio { get; set; }
        public Int32 QuantidadeCandidatos { get; set; }
        public DateTime DataHoraFim { get; set; }
        public Geolocation Geolocation { get; set; }
    }
}
