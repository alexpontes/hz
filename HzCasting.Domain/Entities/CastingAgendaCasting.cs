﻿using System;
using System.Collections.Generic;

namespace HzCasting.Domain.Entities
{
    public class CastingAgendaCasting : Base.Base
    {
        public CastingAgendaCasting()
        {
 
        }
        public virtual IList<AgendaCasting> AgendaCasting { get; set; }
        public virtual Casting Casting { get; set; }
             


        public String Titulo { get; set; }
        public String Cliente { get; set; }
        public String Agencia { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }

    }
}