﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HzCasting.Domain.Entities
{
    public class Token : Base.Base
    {
        [ForeignKey("Usuario")]
        public string UserId { get; set; }
        public string AuthToken { get; set; }
        public DateTime IssuedOn { get; set; }
        public DateTime ExpiresOn { get; set; }

        public Usuario Usuario { get; set; }
    }
}
