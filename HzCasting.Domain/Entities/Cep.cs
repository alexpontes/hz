﻿using System;

namespace HzCasting.Domain.Entities
{
    public class Cep
    {
        public String CepNumero { get; set; }
        public UfEnum UF
        {
            get;
            set;
        }
        public String Logradouro { get; set; }
        public String Bairro { get; set; }
        public String Zona { get; set; }
        public String GDE { get; set; }
        public String Cidade { get; set; }
        public String Estado { get; set; }
     
         

    }
}