﻿using System;
using System.Collections.Generic;

namespace HzCasting.Domain.Entities
{
    public class PerfilFiltro : Base.Base
    {

        public virtual IList<AgendaCasting> AgendasCasting { get; set; }
        public virtual IList<Booking> Bookings { get; set; }

        public virtual Casting Casting { get; set; }
        public TipoSelecaoEnum TipoSelecao { get; set; }
        public String Objetivo { get; set; }
        
        public String Titulo { get; set; }

        public Int32? QuantidadeCandidato { get; set; }

        public Int32? IdadeMin { get; set; }
        public Int32? IdadeMax { get; set; }

        public Int32? PesoMin { get; set; }
        public Int32? PesoMax { get; set; }

        public Decimal? AlturaMin { get; set; }
        public Decimal? AlturaMax { get; set; }

        public Int32? ManequimMin { get; set; }
        public Int32? ManequimMax { get; set; }

        public Int32? CinturaMin { get; set; }
        public Int32? CinturaMax { get; set; }

        public Int32? QuadrilMin { get; set; }
        public Int32? QuadrilMax { get; set; }

        public Int32? BustoMin { get; set; }
        public Int32? BustoMax { get; set; }

        public Int32? CalcadoMin { get; set; }
        public Int32? CalcadoMax { get; set; }

        public Boolean Tatuagem { get; set; }

        public CorPeleEnum CorPele { get; set; }
        public CorCabeloEnum CorCabelo { get; set; }
        public CorOlhosEnum CorOlhos { get; set; }
    }
}