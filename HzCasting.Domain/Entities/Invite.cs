﻿using System;

namespace HzCasting.Domain.Entities
{
    public class Invite : Base.Base
    {
        public Invite()
        {
       
        }        
        public DateTime Validade { get; set; }
        public DateTime? DataEnvio { get; set; }
        public DateTime? DataRetorno { get; set; }
        public virtual Booking Booking { get; set; }

        //public Int32 BookingId { get; set; }
        public StatusTrackInviteEnum StatusTrackInvite { get; set; }
    }
}