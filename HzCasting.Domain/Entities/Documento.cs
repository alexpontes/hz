﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
 public    sealed class Documento 
    {
        public Documento()
        {
            Cpf = new Cpf();
        }
        public String Rg { get; set; }
        public String Pis { get; set; } 
        public Cpf Cpf { get; set; }

        public String CarteiraProfissional { get; set; } 
        public String Passaporte { get; set; }
    }
}
