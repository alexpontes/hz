﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public sealed class Escolaridade
    {
        public String Formacao { get; set; }
        public String Tecnico { get; set; }
        public String Superior { get; set; }

      

    }
}
