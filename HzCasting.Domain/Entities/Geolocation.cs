﻿namespace HzCasting.Domain.Entities
{
    public sealed class Geolocation : Base.Base
    { 
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}