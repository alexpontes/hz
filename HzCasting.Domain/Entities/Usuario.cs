﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    [Table("Usuarios")]
    public class Usuario : IdentityUser
    {
        [Required]
        public string CPF { get; set; }

        public string FacebookEmail { get; set; }

        public DateTime DataCriacao { get { return DateTime.Now; } }

        public DateTime? DataAtualizacao { get; set; }

        [NotMapped]
        public string Senha { get; set; }

        [NotMapped]
        public int Perfild { get; set; }

        public string FacebookToken { get; set; }

        public string MobileKey { get; set; }
        
        public long? ClienteId { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Usuario> manager)
        {
            this.SecurityStamp = Guid.NewGuid().ToString();
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }


    }
}
