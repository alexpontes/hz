﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public sealed class Tatuagem : Base.Base
    {
        public String Descricao { get; set; }
        public String ParteCorpo { get; set; }
    }
}
