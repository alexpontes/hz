﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
   public sealed class AgendaCastingModelo : Base.Base
    {
        public AgendaCastingModelo()
        {
        }
        public Modelo Modelo { get; set; }
        public AgendaCasting AgendaCasting { get; set; }
        public Invite Invite { get; set; }
    }
}
