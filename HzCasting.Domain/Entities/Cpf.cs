﻿using System;

namespace HzCasting.Domain.Entities
{
    public sealed class Cpf 
    {
        public String Numero { get; set; }
        public Boolean IsValido { get; set; }

    }
}