﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public sealed class DadosPessoais
    {
        public String PrimeiroNome { get; set; }
        public String UltimoNome { get; set; }
        public DateTime? DataNascimento { get; set; }
        public EstadoCivilEnum EstadoCivil { get; set; }
        public SexoEnum Sexo { get; set; }
        public String Telefone { get; set; }
        public String Celular { get; set; }
        public String Email { get; set; } 
    }
}
