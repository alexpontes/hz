﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HzCasting.Domain.Entities
{
    public class Booking : Base.Base
    {
        public Booking()
        {
        }        

        public  AgendaCasting AgendaCasting { get; set; }
        public StatusModeloCastingEnum StatusModeloCastingEnum { get; set; }
        public TipoSelecaoEnum TipoSelecao { get; set; }
        public virtual PerfilFiltro PerfilFiltro { get; set; } 

        public virtual Modelo Modelo { get; set; }
         

    }
}