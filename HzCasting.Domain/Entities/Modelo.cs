﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HzCasting.Domain.Entities
{
    public class Modelo : Base.Base
    {
        public Modelo()
        {
            DadosPessoais = new DadosPessoais();
            Documento = new Documento();

        }
        public virtual DadosPessoais DadosPessoais { get; set; }

        public virtual Documento Documento { get; set; }
         
        public virtual Banco Banco { get; set; }

        
      //  public virtual Escolaridade Escolaridade { get; set; }
         
        public virtual Idioma Idioma { get; set; }
         
        public virtual PerfilModelo PerfilModelo { get; set; }

        
        public virtual Tatuagem Tatuagem { get; set; }
         
        public virtual IList<FotoModelo> Fotos { get; set; }

        public virtual ICollection<ModeloIdioma> Idiomas { get; set; }
        
        //public Booking Booking { get; set; }

        public DateTime? DataAprovacao { get; set; }
         
        public virtual Endereco Endereco { get; set; }

        public Boolean DisponibilidadeHorario { get; set; }
        public Boolean DisponibilidadeViagem { get; set; }
        public Boolean PossuiVeiculo { get; set; }

        public StatusModeloEnum StatusModelo { get; set; }
        public TipoModeloEnum TipoModelo { get; set; }
        public CategoriaModeloEnum CategoriaModelo { get; set; }

        public EscolaridadeEnum Escolaridade { get; set; }

        public String IdUsuario { get; set; }

        public virtual IList<Casting> Castings { get; set; }
        public virtual IList<Booking> Bookings { get; set; }
    }
}
