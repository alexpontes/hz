﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public class Agencia : Base.Base
    {
        public Agencia()
        {
        }
        public String Nome { get; set; }
        public String CNPJ { get; set; }
        public virtual Endereco Endereco { get; set; }
        public String Telefone { get; set; }
        public String Celular { get; set; }
        public String Email { get; set; }
    }
}
