﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public sealed  class Banco : Base.Base
    {
        public String Nome { get; set; }
        public String Agencia { get; set; }
        public String Conta { get; set; }
        public TipoContaEnum TipoConta { get; set; }

    }
}
