﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Entities
{
    public class EnumModel
    {
        public string Value { get; set; }
        public string Name { get; set; }
    }
}
