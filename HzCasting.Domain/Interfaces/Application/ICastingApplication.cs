﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface ICastingApplication : IBaseApplication<Casting>
    {
        IEnumerable<Casting> Listar(String titulo, String cliente, String agencia, DateTime? dataFim, DateTime? dataInicio, Int32? statusId, bool? criadoCliente);
        IEnumerable<Modelo> ListarModelosCasting(Int64 castingId);
        void Registrar(Int64 idCliente, Int64 idEvento, HttpPostedFileBase imagem, Int64? idAgencia = null);
        void Atualizar(Int64 idCliente, Int64 idEvento, HttpPostedFileBase imagem, Int64? idAgencia = null, Int64? idCasting = null);
    }
}
