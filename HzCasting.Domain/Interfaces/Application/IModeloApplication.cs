﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface IModeloApplication : IBaseApplication<Modelo>
    {
        Task Registrar(Modelo item, Usuario usuario);
        Task RegistrarAsync(Modelo item);
       
        bool VerificarSelecaoPerfilCasting(long IdModelo, long IdPerfilCasting);
        void AtualizarContato(Modelo item);
        void AtualizarDocumento(Modelo item);
        void AtualizarDadosBancarios(Modelo item);
        void AtualizarEscolaridade(Modelo item);
        void AtualizarIdioma(Modelo item);
        void AtualizarCaracteristicas(Modelo item);
        void AtualizarDadosComplementares(Modelo item);
    
        decimal CalcularPercentualCadastro(String idUsuario); 
        void AtualizarDadosPereciveis(Int32 idModelo, PerfilModelo modelo);
        void InserirImagens(String usuarioId, IList<HttpPostedFileBase> imagens);
        IEnumerable<Modelo> ObterModelosPerfilFiltro(Int32 perfilId, Int32 castingId, out String nomePerfil);
        void AdicionarModeloAoCasting(int castingId, int idModelo);
        void RemoverFoto(String usuarioId, Int64 idFoto);
        void AlterarFotoPrincipal(String usuarioId, Int64 idFoto);
        void AprovarFoto(String usuarioId, Int64 idFoto);


        IEnumerable<Modelo> Listar(String nome, String rg, String cpf, DateTime? dataNascimento);


    }
}
