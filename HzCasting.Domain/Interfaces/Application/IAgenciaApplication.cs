﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface IAgenciaApplication : IBaseApplication<Agencia>
    {
        IEnumerable<Agencia> Listar(String nome, String cnpj, String email);

    }
}
