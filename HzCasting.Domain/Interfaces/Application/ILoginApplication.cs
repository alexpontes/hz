﻿using HzCasting.Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
  public interface ILoginApplication
    {
        Task<IdentityResult> CadastrarUsuarioAsync(Usuario model);
        Task<object> LogarUserAsync(Usuario user);
        Task ResetPassword(string email);
        Task VincularRole(Usuario model);
        bool ValidateApiUser(string name, string password);
    }
}