﻿
namespace HzCasting.Domain.Interfaces.Application
{   
    public interface IApiLoginApplication
    {
        bool ValidateApiUser(string userName, string password);
        void ForgotPassword(string email);
    }
}
