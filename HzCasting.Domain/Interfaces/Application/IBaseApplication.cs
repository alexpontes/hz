﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface IBaseApplication<T>
    {
        void Registrar(T item);
        IEnumerable<T> Listar();
        T Obter(Int32 id);
        IEnumerable<T> Obter(Func<T, Boolean> query);
        void Atualizar(T item);
        void Deletar(int id);
    }
}
