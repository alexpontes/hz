﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface IBookingApplication : IBaseApplication<Booking>
    {
        void Registrar(Int32 castingId, Int32 idModelo, Int32 perfilId);
    }
}
