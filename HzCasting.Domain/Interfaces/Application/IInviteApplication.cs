﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface IInviteApplication : IBaseApplication<Invite>
    {

        IEnumerable<Invite> ListarInvitesModelo(Int64 id, StatusTrackInviteEnum? status);
        void RegistrarInvites(Int32 castingId);
        void Registrar(IEnumerable<Invite> invites);
        //void RegistrarInviteModelo(Int32 castingId, Int32 modeloId);
        void RegistrarInvites(IList<Invite> invites);
    }
}
