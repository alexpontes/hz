﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface IEventoApplication : IBaseApplication<Evento>
    {
        IEnumerable<Evento> Listar(String titulo, DateTime? dataFim, DateTime? dataInicio, Boolean passado, Boolean futuro);
    }
}
