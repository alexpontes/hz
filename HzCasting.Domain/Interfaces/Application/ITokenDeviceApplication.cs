﻿
namespace HzCasting.Domain.Interfaces.Application
{
    public interface ITokenDeviceApplication
    {
        void Register(string token, string userId);
        void Kill(string token);
    }
}
