﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
   public interface IClienteApplication : IBaseApplication<Cliente>
    {
        IEnumerable<Cliente> Listar(String RazaoSocial, String NomeFantasia, String CPNJ);

		bool VerificarUsuarioExistente(Cliente cliente);
	}
}
