﻿using HzCasting.Domain.Entities;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface ITokenApplication
    {
        #region Interface member methods.
        /// <summary>
        ///  Gerar token com expiração.
        ///  Grava na na tabela token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Token GenerateToken(string userId);

        /// <summary>
        /// Valida o token atualiza a expiração e a base de dados.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        bool ValidateToken(string token);

        /// <summary>
        /// Apaga o token
        /// </summary>
        /// <param name="tokenId"></param>
        void Kill(string token);

        /// <summary>
        /// Apaga o token do usuario
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        void DeleteByUserId(string userId);
        #endregion
    }
}
