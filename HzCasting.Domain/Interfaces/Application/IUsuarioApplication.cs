﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Application
{
    public interface IUsuarioApplication : IBaseApplication<Usuario>
    {
        Usuario VerificarUsuarioExistente(Usuario usuario);
        Task<List<UsuarioFacebook>> ObterFotosUsuarioAsync(string fbTOken, int take = 10, int skip = 0);
        void Deletar(string id);
        IEnumerable<Usuario> GetWithRoles();
        void AssociarFacebook(Usuario obj);

        IEnumerable<Usuario> Listar(String cpf, String email);
        Usuario GetUserByEmail(string email);
    }
}