﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces
{
    public interface IClienteRepository : IRepositoryBase<Cliente>
    {

    }
}
