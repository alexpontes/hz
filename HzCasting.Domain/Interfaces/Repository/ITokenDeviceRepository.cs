﻿using HzCasting.Domain.Entities;

namespace HzCasting.Domain.Interfaces.Repository
{
    public interface ITokenDeviceRepository : IRepositoryBase<TokenDevice>
    {
        void Kill(string token);
        TokenDevice GetByToken(string token);
    }
}
