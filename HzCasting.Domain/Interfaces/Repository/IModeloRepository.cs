﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces
{
    public interface IModeloRepository : IRepositoryBase<Modelo> 
    {
        bool VerificarSelecaoPerfil(long IdModelo, long IdPerfilCasting);
        void AtualizarContato(Modelo item);
        void AtualizarDocumento(Modelo item);
        void AtualizarDadosBancarios(Modelo item);
        void AtualizarEscolaridade(Modelo item);
        void AtualizarIdioma(Modelo item);
        void AtualizarCaracteristicas(Modelo item);
        void AtualizarDadosComplementares(Modelo item);
    }
}
