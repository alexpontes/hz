﻿using HzCasting.Domain.Entities;

namespace HzCasting.Domain.Interfaces.Repository
{
    public interface ITokenRepository : IRepositoryBase<Token>
    {
        void DeleteByUserId(string userId);
        void DeleteByToken(string token);
        Token GetByToken(string token);
    }
}