﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Domain.Interfaces.Repository
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
        void Delete(string id);
        IEnumerable<Usuario> GetWithRoles();
        void AssociarFacebook(Usuario obj);
        Usuario GetUserByEmail(string email);
    }
}