﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces;
using HzCasting.Infra.Repositories.EF.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories
{
    public class ModeloRepository : RepositoryBase<Modelo>, IModeloRepository
    {
        public bool VerificarSelecaoPerfil(long IdModelo, long IdPerfilCasting)
        {
            //todo: VERIFICAR
            throw new NotImplementedException("Metodo refatorado..");
            //return Context.Set<Booking>().Where(i => i.PerfilCasting.Id == IdPerfilCasting 
            //                                                  && i.Modelo.Id == IdModelo).ToList().Count > 0;
        }

        public void AtualizarContato(Modelo modelo)
        {
            HzCastingDbContext hzContext = new HzCastingDbContext();

            var model = hzContext.Modelos.Include("Endereco").FirstOrDefault(x => x.Id == modelo.Id);

            model.DadosPessoais.Celular = modelo.DadosPessoais.Celular;
            model.DadosPessoais.Telefone = modelo.DadosPessoais.Telefone;

            model.Endereco = hzContext.Enderecos.FirstOrDefault(x => x.Id == model.Endereco.Id);
            var entry = hzContext.Entry(model);
            var enderecoEntry = hzContext.Entry(model.Endereco);
            enderecoEntry.ComplexProperty(x => x.Cep).IsModified = false;

            entry.Property(x => x.DadosPessoais.Celular).IsModified = true;
            entry.Property(x => x.DadosPessoais.Telefone).IsModified = true;

            hzContext.SaveChanges();



        }

        public void AtualizarDocumento(Modelo item)
        {
            HzCastingDbContext hzContext = new HzCastingDbContext();

            var modelo = hzContext.Modelos.Include("Endereco").FirstOrDefault(x => x.Id == item.Id);

            modelo.Documento.Cpf = new Cpf { Numero = item?.Documento.Cpf.Numero, IsValido = true };
            modelo.Documento.CarteiraProfissional = item?.Documento.CarteiraProfissional;
            modelo.Documento.Pis = item?.Documento.Pis;
            modelo.Documento.Rg = item?.Documento.Rg;

            modelo.Endereco = hzContext.Enderecos.FirstOrDefault(x => x.Id == modelo.Endereco.Id);

            var entry = hzContext.Entry(modelo);
            var enderecoEntry = hzContext.Entry(modelo.Endereco);

            enderecoEntry.ComplexProperty(x => x.Cep).IsModified = false;

            hzContext.SaveChanges();

        }
        public void AtualizarDadosBancarios(Modelo item)
        {

            HzCastingDbContext hzContext = new HzCastingDbContext();

            var modelo = hzContext.Modelos.Include("Endereco").FirstOrDefault(x => x.Id == item.Id);

            modelo.Banco = new Banco
            {
                Agencia = item?.Banco.Agencia,
                Conta = item?.Banco.Conta,
                Nome = item?.Banco.Nome,
                TipoConta = (TipoContaEnum)item?.Banco.TipoConta
            };

            hzContext.SaveChanges();

        }

        public void AtualizarEscolaridade(Modelo item)
        {

            HzCastingDbContext hzContext = new HzCastingDbContext();

            var modelo = hzContext.Modelos.Include("Endereco").FirstOrDefault(x => x.Id == item.Id);

            modelo.Escolaridade = item.Escolaridade;

            hzContext.SaveChanges();

        }

        public void AtualizarIdioma(Modelo item)
        {

            HzCastingDbContext hzContext = new HzCastingDbContext();

            var modelo = hzContext.Modelos.Include("Endereco").FirstOrDefault(x => x.Id == item.Id);

            modelo.Idioma = new Idioma
            {
                Descricao = item?.Idioma.Descricao
            };

            hzContext.SaveChanges();
        }

        public void AtualizarCaracteristicas(Modelo item)
        {

            HzCastingDbContext hzContext = new HzCastingDbContext();

            var modelo = hzContext.Modelos.Include("Endereco").FirstOrDefault(x => x.Id == item.Id);

            modelo.PerfilModelo = new PerfilModelo
            {
                Altura = item.PerfilModelo.Altura,
                Busto = item.PerfilModelo.Busto,
                Calcado = item.PerfilModelo.Calcado,
                Cintura = item.PerfilModelo.Cintura,
                CorCabelo = (CorCabeloEnum)(item?.PerfilModelo.CorCabelo ?? 0),
				CorOlhos = (CorOlhosEnum)(item?.PerfilModelo.CorOlhos ?? 0),
                CorPele = (CorPeleEnum)(item.PerfilModelo.CorPele),
                Manequim = item.PerfilModelo.Manequim,
                Outros = item.PerfilModelo?.Outros,
                Peso = item.PerfilModelo?.Peso ?? 0,
                Quadril = item.PerfilModelo?.Quadril ?? 0
            };

            modelo.CategoriaModelo = (CategoriaModeloEnum)(item?.CategoriaModelo ?? 0);
            modelo.TipoModelo = (TipoModeloEnum)item?.TipoModelo;
            modelo.DataAprovacao = item?.DataAprovacao == DateTime.MinValue ? DateTime.Now : item?.DataAprovacao;

            hzContext.SaveChanges();

        }

        public void AtualizarDadosComplementares(Modelo item)
        {
            HzCastingDbContext hzContext = new HzCastingDbContext();

            var modelo = hzContext.Modelos.Include("Endereco").FirstOrDefault(x => x.Id == item.Id);

            modelo.DisponibilidadeHorario = (item?.DisponibilidadeHorario ?? false);
            modelo.DisponibilidadeViagem = (item?.DisponibilidadeViagem ?? false);
            modelo.PossuiVeiculo = (item?.PossuiVeiculo ?? false);

            hzContext.SaveChanges();
        }
    }




}

