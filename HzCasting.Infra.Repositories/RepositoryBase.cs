﻿using HzCasting.Domain.Interfaces;
using HzCasting.Infra.Repositories.EF;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities.Base;
namespace HzCasting.Infra.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : Base
    {
        protected DbContext Context
        {
            get;
            private set;
        }

        public RepositoryBase()
        {
            var contextManager = ServiceLocator.Current.GetInstance<ContextManager>();
            Context = contextManager.Context;
        }

        public void Add(TEntity obj)
        { 
            Context.Set<TEntity>().Add(obj);
        }

        public void Delete(TEntity obj)
        {
            obj.Ativo = false;
            this.Update(obj);
        }

        public void Delete(int id)
        {
            var obj = Get(id);
            Delete(obj);
        }

        public TEntity Get(int id)
        {
            return Context.Set<TEntity>().Where(m => (bool)m.Ativo).FirstOrDefault(m => m.Id == id);
        }
        public IEnumerable<TEntity> Get()
        {
            return Context.Set<TEntity>().Where(m => (bool)m.Ativo).ToList();
        }
        public void Update(TEntity obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
        }


    }

}
