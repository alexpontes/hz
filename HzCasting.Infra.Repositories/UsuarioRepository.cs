﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces;
using HzCasting.Domain.Interfaces.Repository;
using HzCasting.Infra.Repositories.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Infra.Repositories.EF.Context;

namespace HzCasting.Infra.Repositories
{
    public sealed class UsuarioRepository : IRepositoryBase<Usuario>, IUsuarioRepository
    {
        public void Add(Usuario obj)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Users.Add(obj);
                db.SaveChanges();
            }

        }

        public void Delete(int id)
        {
            var obj = Get(id);
            Delete(obj);
        }

        public void Delete(string id)
        {
            using (var db = new ApplicationDbContext())
            {
                var obj = db.Users.Find(id);
                db.Users.Remove(obj);
                db.SaveChanges();
            }
        }

        public void Delete(Usuario obj)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Users.Remove(obj);
                db.SaveChanges();
            }
        }

        public IEnumerable<Usuario> Get()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users.ToList();

            }
        }

        public Usuario Get(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users.Find(id);
            }
        }

        public IEnumerable<Usuario> GetWithRoles()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users.Include(x => x.Roles).ToList();
            }
        }

        public void Update(Usuario obj)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
            }

        }

        public void AssociarFacebook(Usuario obj)
        {
            using (var db = new ApplicationDbContext())
            {

                db.Users.Attach(obj);
                var entry = db.Entry(obj);

                foreach (var item in entry.OriginalValues.PropertyNames)
                {
                    entry.Property(item).IsModified = false;
                }

                entry.Property(x => x.FacebookEmail).IsModified = true;
                entry.Property(x => x.FacebookToken).IsModified = true;

                entry.State = EntityState.Modified;
                db.SaveChanges();
            }

        }

        public Usuario GetUserByEmail(string email)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users.FirstOrDefault(user => user.Email == email);
            }
        }
    }
}