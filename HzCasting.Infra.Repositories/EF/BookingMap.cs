﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories
{
    public class BookingMap : EntityTypeConfiguration<Booking>
    {
        public BookingMap()
        {
            HasMany(m => m.Modelos)
              .WithMany(m => m.Bookings)
              .Map(m =>
              {
                  m.MapLeftKey("ModeloId");
                  m.MapRightKey("BookingId");
                  m.ToTable("ModeloBooking");
              });

            HasMany(m => m.PerfilsCastings)
              .WithMany(m => m.Bookings)
              .Map(m =>
              {
                  m.MapLeftKey("PerfilCastingId");
                  m.MapRightKey("BookingId");
                  m.ToTable("PerfilCastingBooking");
              });
        }

    }
}
