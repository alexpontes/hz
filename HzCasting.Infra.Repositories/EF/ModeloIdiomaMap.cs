﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories.EF
{
    public sealed class ModeloIdiomaMap : EntityTypeConfiguration<ModeloIdioma>
    {
        public ModeloIdiomaMap()
        {
            HasRequired(x => x.Modelo)
                .WithMany(x => x.Idiomas) 
                .HasForeignKey(x=> x.ModeloId);
          
        }
    }
}
