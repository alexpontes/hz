﻿//using System;
//using System.Collections.Generic;
//using System.Data.Entity.ModelConfiguration;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using HzCasting.Domain.Entities;

//namespace HzCasting.Infra.Repositories.EF
//{
//  public sealed  class PerfilModeloMap : EntityTypeConfiguration<PerfilModelo>
//    {
//      public PerfilModeloMap()
//      {
//          HasKey(x => x.Id);

//          Property(x => x.Altura).IsOptional();
//          Property(x => x.Peso).IsOptional();
//          Property(x => x.Manequim).IsOptional().HasMaxLength(100);
//          Property(x => x.Cintura).IsOptional();
//          Property(x => x.Quadril).IsOptional();
//          Property(x => x.Busto).IsOptional();
//          Property(x => x.Calcado).IsOptional();
//          Property(x => x.Outros).IsOptional().HasMaxLength(100);
//          Property(x => x.CorOlhos).IsOptional();
//          Property(x => x.CorPele).IsOptional();

//          HasOptional(m => m.CorCabelo)
//              .WithMany()
//              .HasForeignKey(m => m.CorCabelo.Id);
//      }
//    }
//}
