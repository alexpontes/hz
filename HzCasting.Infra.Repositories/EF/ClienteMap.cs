﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories.EF
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            HasKey(x => x.Id);
            Property(x => x.RazaoSocial).IsRequired().HasMaxLength(120);
            Property(x => x.NomeFantasia).HasMaxLength(120);
            Property(x => x.CPNJ).IsRequired().HasMaxLength(14);         
        }
    }
}
