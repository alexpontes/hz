﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories.EF.Identity
{
 public sealed    class RoleMap : EntityTypeConfiguration<IdentityRole>
    {
        public RoleMap()
        {
           HasKey(r => r.Id);
        }
    }
}
