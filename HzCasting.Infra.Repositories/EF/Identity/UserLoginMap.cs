﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories.EF.Identity
{
   public sealed class UserLoginMap : EntityTypeConfiguration<IdentityUserLogin>
    {
        public UserLoginMap()
        { 

            HasKey(l => l.UserId);
        }
    }
}
