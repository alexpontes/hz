﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories.EF.Identity
{
   public sealed  class UsuarioMap : EntityTypeConfiguration<Usuario>
    {
        public UsuarioMap()
        {
            ToTable("Usuarios")
                .HasKey(m => m.Id);

            Property(m => m.UserName)
                .HasColumnName("Usuario");

            Property(m => m.PasswordHash)
                .HasColumnName("Senha") ;

            Property(m => m.FacebookToken)
                .IsOptional();

            Property(m => m.ClienteId)
                .IsOptional();
        }
    }
}
