﻿using HzCasting.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace HzCasting.Infra.Repositories.EF
{
    public class ModeloMap : EntityTypeConfiguration<Modelo>
    {
        public ModeloMap()
        {
            HasKey(x => x.Id);
            Property(x => x.DadosPessoais.PrimeiroNome)
                .IsOptional()
                .HasMaxLength(80)
                .HasColumnName("PrimeiroNome");

            Property(x => x.DadosPessoais.UltimoNome)
                .IsOptional()
                .HasMaxLength(80)
                .HasColumnName("UltimoNome");

            Property(x => x.DadosPessoais.DataNascimento)
                .HasColumnName("DataNascimento")
                .IsOptional();

            Property(x => x.DadosPessoais.EstadoCivil)
                .HasColumnName("EstadoCivil")
                .IsOptional();

            Property(x => x.DadosPessoais.Sexo)
                .HasColumnName("Sexo")
                .IsOptional();


            Property(x => x.Documento.Cpf.Numero)
                .HasColumnName("CpfNumero")
                .IsOptional().HasMaxLength(21);

            Property(x => x.Documento.Cpf.IsValido)
                .HasColumnName("CpfIsValido")
                .IsOptional();

            Property(x => x.Documento.Rg)
                .HasColumnName("Rg")
                .IsOptional().HasMaxLength(10);


            Property(x => x.Documento.Pis)
                .HasColumnName("Pis")
                .IsOptional().HasMaxLength(20);

            Property(x => x.Documento.CarteiraProfissional)
                .HasColumnName("CarteiraProfissional")
                .IsOptional().HasMaxLength(20);

            Property(x => x.Documento.Passaporte)
                .HasColumnName("Passaporte")
                .IsOptional().HasMaxLength(20);

            Property(x => x.StatusModelo)
                .IsOptional();

            Property(x => x.DataAprovacao).IsOptional();

            Property(x => x.IdUsuario).IsRequired();


            //HasOptional(x => x.Banco)
            //    .WithMany()
            //    .HasForeignKey(x => x.BancoId);

            //HasOptional(m => m.Idioma)
            //    .WithMany()
            //    .HasForeignKey(m => m.IdiomaId);

            //HasOptional(x => x.PerfilModelo)
            //    .WithMany()
            //    .HasForeignKey(x => x.PerfilModeloId);

            //HasOptional(m => m.Fotos)
            //    .WithMany()
            //    .HasForeignKey(m => m.FotoId);


            //HasOptional(m => m.Endereco)
            //    .WithMany()
            //    .HasForeignKey(m => m.EnderecoId);

            HasMany(s => s.Castings)
            .WithMany(c => c.Modelos)
            .Map(cs =>
            {
                cs.MapLeftKey("CastingId");
                cs.MapRightKey("ModeloId");
                cs.ToTable("CastingModelo");
            });
                
        }
    }
}
