﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HzCasting.Infra.Repositories.EF.Context;

namespace HzCasting.Infra.Repositories.EF
{
    public class ContextManager
    {
        private const string CONTEXTKEY = "CONTEXTMANAGER.CONTEXT";
        public DbContext Context
        {
            get
            {
                if (HttpContext.Current.Items[CONTEXTKEY] == null)
                {
                    HttpContext.Current.Items[CONTEXTKEY] = new HzCastingDbContext();
                }
                return (HzCastingDbContext)HttpContext.Current.Items[CONTEXTKEY];
            }


        }
    }
}