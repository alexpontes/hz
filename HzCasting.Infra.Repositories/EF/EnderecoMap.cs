﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories.EF
{
    public class EnderecoMap : EntityTypeConfiguration<Endereco>
    {
        public EnderecoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Numero)
               .HasColumnName("Numero");

            Property(x => x.Complemento)
              .HasColumnName("Complemento");

            Property(x => x.Cep.Bairro)
            .HasColumnName("Cep_Bairro");

            Property(x => x.Cep.CepNumero)
                .HasColumnName("Cep_CepNumero");

            Property(x => x.Cep.Cidade)
               .HasColumnName("Cep_Cidade");

            Property(x => x.Cep.Estado)
            .HasColumnName("Cep_Estado");

            Property(x => x.Cep.GDE)
            .HasColumnName("Cep_GDE");

            Property(x => x.Cep.Logradouro)
            .HasColumnName("Cep_Logradouro");
        }
    }
}
