﻿using HzCasting.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories.EF
{
    public sealed class FotoModeloMap : EntityTypeConfiguration<FotoModelo>
    {
        public FotoModeloMap()
        {
            ToTable("FotoModelo");
            HasKey(m => m.Id);

            Property(m => m.ImagemBinario)
                .IsOptional();

            Property(m => m.Tipo)
                .HasColumnName("TipoArquivo")
                .IsOptional();

            Property(m => m.Descricao)
                .HasColumnName("Descricao")
                .IsOptional();
             

        }
    }
}
