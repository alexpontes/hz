﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;

namespace HzCasting.Infra.Repositories.EF
{
    public sealed class CorCabeloMap : EntityTypeConfiguration<CorCabelo>
    {
        public CorCabeloMap()
        {
            HasKey(x => x.Id);
            Property(x => x.Descricao).IsOptional().HasMaxLength(20);

        }
    }
}
