﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;

namespace HzCasting.Infra.Repositories.EF
{
    public sealed class BancoMap : EntityTypeConfiguration<Banco>
    {
        public BancoMap()
        {
            HasKey(m => m.Id);

            Property(x => x.Agencia).IsOptional().HasMaxLength(100);
            Property(x => x.Conta).IsOptional().HasMaxLength(100);
            Property(x => x.Nome).IsOptional().HasMaxLength(100);
            Property(x => x.TipoConta).IsOptional();

        }
    }
}
