﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HzCasting.Domain.Entities;

namespace HzCasting.Infra.Repositories.EF
{
  public sealed  class IdiomaMap :  EntityTypeConfiguration<Idioma>
    {
      public IdiomaMap()
      {
          HasKey(x => x.Id);
          Property(x => x.Descricao).IsOptional().HasMaxLength(100);
      }
    }
}
