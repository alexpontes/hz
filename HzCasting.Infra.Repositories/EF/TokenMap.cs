﻿using HzCasting.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace HzCasting.Infra.Repositories.EF
{
    public sealed class TokenMap : EntityTypeConfiguration<Token>
    {
        public TokenMap()
        {
            HasKey(token => token.Id);
            Property(token => token.Ativo).IsRequired();
            Property(token => token.AuthToken).IsRequired().HasMaxLength(200);
            Property(token => token.DataInclusao).IsRequired();
            Property(token => token.ExpiresOn).IsRequired();
            Property(token => token.IssuedOn).IsRequired();
            Property(token => token.UserId);
        }
    }
}