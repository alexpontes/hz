﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using HzCasting.Domain.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;
using MySql.Data.Entity;

namespace HzCasting.Infra.Repositories.EF.Context
{
    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class HzCastingDbContext : DbContext
    {


        public HzCastingDbContext() : base("dbhzcasting")
        {
            var xxxx = this.Database.Connection;
        }
        public DbSet<Agencia> Agencias { get; set; }
        public DbSet<Modelo> Modelos { get; set; }
        public DbSet<Evento> Eventos { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<Banco> Bancos { get; set; }
        public DbSet<CorCabelo> CorCabelos { get; set; }
        public DbSet<Idioma> Idiomas { get; set; }
        public DbSet<PerfilModelo> PerfisModelos { get; set; }
        public DbSet<Tatuagem> Tatuagens { get; set; }
        public DbSet<Foto> Fotos { get; set; }
        public DbSet<FotoModelo> FotoModelo { get; set; }
        public DbSet<AgendaCasting> AgendasCasting { get; set; }
        public DbSet<AgendaCastingModelo> AgendasCastingModelos { get; set; }

        public DbSet<Casting> Castings { get; set; }
        public DbSet<CastingAgendaCasting> CastingAgendaCasting { get; set; }
        public DbSet<Geolocation> Geolocations { get; set; }
        public DbSet<Invite> Invites { get; set; }
        public DbSet<PerfilFiltro> PerfilFiltroCasting { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<BackOffice> BackOffices { get; set; }
        public DbSet<UsuarioCliente> UsuariosCliente { get; set; }
        public DbSet<ModeloIdioma> ModeloIdioma { get; set; }
        public DbSet<Token> Token { get; set; }
        public DbSet<TokenDevice> TokenDevice { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new ClienteMap());
            modelBuilder.Configurations.Add(new ModeloMap());

            modelBuilder.Configurations.Add(new BancoMap());
            modelBuilder.Configurations.Add(new IdiomaMap()); 
            modelBuilder.Configurations.Add(new TatuagemMap());
            modelBuilder.Configurations.Add(new FotoMap());
            modelBuilder.Configurations.Add(new FotoModeloMap()); 
            modelBuilder.Configurations.Add(new EnderecoMap());
            modelBuilder.Configurations.Add(new PerfilFiltroMap());
            modelBuilder.Configurations.Add(new ModeloIdiomaMap());
            modelBuilder.Configurations.Add(new TokenMap());
        }

    }
}
