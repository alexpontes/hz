﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces;
using HzCasting.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzCasting.Infra.Repositories
{
    public sealed class CastingAgendaCastingRepository : RepositoryBase<CastingAgendaCasting>, ICastingAgendaCastingRepository
    {

        public IEnumerable<CastingAgendaCasting>  GetComplete()
        {
            return Context.Set<CastingAgendaCasting>().Include("Casting").ToList();
        }
    }
}
