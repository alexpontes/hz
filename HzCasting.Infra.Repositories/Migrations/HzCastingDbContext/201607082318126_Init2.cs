namespace HzCasting.Infra.Repositories.Migrations.HzCastingDbContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agencia", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Endereco", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.AgendaCasting", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Geolocation", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.PerfilFiltro", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Booking", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Modelo", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Banco", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Casting", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Cliente", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Evento", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Foto", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.FotoModelo", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Idioma", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.PerfilModelo", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.CorCabelo", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Tatuagem", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.AgendaCastingModelo", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.Invite", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.BackOffice", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.CastingAgendaCasting", "DataInclusao", c => c.DateTime());
            AddColumn("dbo.UsuarioCliente", "DataInclusao", c => c.DateTime());
            DropColumn("dbo.Foto", "DtInc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Foto", "DtInc", c => c.DateTime(nullable: false));
            DropColumn("dbo.UsuarioCliente", "DataInclusao");
            DropColumn("dbo.CastingAgendaCasting", "DataInclusao");
            DropColumn("dbo.BackOffice", "DataInclusao");
            DropColumn("dbo.Invite", "DataInclusao");
            DropColumn("dbo.AgendaCastingModelo", "DataInclusao");
            DropColumn("dbo.Tatuagem", "DataInclusao");
            DropColumn("dbo.CorCabelo", "DataInclusao");
            DropColumn("dbo.PerfilModelo", "DataInclusao");
            DropColumn("dbo.Idioma", "DataInclusao");
            DropColumn("dbo.FotoModelo", "DataInclusao");
            DropColumn("dbo.Foto", "DataInclusao");
            DropColumn("dbo.Evento", "DataInclusao");
            DropColumn("dbo.Cliente", "DataInclusao");
            DropColumn("dbo.Casting", "DataInclusao");
            DropColumn("dbo.Banco", "DataInclusao");
            DropColumn("dbo.Modelo", "DataInclusao");
            DropColumn("dbo.Booking", "DataInclusao");
            DropColumn("dbo.PerfilFiltro", "DataInclusao");
            DropColumn("dbo.Geolocation", "DataInclusao");
            DropColumn("dbo.AgendaCasting", "DataInclusao");
            DropColumn("dbo.Endereco", "DataInclusao");
            DropColumn("dbo.Agencia", "DataInclusao");
        }
    }
}
