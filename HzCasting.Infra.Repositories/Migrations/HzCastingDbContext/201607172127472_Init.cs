namespace HzCasting.Infra.Repositories.Migrations.HzCastingDbContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PerfilModelo", "CorCabelo_Id", "dbo.CorCabelo");
            DropIndex("dbo.PerfilModelo", new[] { "CorCabelo_Id" });
            AddColumn("dbo.PerfilModelo", "CorCabelo", c => c.Int(nullable: false));
            DropColumn("dbo.PerfilModelo", "CorCabelo_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PerfilModelo", "CorCabelo_Id", c => c.Long());
            DropColumn("dbo.PerfilModelo", "CorCabelo");
            CreateIndex("dbo.PerfilModelo", "CorCabelo_Id");
            AddForeignKey("dbo.PerfilModelo", "CorCabelo_Id", "dbo.CorCabelo", "Id");
        }
    }
}
