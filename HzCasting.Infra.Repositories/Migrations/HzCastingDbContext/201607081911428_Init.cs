namespace HzCasting.Infra.Repositories.Migrations.HzCastingDbContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.FotoModelo");
            AlterColumn("dbo.FotoModelo", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.FotoModelo", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.FotoModelo");
            AlterColumn("dbo.FotoModelo", "Id", c => c.Long(nullable: false));
            AddPrimaryKey("dbo.FotoModelo", "Id");
        }
    }
}
