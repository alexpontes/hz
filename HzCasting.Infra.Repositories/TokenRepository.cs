﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;
using HzCasting.Infra.Repositories.EF.Context;
using System.Linq;

namespace HzCasting.Infra.Repositories
{
    public class TokenRepository : RepositoryBase<Token>, ITokenRepository
    {
        public void DeleteByToken(string token)
        {
            var entity = GetByToken(token);
            Delete(entity);
        }

        public void DeleteByUserId(string userId)
        {
            var db = new HzCastingDbContext();
            var entity = db.Token.FirstOrDefault(t => t.UserId == userId);
            Delete(entity);
        }

        public Token GetByToken(string token)
        {
            var db = new HzCastingDbContext();
            return db.Token.FirstOrDefault(t => t.AuthToken == token);
        }
    }
}
