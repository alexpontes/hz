﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces;

namespace HzCasting.Infra.Repositories
{
    public class ClienteRepository : RepositoryBase<Cliente>, IClienteRepository
    {
    }
}
