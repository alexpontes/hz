﻿using HzCasting.Domain.Entities;
using HzCasting.Domain.Interfaces.Repository;
using HzCasting.Infra.Repositories.EF.Context;
using System.Linq;
using System;

namespace HzCasting.Infra.Repositories
{
    public class TokenDeviceRepository : RepositoryBase<TokenDevice>, ITokenDeviceRepository
    {
        public TokenDevice GetByToken(string token)
        {
            var db = new HzCastingDbContext();
            return db.TokenDevice.FirstOrDefault(t => t.Token == token);
        }

        public void Kill(string token)
        {
            var entity = GetByToken(token);
            Delete(entity);
        }
    }
}